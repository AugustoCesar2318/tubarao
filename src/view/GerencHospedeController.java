/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import dao.HospedeJpaController;
import dao.PessoaJpaController;
import dao.TelefoneJpaController;
import domain.Hospede;
import domain.Telefone;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import view.lista.ListaHospede;

/**
 * FXML Controller class
 *
 * @author Augusto
 */
public class GerencHospedeController implements Initializable {

    @FXML
    private TableView<ListaHospede> tabela;

    @FXML
    private TableColumn<ListaHospede, ImageView> foto;

    @FXML
    private TableColumn<ListaHospede, String> nome;

    @FXML
    private TableColumn<ListaHospede, String> email;

    @FXML
    private TableColumn<ListaHospede, String> dtnasc;

    @FXML
    private TableColumn<ListaHospede, Integer> idade;

    @FXML
    private TableColumn<ListaHospede, String> telefone;

    @FXML
    private TableColumn<ListaHospede, String> celular;

    @FXML
    private ImageView logo;

    @FXML
    private TextField buscar;

    @FXML
    private Button adicionar;

    @FXML
    private Button voltar;

    //Jpa's
    HospedeJpaController jpaHosp = new HospedeJpaController();
    TelefoneJpaController jpaTel = new TelefoneJpaController();

    private ObservableList<ListaHospede> data = FXCollections.observableArrayList();
    static List<Hospede> listaUpdate = null;
    private String busca = "";

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        Image img = new Image("file:C:\\Users\\Augusto\\Downloads\\datafiles_v2\\datafiles\\Secao1\\datafiles\\Logo\\super_black.jpg");
        logo.setImage(img);

        populaTabela(busca);

        voltar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    GerencHospede.Hospede.close();
                    new Menu().start(new Stage());
                } catch (IOException ex) {
                    Logger.getLogger(GerencHospedeController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        buscar.setOnKeyTyped(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                busca = buscar.getText();

                populaTabela(busca);
            }
        });

        adicionar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    GerencHospede.Hospede.close();
                    new CadHospede().start(new Stage());
                } catch (IOException ex) {
                    Logger.getLogger(GerencHospedeController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        tabela.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                if (event.getClickCount() > 1) {
                    String emailTab = tabela.getSelectionModel().getSelectedItem().getEmail();
                    listaUpdate = jpaHosp.findByEmail(emailTab);
                    try {
                        GerencHospede.Hospede.close();
                        new CadHospede().start(new Stage());
                    } catch (IOException ex) {
                        Logger.getLogger(GerencHospedeController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    }

    private void populaTabela(String busca) {
        tabela.getItems().clear();
        List<Hospede> lista = null;
        if (busca.equals("")) {
            lista = jpaHosp.findHospedeEntities();
        } else {
            lista = jpaHosp.findByNome(busca);
        }

        for (Hospede hosp : lista) {

            String nome2 = hosp.getPessoaidPessoa().getNomePessoa();

            String email2 = hosp.getPessoaidPessoa().getEmail();

            Date data2 = hosp.getPessoaidPessoa().getDtNascimento();
            SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
            String dt2 = formatador.format(data2);

            System.out.println(hosp.getPessoaidPessoa().getFoto());
            ImageView foto2 = new ImageView(new Image("File:/" + hosp.getPessoaidPessoa().getFoto()));
            foto2.setFitWidth(125);
            foto2.setFitHeight(120);

            Date Hoje = new Date();
            int anoAT = Hoje.getYear();
            int anoNasc = data2.getYear();
            int idad = anoAT - anoNasc;

            List<Telefone> tel = jpaTel.findTelByPessoa(hosp.getPessoaidPessoa());
            String telefone2 = "";
            String celular2 = "";
            for (Telefone telefono : tel) {
                telefone2 += telefono.getNumero() + ", ";
                celular2 += telefono.getCelular() + ", ";
            }

            data.add(new ListaHospede(nome2, foto2, email2, dt2, idad, telefone2, celular2));
        }
        tabela.setItems(data);

        nome.setCellValueFactory(cell -> cell.getValue().getNomeProperty());
        email.setCellValueFactory(cell -> cell.getValue().getEmailProperty());
        foto.setCellValueFactory(cell -> cell.getValue().getFotoProperty());
        dtnasc.setCellValueFactory(cell -> cell.getValue().getDtNascProperty());
        idade.setCellValueFactory(cell -> cell.getValue().getIdadeProperty().asObject());
        telefone.setCellValueFactory(cell -> cell.getValue().getTelefoneProperty());
        celular.setCellValueFactory(cell -> cell.getValue().getCelularProperty());
    }
}
