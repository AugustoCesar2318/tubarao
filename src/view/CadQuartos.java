/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.io.IOException;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author Augusto
 */
public class CadQuartos extends Application {
    
    static Stage cadquartos = new Stage();
    
    @Override
    public void start(Stage primaryStage) throws IOException {
        
        cadquartos = primaryStage;
        
        Parent root = FXMLLoader.load(CadQuartos.class.getResource("CadQuartos.fxml"));
        
        Scene scene = new Scene(root);
        
        primaryStage.setTitle("Cadastro de Quartos");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }   
}