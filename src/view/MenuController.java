/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import dao.FuncionariosJpaController;
import domain.Funcionarios;
import domain.Rededehoteis;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

public class MenuController implements Initializable {

    @FXML
    private ImageView logo;

    @FXML
    private ImageView image;

    @FXML
    private Button sair;

    @FXML
    private Label user;

    @FXML
    private Button funcionarios;

    @FXML
    private Button hospedes;

    @FXML
    private Button quartos;

    @FXML
    private Button aniversariantes;

    static Rededehoteis rede = null;
    FuncionariosJpaController jpaFunc = new FuncionariosJpaController();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {

//    Image image = new Image(Menu.class.getResourceAsStream("/Logo/super_blue.png"));
//    logo.setImage(image);
        user.setText(LoginController.user);
        image.setImage(LoginController.imagem);
        Image img = new Image("file:C:\\Users\\Augusto\\Downloads\\datafiles_v2\\datafiles\\Secao1\\datafiles\\Logo\\super_black.jpg");
        logo.setImage(img);
        
        List<Funcionarios> f = jpaFunc.findByNome(user.getText());
        for(Funcionarios func : f){
            rede = func.getRedeDeHoteisidRedeDeHoteis();
        }

        sair.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    new MainLogin().start(new Stage());
                    Menu.MyStage.close();
                } catch (IOException ex) {
                    Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        
        funcionarios.setOnAction(new EventHandler<ActionEvent>(){
           @Override
           public void handle(ActionEvent event){
               try {
                   Menu.MyStage.close();
                   new GerencFunc().start(new Stage());
               } catch (IOException ex) {
                   Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE, null, ex);
               }
           }
        });
        
        aniversariantes.setOnAction(new EventHandler<ActionEvent>(){
           @Override
           public void handle(ActionEvent event){
               try {
                   Menu.MyStage.close();
                   new RelatorioAniversario().start(new Stage());
               } catch (IOException ex) {
                   Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE, null, ex);
               }
           }
        });
        
        hospedes.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event){
                try {
                    Menu.MyStage.close();
                    new GerencHospede().start(new Stage());
                } catch (IOException ex) {
                    Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        
        quartos.setOnAction(new EventHandler<ActionEvent>(){
           @Override
           public void handle(ActionEvent e){
               try{
                   Menu.MyStage.close();
                   new GerencQuartos().start(new Stage());
               }catch(IOException ex){
                   Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE, null, ex);
               }
           }
        });
    }
}
