/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import dao.FuncionariosJpaController;
import domain.Funcionarios;
import domain.Pessoa;
import domain.Rededehoteis;
import domain.TipoFuncionario;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import view.lista.ListaFuncionarios;

/**
 * FXML Controller class
 *
 * @author Augusto
 */
public class GerencFuncController implements Initializable {

    @FXML
    private TextField buscar;

    @FXML
    private Button adicionar;

    @FXML
    private TableView<ListaFuncionarios> tabela;

    @FXML
    private TableColumn<ListaFuncionarios, ImageView> foto;

    @FXML
    private TableColumn<ListaFuncionarios, String> nome;

    @FXML
    private TableColumn<ListaFuncionarios, String> email;

    @FXML
    private TableColumn<ListaFuncionarios, String> dtnasc;

    @FXML
    private TableColumn<ListaFuncionarios, Integer> idade;

    @FXML
    private TableColumn<ListaFuncionarios, String> funcao;

    @FXML
    private TableColumn<ListaFuncionarios, String> hotel;

    @FXML
    private TableColumn<ListaFuncionarios, String> status;

    @FXML
    private ImageView logo;

    @FXML
    private Button voltar;

    @FXML
    private Button ativar;

    //variaveis
    FuncionariosJpaController daofunc = new FuncionariosJpaController();
    ObservableList<ListaFuncionarios> data;

    //Variáveis para recuperar no update
    String NomeTabela;
    String emailTabela;
    String dtnascTabela;
    String funcaoTabela;
    String hotelTabela;
    String statusTabela;
    static List<Funcionarios> listaupdate = null;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        Image img = new Image("file:C:\\Users\\Augusto\\Downloads\\datafiles_v2\\datafiles\\Secao1\\datafiles\\Logo\\super_black.jpg");
        logo.setImage(img);

        String busca = buscar.getText();
        buscar.setOnKeyTyped(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent e) {
                String b = buscar.getText();
                populaTabela(b);
            }
        });

        if (busca.equals("")) {
            populaTabela(busca);
        }

        adicionar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                try {
                    GerencFunc.stage.close();
                    new CadFunc().start(new Stage());
                } catch (IOException ex) {
                    Logger.getLogger(GerencFuncController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        tabela.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                if (e.getClickCount() > 1) {
                    emailTabela = tabela.getSelectionModel().getSelectedItem().getEmail();

                    listaupdate = daofunc.findByEmail(emailTabela);

                    try {
                        GerencFunc.stage.close();
                        new CadFunc().start(new Stage());
                    } catch (IOException ex) {
                        Logger.getLogger(GerencFuncController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });

        voltar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    GerencFunc.stage.close();
                    new Menu().start(new Stage());
                } catch (IOException ex) {
                    Logger.getLogger(GerencFuncController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        ativar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                emailTabela = tabela.getSelectionModel().getSelectedItem().getEmail();

                List<Funcionarios> list = daofunc.findByEmail(emailTabela);

                for (Funcionarios f : list) {
                    if (f.getAtivo()) {
                        daofunc.update2(f);
                        JOptionPane.showMessageDialog(null, "Funcionário Inativo!");
                    } else {
                        daofunc.update(f);
                        JOptionPane.showMessageDialog(null, "Funcionário Ativo!");
                    }
                    populaTabela(busca);
                }
            }
        }
        );
    }

    public void populaTabela(String busca) {
        List<Funcionarios> lista = null;
        if (busca.equals("")) {
            lista = daofunc.findFuncionariosEntities();
        } else {
            lista = daofunc.findByNome(busca);
        }

        data = FXCollections.observableArrayList();
        for (int i = 0; i < lista.size(); i++) {
            ListaFuncionarios func = new ListaFuncionarios();
            Funcionarios f = lista.get(i);
            Pessoa p = f.getPessoaidPessoa();
            TipoFuncionario tipo = f.getTipoFuncionarioidTipoFuncionario();
            Rededehoteis hoteis = f.getRedeDeHoteisidRedeDeHoteis();

            int id = f.getIdFuncionarios();
            int idPessoa = p.getIdPessoa();
            String nome = p.getNomePessoa();
            ImageView foto = new ImageView(new Image("File:"+p.getFoto()));
            foto.setFitWidth(120);
            foto.setFitHeight(125);
            
            
            String email = p.getEmail();
            Date dtNasc = (Date) p.getDtNascimento();
            SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");//Formata a hora para ficar bonitinho
            String nasc = formatador.format(dtNasc);

            Date dataNasc = new Date();//Essa e a linha de baixo pega a data atual
            String dt = formatador.format(dataNasc);

            int nascimento = dtNasc.getYear();//pega o ano do nascimento
            int atual = dataNasc.getYear();//pega o ano atual
            int idad = atual - nascimento;//faz o calculo pra ver a idade(não leva em considerção dia e mes)

            String funcao = tipo.getNome();
            String hotel = hoteis.getNomeHotel();
            String status = "";
            if (f.getAtivo()) {
                status = "Ativo";
            } else {
                status = "Inativo";
            }

            data.add(new ListaFuncionarios(id, nome, foto, email, nasc, idad, funcao, hotel, status));
        }

        tabela.setItems(data);

        foto.setCellValueFactory(cell -> cell.getValue().getFotoProperty());
        nome.setCellValueFactory(cell -> cell.getValue().getNomeProperty());
        email.setCellValueFactory(cell -> cell.getValue().getEmailProperty());
        dtnasc.setCellValueFactory(cell -> cell.getValue().getDtNascProperty());
        idade.setCellValueFactory(cell -> cell.getValue().getIdadeProperty().asObject());  //SimpleIntegerProperty cannot be converted to ObservableValue<Integer>, colocar o .asObject() resolve esse erro
        funcao.setCellValueFactory(cell -> cell.getValue().getFuncaoProperty());
        hotel.setCellValueFactory(cell -> cell.getValue().getHotelProperty());
        status.setCellValueFactory(cell -> cell.getValue().getStatusProperty());
    }
}
