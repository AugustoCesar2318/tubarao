/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import dao.CidadeJpaController;
import dao.EstadoJpaController;
import dao.HospedeJpaController;
import dao.PaisJpaController;
import dao.PessoaJpaController;
import dao.TelefoneJpaController;
import dao.exceptions.NonexistentEntityException;
import domain.Cidade;
import domain.Estado;
import domain.Hospede;
import domain.Pais;
import domain.Pessoa;
import domain.Telefone;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Augusto
 */
public class CadHospedeController implements Initializable {

    @FXML
    private ImageView logo;

    @FXML
    private Button choose;

    @FXML
    private ImageView foto;

    @FXML
    private TextField nome;

    @FXML
    private TextField cpf;

    @FXML
    private TextField sexo;

    @FXML
    private ComboBox<Pais> pais;

    @FXML
    private ComboBox<Estado> estado;

    @FXML
    private ComboBox<Cidade> cidade;

    @FXML
    private TextField cep;

    @FXML
    private TextField bairro;

    @FXML
    private TextField rua;

    @FXML
    private TextField numero;

    @FXML
    private TextField complemento;

    @FXML
    private TextField telefone;

    @FXML
    private TextField celular;

    @FXML
    private TextField email;

    @FXML
    private DatePicker date;

    @FXML
    private TextArea obs;

    @FXML
    private Button cadastrar;

    @FXML
    private Button voltar;

    private int idPessoa;
    private int idHospede;
    private int idTelefone;

    //Jpa's
    PessoaJpaController jpaPessoa = new PessoaJpaController();
    HospedeJpaController jpaHosp = new HospedeJpaController();
    TelefoneJpaController jpaTel = new TelefoneJpaController();
    PaisJpaController jpaPais = new PaisJpaController();
    EstadoJpaController jpaEstado = new EstadoJpaController();
    CidadeJpaController jpaCidade = new CidadeJpaController();

    private String ImagePath;
    int achou = 0;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Image img = new Image("file:C:\\Users\\Augusto\\Downloads\\datafiles_v2\\datafiles\\Secao1\\datafiles\\Logo\\super_black.jpg");
        logo.setImage(img);

        List<Pais> listPais = jpaPais.findPaisEntities();
        for (Pais pais2 : listPais) {
            pais.getItems().addAll(pais2);
        }

        estado.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                List<Estado> listEstado = jpaEstado.findByPais(pais.getSelectionModel().getSelectedItem());
                for (Estado estados : listEstado) {
                    estado.getItems().addAll(estados);
                }
            }
        });

        cidade.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                List<Cidade> listCidade = jpaCidade.findByEstado(estado.getSelectionModel().getSelectedItem());
                for (Cidade city : listCidade) {
                    cidade.getItems().addAll(city);
                }
            }
        });

        List<Hospede> lista = GerencHospedeController.listaUpdate;
        if (lista != null) {
            for (Hospede h : lista) {
                idPessoa = h.getPessoaidPessoa().getIdPessoa();
                nome.setText(h.getPessoaidPessoa().getNomePessoa());
                email.setText(h.getPessoaidPessoa().getEmail());
                cpf.setText(h.getPessoaidPessoa().getCpf());
                sexo.setText(h.getPessoaidPessoa().getSexo());
                email.setText(h.getPessoaidPessoa().getEmail());

                idHospede = h.getIdHospede();

                SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");//Formata a data para ficar bonitinho
                String nasc = formatador.format(h.getPessoaidPessoa().getDtNascimento());
                date.setPromptText(nasc);
                obs.setText(h.getPessoaidPessoa().getObs());

                Image image = new Image("File:/" + h.getPessoaidPessoa().getFoto());
                foto.setImage(image);

                //Endereco
                pais.setPromptText(h.getPessoaidPessoa().getCidadeidCidade().getEstadoidEstado().getPaisidPais().getNome());
                estado.setPromptText(h.getPessoaidPessoa().getCidadeidCidade().getEstadoidEstado().getNome());
                cidade.setPromptText(h.getPessoaidPessoa().getCidadeidCidade().getNome());
                bairro.setText(h.getPessoaidPessoa().getBairro());
                rua.setText(h.getPessoaidPessoa().getRua());
                numero.setText(String.valueOf(h.getPessoaidPessoa().getNumero()));
                cep.setText(h.getPessoaidPessoa().getCep());
                complemento.setText(h.getPessoaidPessoa().getComplemento());

                //Contato
                List<Telefone> Listatelefone = jpaTel.findTelByPessoa(h.getPessoaidPessoa());
                for (Telefone telefono : Listatelefone) {
                    idTelefone = telefono.getIdTelefone();
                    telefone.setText(telefono.getNumero());
                    celular.setText(telefono.getCelular());
                    break;
                }
            }
            achou = 1;
        }

        cadastrar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Pessoa p = new Pessoa();
                Hospede h = new Hospede();
                Telefone t = new Telefone();
                Cidade c = new Cidade();

                c = cidade.getSelectionModel().getSelectedItem();

                LocalDate dt = date.getValue();
                String dt2 = String.valueOf(dt);
                dt2.replace('/', '-');
                Date dtNasc = Date.valueOf(dt2);

                p.setIdPessoa(idPessoa);
                p.setNomePessoa(nome.getText());
                p.setBairro(bairro.getText());
                p.setCep(cep.getText());
                p.setCidadeidCidade(c);
                p.setComplemento(complemento.getText());
                p.setCpf(cpf.getText());
                p.setEmail(email.getText());
                p.setNumero(Integer.parseInt(numero.getText()));
                p.setObs(obs.getText());
                p.setRua(rua.getText());
                p.setSexo(sexo.getText());
                p.setFoto(ImagePath);
                p.setDtNascimento(dtNasc);

                if (achou == 0) {
                    jpaPessoa.create(p);
                } else {
                    try {
                        jpaPessoa.edit(p);
                    } catch (NonexistentEntityException ex) {
                        Logger.getLogger(CadHospedeController.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (Exception ex) {
                        Logger.getLogger(CadHospedeController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                h.setPessoaidPessoa(p);
                h.setIdHospede(idHospede);

                if (achou == 0) {
                    jpaHosp.create(h);
                } else {
                    try {
                        jpaHosp.edit(h);
                    } catch (NonexistentEntityException ex) {
                        Logger.getLogger(CadHospedeController.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (Exception ex) {
                        Logger.getLogger(CadHospedeController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                t.setIdTelefone(idTelefone);
                t.setCelular(celular.getText());
                t.setNumero(telefone.getText());
                t.setPessoaidPessoa(p);
                t.setRedeDeHoteisidRedeDeHoteis(null);

                if (achou == 0) {
                    jpaTel.create(t);
                } else {
                    try {
                        jpaTel.edit(t);
                    } catch (Exception ex) {
                        Logger.getLogger(CadHospedeController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });

        voltar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    CadHospede.stage.close();
                    new GerencHospede().start(new Stage());
                } catch (IOException ex) {
                    Logger.getLogger(CadHospedeController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        choose.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                FileChooser filechooser = new FileChooser();
                filechooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Image FIles", "*.jpg", "*.png"));
                File file = filechooser.showOpenDialog(CadHospede.stage);

                URI ulr = file.toURI();
                String url = ulr.toString();
                Image img = new Image(url);
                foto.setImage(img);

                String replace = url.replace("file:/", "");
                String replace1 = replace.replace("/", "//");

                ImagePath = replace1;
            }
        });
    }
}