/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import dao.FuncionariosJpaController;
import dao.HospedeJpaController;
import dao.PessoaJpaController;
import dao.utilDAO;
import domain.Funcionarios;
import domain.Hospede;
import domain.Pessoa;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import view.lista.ListaAniversariantes;

/**
 * FXML Controller class
 *
 * @author Augusto
 */
public class RelatorioAniversariantesController implements Initializable {

    @FXML
    private ImageView logo;
    
    @FXML
    private TableView<ListaAniversariantes> tabela;

    @FXML
    private TableColumn<ListaAniversariantes, String> nome;

    @FXML
    private TableColumn<ListaAniversariantes, String> dtnasc;

    @FXML
    private TableColumn<ListaAniversariantes, Integer> idade;

    @FXML
    private TableColumn<ListaAniversariantes, Integer> dias;

    @FXML
    private TableColumn<ListaAniversariantes, String> tipo;

    @FXML
    private ComboBox<String> mes;

    @FXML
    private ComboBox<String> filtro;

    @FXML
    private Button voltar;
    
    utilDAO util = new utilDAO();

    //JPA's
    PessoaJpaController jpaPessoa = new PessoaJpaController();
    FuncionariosJpaController jpaFunc = new FuncionariosJpaController();
    HospedeJpaController jpaHosp = new HospedeJpaController();

    ObservableList<ListaAniversariantes> data;
    List<Pessoa> lista = jpaPessoa.findPessoaEntities();
    List<Hospede> listaHosp = jpaHosp.findHospedeEntities();
    List<Funcionarios> listaFunc = jpaFunc.findFuncionariosEntities();

    int mes2 = 1;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        Image image = new Image("file:C:\\Users\\Augusto\\Downloads\\datafiles_v2\\datafiles\\Secao1\\datafiles\\Logo\\super_black.jpg");
        logo.setImage(image);
        
        voltar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    new Menu().start(new Stage());
                    RelatorioAniversario.stage.close();
                } catch (IOException ex) {
                    Logger.getLogger(RelatorioAniversariantesController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        });
        String busca = "";

        mes.getItems().addAll("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");
        mes.setValue("Janeiro");

        data = FXCollections.observableArrayList();
        
        populatabela();

        mes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                switch (mes.getSelectionModel().getSelectedItem()) {
                    case "Janeiro":
                        mes2 = 1;
                        break;
                    case "Fevereiro":
                        mes2 = 2;
                        break;
                    case "Março":
                        mes2 = 3;
                        break;
                    case "Abril":
                        mes2 = 4;
                        break;
                    case "Maio":
                        mes2 = 5;
                        break;
                    case "Junho":
                        mes2 = 6;
                        break;
                    case "Julho":
                        mes2 = 7;
                        break;
                    case "Agosto":
                        mes2 = 8;
                        break;
                    case "Setembro":
                        mes2 = 9;
                        break;
                    case "Outubro":
                        mes2 = 10;
                        break;
                    case "Novembro":
                        mes2 = 11;
                        break;
                    case "Dezembro":
                        mes2 = 12;
                        break;
                }
                if (filtro.getSelectionModel().getSelectedItem().equals("Todos")) {
                    tabela.getItems().clear();
                    populatabela();
                } else if (filtro.getSelectionModel().getSelectedItem().equals("Hóspedes")) {
                    tabela.getItems().clear();
                    populaTabelaHospede();
                } else if (filtro.getSelectionModel().getSelectedItem().equals("Funcionários")) {
                    tabela.getItems().clear();
                    populaTabelaFunc();
                }
            }
        });

        filtro.getItems().addAll("Todos", "Hóspedes", "Funcionários");
        filtro.setValue("Todos");

        filtro.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (filtro.getSelectionModel().getSelectedItem().equals("Todos")) {
                    tabela.getItems().clear();
                    populatabela();
                } else if (filtro.getSelectionModel().getSelectedItem().equals("Hóspedes")) {
                    tabela.getItems().clear();
                    populaTabelaHospede();
                } else if (filtro.getSelectionModel().getSelectedItem().equals("Funcionários")) {
                    tabela.getItems().clear();
                    populaTabelaFunc();
                }
            }
        });
    }

    public void populatabela() {
        for (Pessoa p : lista) {
            
            String tipo = "";
            
            String nome = p.getNomePessoa();
            String email = p.getEmail();
            Date dtNasc = (Date) p.getDtNascimento();
            SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");//Formata a data para ficar bonitinho
            String nasc = formatador.format(dtNasc);

            if(p.getFuncionariosCollection() != null){
                tipo = "Funcionário";
            }else{
                tipo = "Hóspede";
            }
            
            Calendar cal = Calendar.getInstance();
            
            SimpleDateFormat formata = new SimpleDateFormat("yyyy/MM/dd");//Formata a data para ficar bonitinho
            Date dataNasc = util.getDate();
            String dt = formata.format(dataNasc);
            
            int mesNascimento = dtNasc.getMonth();

            int nascimento = dtNasc.getYear();//pega o ano do nascimento
            int atual = dataNasc.getYear();//pega o ano atual
            int idad = atual - nascimento;
            
            SimpleDateFormat day = new SimpleDateFormat("dd");
            String dianasc = day.format(dtNasc);
            int dianascimento = Integer.parseInt(dianasc);
            int diaatual = cal.get(Calendar.DAY_OF_MONTH);
//            int diaatual = dataNasc.getDate();
            
            int dias = 0;

            dias = dianascimento - diaatual;

            if (mesNascimento == mes2-1) {
                data.add(new ListaAniversariantes(nome, nasc, idad, dias, tipo));
            }
        }
        tabela.setItems(data);

        nome.setCellValueFactory(cell -> cell.getValue().getNomeProperty());
        dtnasc.setCellValueFactory(cell -> cell.getValue().getDtNascProperty());
        idade.setCellValueFactory(cell -> cell.getValue().getIdadeProperty().asObject());
        dias.setCellValueFactory(cell -> cell.getValue().getDiasProperty().asObject());
        tipo.setCellValueFactory(cell -> cell.getValue().getTipoProperty());
    }

    public void populaTabelaFunc() {
        for (Funcionarios f : listaFunc) {

            Pessoa p = f.getPessoaidPessoa();

            String nome = p.getNomePessoa();
            String email = p.getEmail();
            Date dtNasc = (Date) p.getDtNascimento();
            SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");//Formata a data para ficar bonitinho
            String nasc = formatador.format(dtNasc);

            Calendar cal = Calendar.getInstance();
            
            SimpleDateFormat formata = new SimpleDateFormat("yyyy/MM/dd");//Formata a data para ficar bonitinho
            Date dataNasc = util.getDate();
            String dt = formata.format(dataNasc);
            
            int mesNascimento = dtNasc.getMonth();

            int nascimento = dtNasc.getYear();//pega o ano do nascimento
            int atual = dataNasc.getYear();//pega o ano atual
            int idad = atual - nascimento;
            
            SimpleDateFormat day = new SimpleDateFormat("dd");
            String dianasc = day.format(dtNasc);
            int dianascimento = Integer.parseInt(dianasc);
            int diaatual = cal.get(Calendar.DAY_OF_MONTH);
//            int diaatual = dataNasc.getDate();
            
            int dias = 0;

            dias = dianascimento - diaatual;
            

            String tipo = "Funcionário";

            if (mesNascimento == mes2-1) {
                data.add(new ListaAniversariantes(nome, nasc, idad, dias, tipo));
            }
        }
        tabela.setItems(data);

        nome.setCellValueFactory(cell -> cell.getValue().getNomeProperty());
        dtnasc.setCellValueFactory(cell -> cell.getValue().getDtNascProperty());
        idade.setCellValueFactory(cell -> cell.getValue().getIdadeProperty().asObject());
        dias.setCellValueFactory(cell -> cell.getValue().getDiasProperty().asObject());
        tipo.setCellValueFactory(cell -> cell.getValue().getTipoProperty());
    }

    public void populaTabelaHospede() {
        for (Hospede h : listaHosp) {

            Pessoa p = h.getPessoaidPessoa();

            String nome = p.getNomePessoa();
            String email = p.getEmail();
            Date dtNasc = (Date) p.getDtNascimento();
            SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");//Formata a data para ficar bonitinho
            String nasc = formatador.format(dtNasc);

            Calendar cal = Calendar.getInstance();
            
            SimpleDateFormat formata = new SimpleDateFormat("yyyy/MM/dd");//Formata a data para ficar bonitinho
            Date dataNasc = util.getDate();
            String dt = formata.format(dataNasc);
            
            int mesNascimento = dtNasc.getMonth();

            int nascimento = dtNasc.getYear();//pega o ano do nascimento
            int atual = dataNasc.getYear();//pega o ano atual
            int idad = atual - nascimento;
            
            SimpleDateFormat day = new SimpleDateFormat("dd");
            String dianasc = day.format(dtNasc);
            int dianascimento = Integer.parseInt(dianasc);
            int diaatual = cal.get(Calendar.DAY_OF_MONTH);
//            int diaatual = dataNasc.getDate();
            
            int dias = 0;

            dias = dianascimento - diaatual;
            

            String tipo = "Hóspede";

            if (mesNascimento == mes2-1) {
                data.add(new ListaAniversariantes(nome, nasc, idad, dias, tipo));
            }
        }
        tabela.setItems(data);

        nome.setCellValueFactory(cell -> cell.getValue().getNomeProperty());
        dtnasc.setCellValueFactory(cell -> cell.getValue().getDtNascProperty());
        idade.setCellValueFactory(cell -> cell.getValue().getIdadeProperty().asObject());
        dias.setCellValueFactory(cell -> cell.getValue().getDiasProperty().asObject());
        tipo.setCellValueFactory(cell -> cell.getValue().getTipoProperty());
    }
}
