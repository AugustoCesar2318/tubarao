/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import dao.CidadeJpaController;
import dao.EstadoJpaController;
import dao.FuncionariosJpaController;
import dao.PaisJpaController;
import dao.PessoaJpaController;
import dao.RededehoteisJpaController;
import dao.TelefoneJpaController;
import dao.TipoFuncionarioJpaController;
import domain.Cidade;
import domain.Estado;
import domain.Funcionarios;
import domain.Pais;
import domain.Pessoa;
import domain.Rededehoteis;
import domain.Telefone;
import domain.TipoFuncionario;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author Augusto
 */
public class CadFuncController implements Initializable {

    @FXML
    private TextField nome;

    @FXML
    private TextField cpf;

    @FXML
    private TextField sexo;

    @FXML
    private TextField email;

    @FXML
    private ComboBox<Pais> pais;

    @FXML
    private ComboBox<Estado> estado;

    @FXML
    private ComboBox<Cidade> cidade;

    @FXML
    private TextField cep;

    @FXML
    private TextField bairro;

    @FXML
    private TextField rua;

    @FXML
    private TextField numero;

    @FXML
    private TextField complemento;

    @FXML
    private TextField telefone;

    @FXML
    private TextField celular;

    @FXML
    private DatePicker dtNasc;

    @FXML
    private ComboBox<TipoFuncionario> funcao;

    @FXML
    private Button foto;

    @FXML
    private Button cadastrar;

    @FXML
    private ImageView imagem;

    @FXML
    private Button voltar;

    @FXML
    private ComboBox<Rededehoteis> hotel;

    @FXML
    private PasswordField senha;

    @FXML
    private PasswordField confirm;

    @FXML
    private TextArea obs;

    @FXML
    private ImageView logo;

    //JPA's
    PaisJpaController jpaPais = new PaisJpaController();
    EstadoJpaController jpaEstado = new EstadoJpaController();
    CidadeJpaController jpaCidade = new CidadeJpaController();
    RededehoteisJpaController jpaHoteis = new RededehoteisJpaController();
    PessoaJpaController jpaPessoa = new PessoaJpaController();
    TipoFuncionarioJpaController jpaTipo = new TipoFuncionarioJpaController();
    FuncionariosJpaController jpaFunc = new FuncionariosJpaController();
    TelefoneJpaController jpaTelefone = new TelefoneJpaController();

    private String imagemPath = "";
    
    private int idPessoa;
    private int idFunc;
    private int idTel;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        Image log = new Image("file:C:\\Users\\Augusto\\Downloads\\datafiles_v2\\datafiles\\Secao1\\datafiles\\Logo\\super_black.jpg");
        logo.setImage(log);

        List<TipoFuncionario> listTipo = jpaTipo.findTipoFuncionarioEntities();
        for (TipoFuncionario tipo : listTipo) {
            funcao.getItems().addAll(tipo);
        }

        List<Pais> listPais = jpaPais.findPaisEntities();
        for (Pais pais2 : listPais) {
            pais.getItems().addAll(pais2);
        }

        estado.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                estado.getItems().clear();
                List<Estado> listEstado = jpaEstado.findByPais(pais.getSelectionModel().getSelectedItem());
                for (Estado estados : listEstado) {
                    estado.getItems().addAll(estados);
                }
            }
        });

        cidade.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                cidade.getItems().clear();
                List<Cidade> listCidade = jpaCidade.findByEstado(estado.getSelectionModel().getSelectedItem());
                for (Cidade city : listCidade) {
                    cidade.getItems().addAll(city);
                }
            }
        });

        List<Rededehoteis> listHoteis = jpaHoteis.findRededehoteisEntities();
        for (Rededehoteis red : listHoteis) {
            hotel.getItems().addAll(red);
        }

        //Recebe as informações da tabela para o update
        List<Funcionarios> lista5 = GerencFuncController.listaupdate;
        if (lista5 != null) {
            for (Funcionarios func : lista5) {

                Pessoa p = func.getPessoaidPessoa();
                List<Telefone> Listatelefone = jpaTelefone.findTelByPessoa(p);

                nome.setText(func.getPessoaidPessoa().getNomePessoa());
                email.setText(func.getPessoaidPessoa().getEmail());
                cpf.setText(func.getPessoaidPessoa().getCpf());
                sexo.setText(func.getPessoaidPessoa().getSexo());
                obs.setText(func.getPessoaidPessoa().getObs());
                idFunc = func.getIdFuncionarios();
                idPessoa = func.getPessoaidPessoa().getIdPessoa();

                SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");//Formata a data para ficar bonitinho
                String nasc = formatador.format(func.getPessoaidPessoa().getDtNascimento());
                dtNasc.setPromptText(nasc);
                //endereco
                cep.setText(func.getPessoaidPessoa().getCep());
                bairro.setText(func.getPessoaidPessoa().getBairro());
                rua.setText(func.getPessoaidPessoa().getRua());
                numero.setText(String.valueOf(func.getPessoaidPessoa().getNumero()));
                complemento.setText(func.getPessoaidPessoa().getComplemento());
                //Cidade
                cidade.setPromptText(func.getPessoaidPessoa().getCidadeidCidade().getNome());
                //Estado
                estado.setPromptText(func.getPessoaidPessoa().getCidadeidCidade().getEstadoidEstado().getNome());
                //Pais
                pais.setPromptText(func.getPessoaidPessoa().getCidadeidCidade().getEstadoidEstado().getPaisidPais().getNome());
                //funcao
                funcao.setPromptText(func.getTipoFuncionarioidTipoFuncionario().getNome());
                //hotel
                hotel.setPromptText(func.getRedeDeHoteisidRedeDeHoteis().getNomeHotel());
                senha.setText(func.getSenha());
                confirm.setText(func.getSenha());
                //telefone
                for (Telefone tel : Listatelefone) {
                    idTel = tel.getIdTelefone();
                    telefone.setText(tel.getNumero());
                    celular.setText(tel.getCelular());
                    break;
                }
                //foto
                Image img = new Image("file:" + func.getPessoaidPessoa().getFoto());
                imagem.setImage(img);
            }
        }

        foto.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                FileChooser filechooser = new FileChooser();
                filechooser.getExtensionFilters().addAll(new ExtensionFilter("Image FIles", "*.jpg", "*.png"));
                File file = filechooser.showOpenDialog(CadFunc.Cad);

                URI ulr = file.toURI();
                String url = ulr.toString();
                Image img = new Image(url);
                imagem.setImage(img);

                String replace = url.replace("file:/", "");
                String replace1 = replace.replace("/", "//");

                imagemPath = replace1;
            }
        });

        voltar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                try {
                    CadFunc.Cad.close();
                    new GerencFunc().start(new Stage());
                    GerencFuncController.listaupdate = null;
                } catch (IOException ex) {
                    Logger.getLogger(CadFuncController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        cadastrar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                int dont = 0;//verifica se dá erro(Como ainda não fiz validação pra cada campo fiz esse pra me informar se desse algum)

                //Pega data e converte pra LocalDate que é convertido pra String e depois trocado os '/' para '-'
                LocalDate dt = dtNasc.getValue();
                String dt2 = String.valueOf(dt);
                dt2.replace('/', '-');
                Date dtNascimento = Date.valueOf(dt2);

                Funcionarios func = new Funcionarios();
                Pessoa pessoa = new Pessoa();
                Telefone tel = new Telefone();
                Rededehoteis hot = new Rededehoteis();

                pessoa.setCidadeidCidade(cidade.getValue());
                pessoa.setBairro(bairro.getText());
                pessoa.setCep(cep.getText());
                pessoa.setRua(rua.getText());
                pessoa.setComplemento(complemento.getText());
                pessoa.setNumero(Integer.parseInt(numero.getText()));

                pessoa.setNomePessoa(nome.getText());
                pessoa.setCpf(cpf.getText());
                pessoa.setEmail(email.getText());
                pessoa.setDtNascimento(dtNascimento);
                pessoa.setSexo(sexo.getText());
                pessoa.setObs(obs.getText());
                pessoa.setFoto(imagemPath);

                try {
//                    if (jpaPessoa.findByEmail(pessoa.getEmail()) != null) {/*O email é Unique, se ele é null é pq não existe essa pessoa ainda,
//                        usei ele pra fazer a verificação daí*/
//                        pessoa.setIdPessoa(idPessoa);
//                        jpaPessoa.edit(pessoa);
//                    } else {
                        jpaPessoa.create(pessoa);
//                    }
                } catch (Exception ex) {
                    dont = 1;
                    Logger.getLogger(CadFuncController.class.getName()).log(Level.SEVERE, null, ex);
                }

                hot = hotel.getValue();

                tel.setCelular(celular.getText());
                tel.setNumero(telefone.getText());
                tel.setPessoaidPessoa(pessoa);
                tel.setRedeDeHoteisidRedeDeHoteis(hot);

                try {
//                    if (jpaTelefone.findTelByPessoa(tel.getPessoaidPessoa()) != null) {
//                        tel.setIdTelefone(idTel);
//                        jpaTelefone.edit(tel);
//                    } else {
                        jpaTelefone.create(tel);
//                    }
                } catch (Exception ex) {
                    dont = 1;
                    Logger.getLogger(CadFuncController.class.getName()).log(Level.SEVERE, null, ex);
                }

                func.setPessoaidPessoa(pessoa);
                func.setTipoFuncionarioidTipoFuncionario(funcao.getValue());
                func.setAtivo(true);
                func.setRedeDeHoteisidRedeDeHoteis(hot);

                if (senha.getText().equals(confirm.getText())) {
                    func.setSenha(senha.getText());
                } else {
                    JOptionPane.showMessageDialog(null, "Senha e Confirmação de senha estão diferentes!");
                    dont = 1;
                }

                try {
//                    if (jpaFunc.findByEmail(pessoa.getEmail()) != null) {
//                        func.setIdFuncionarios(idFunc);
//                        jpaFunc.edit(func);
//                    } else {
                        jpaFunc.create(func);
//                    }
                } catch (Exception ex) {
                    dont = 1;
                    Logger.getLogger(CadFuncController.class.getName()).log(Level.SEVERE, null, ex);
                }

                if (dont == 1) {
                    JOptionPane.showMessageDialog(null, "Erro");
                } else {
                    JOptionPane.showMessageDialog(null, "Funcionário cadastrado com sucesso!");
                }

                GerencFuncController.listaupdate = null;
            }
        });
    }
}