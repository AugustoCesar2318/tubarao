/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.lista;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.image.ImageView;

/**
 *
 * @author Augusto
 */
public class ListaHospede {

    private SimpleStringProperty nome;
    private SimpleObjectProperty<ImageView> foto;
    private SimpleStringProperty email;
    private SimpleStringProperty dtNasc;
    private SimpleIntegerProperty idade;
    private SimpleStringProperty celular;
    private SimpleStringProperty telefone;

    public ListaHospede(String nome, ImageView foto, String email, String dtNasc, Integer idade, String telefone, String celular) {
        this.nome = new SimpleStringProperty(nome);
        this.foto = new SimpleObjectProperty<>(foto);
        this.email = new SimpleStringProperty(email);
        this.dtNasc = new SimpleStringProperty(dtNasc);
        this.idade = new SimpleIntegerProperty(idade);
        this.telefone = new SimpleStringProperty(telefone);
        this.celular = new SimpleStringProperty(celular);
    }

    public ListaHospede() {
    }

    public String getNome() {
        return nome.get();
    }

    public SimpleStringProperty getNomeProperty() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = new SimpleStringProperty(nome);
    }

    public ImageView getFoto() {
        return foto.get();
    }

    public SimpleObjectProperty<ImageView> getFotoProperty() {
        return foto;
    }

    public void setFoto(ImageView foto) {
        this.foto = new SimpleObjectProperty<>(foto);
    }

    public String getEmail() {
        return email.get();
    }

    public SimpleStringProperty getEmailProperty() {
        return email;
    }

    public void setEmail(String email) {
        this.email = new SimpleStringProperty(email);
    }

    public String getDtNasc() {
        return dtNasc.get();
    }

    public SimpleStringProperty getDtNascProperty() {
        return dtNasc;
    }

    public void setDtNasc(String dtNasc) {
        this.dtNasc = new SimpleStringProperty(dtNasc);
    }

    public Integer getIdade() {
        return idade.get();
    }

    public SimpleIntegerProperty getIdadeProperty() {
        return idade;
    }

    public void setIdade(Integer idade) {
        this.idade = new SimpleIntegerProperty(idade);
    }

    public String getTelefone() {
        return telefone.get();
    }

    public SimpleStringProperty getTelefoneProperty() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = new SimpleStringProperty(telefone);
    }

    public String getCelular() {
        return celular.get();
    }

    public SimpleStringProperty getCelularProperty() {
        return celular;
    }

    public void setCelular(String telefone) {
        this.celular = new SimpleStringProperty(telefone);
    }
}
