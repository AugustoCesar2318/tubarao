/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.lista;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Augusto
 */
public class ListaAniversariantes {
    private SimpleStringProperty nome;
    private SimpleStringProperty dtNasc;
    private SimpleIntegerProperty idade;
    private SimpleIntegerProperty dias;
    private SimpleStringProperty tipo;

    public ListaAniversariantes(String nome, String dtNasc, Integer idade, Integer dias, String tipo) {
        this.nome = new SimpleStringProperty(nome);
        this.dtNasc = new SimpleStringProperty(dtNasc);
        this.idade = new SimpleIntegerProperty(idade);
        this.dias = new SimpleIntegerProperty(dias);
        this.tipo = new SimpleStringProperty(tipo);
    }

    //nome
    public String getNome() {
        return nome.get();
    }

    public SimpleStringProperty getNomeProperty() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = new SimpleStringProperty(nome);
    }
    
    //data nascimento
    public String getDtNasc(){
        return dtNasc.get();
    }

    public SimpleStringProperty getDtNascProperty() {
        return dtNasc;
    }

    public void setDtNasc(String dtNasc) {
        this.dtNasc = new SimpleStringProperty(dtNasc);
    }

    //idade
    public Integer getIdade(){
        return idade.get();
    }
    
    public SimpleIntegerProperty getIdadeProperty() {
        return idade;
    }

    public void setIdade(Integer idade) {
        this.idade = new SimpleIntegerProperty(idade);
    }
    
    //dias
    public Integer getDias(){
        return dias.get();
    }

    public SimpleIntegerProperty getDiasProperty() {
        return dias;
    }

    public void setDias(Integer dias) {
        this.dias = new SimpleIntegerProperty(dias);
    }
    
    //tipo
    public String getTipo(){
        return tipo.get();
    }

    public SimpleStringProperty getTipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = new SimpleStringProperty(tipo);
    }
    
    
}
