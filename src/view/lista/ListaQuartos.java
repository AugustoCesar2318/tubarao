/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.lista;

import java.math.BigDecimal;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Augusto
 */
public class ListaQuartos {
    SimpleIntegerProperty numero;
    SimpleStringProperty Bloco;
    SimpleStringProperty tipo;
    SimpleObjectProperty<BigDecimal> preco;

    public ListaQuartos(Integer numero, String Bloco, String tipo, BigDecimal preco) {
        this.numero = new SimpleIntegerProperty(numero);
        this.Bloco = new SimpleStringProperty(Bloco);
        this.tipo = new SimpleStringProperty(tipo);
        this.preco = new SimpleObjectProperty<>(preco);
    }
    
    public ListaQuartos(){
    }
    
    public Integer getNumero(){
        return numero.get();
    }

    public SimpleIntegerProperty getNumeroProperty() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = new SimpleIntegerProperty(numero);
    }
    
    public String getBloco(){
        return Bloco.get();
    }

    public SimpleStringProperty getBlocoProperty() {
        return Bloco;
    }

    public void setBloco(String Bloco) {
        this.Bloco = new SimpleStringProperty(Bloco);
    }
    
    public String getTipo(){
        return tipo.get();
    }

    public SimpleStringProperty getTipoProperty() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = new SimpleStringProperty(tipo);
    }

    public BigDecimal getPreco(){
        return preco.get();
    }
    
    public SimpleObjectProperty<BigDecimal> getPrecoProperty() {
        return preco;
    }

    public void setPreco(BigDecimal preco) {
        this.preco = new SimpleObjectProperty<>(preco);
    }
}