/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.lista;

import java.io.File;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Augusto
 */
public class ListaFuncionarios {

    private SimpleIntegerProperty idFunc;
    private SimpleStringProperty nome;
    private SimpleObjectProperty<ImageView> foto;
    private SimpleStringProperty email;
    private SimpleStringProperty dtNasc;
    private SimpleIntegerProperty idade;
    private SimpleStringProperty funcao;
    private SimpleStringProperty hotel;
    private SimpleStringProperty status;

    public ListaFuncionarios(Integer idFunc, String nome, ImageView foto, String email, String dtNasc, Integer idade, String funcao, String hotel, String status) {
        this.idFunc = new SimpleIntegerProperty(idFunc);
        this.nome = new SimpleStringProperty(nome);
        this.foto = new SimpleObjectProperty<>(foto);
        this.email = new SimpleStringProperty(email);
        this.dtNasc = new SimpleStringProperty(dtNasc);
        this.idade = new SimpleIntegerProperty(idade);
        this.funcao = new SimpleStringProperty(funcao);
        this.hotel = new SimpleStringProperty(hotel);
        this.status = new SimpleStringProperty(status);
    }

    public ListaFuncionarios(Integer idFunc) {
        this.idFunc = new SimpleIntegerProperty(idFunc);
    }

    public ListaFuncionarios() {
    }

    public Integer getIdFunc() {
        return idFunc.get();
    }

    public SimpleIntegerProperty getIdFuncProperty() {
        return idFunc;
    }

    public void setIdFunc(Integer idFunc) {
        this.idFunc = new SimpleIntegerProperty(idFunc);
    }

    public String getNome() {
        return nome.get();
    }

    public SimpleStringProperty getNomeProperty() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = new SimpleStringProperty(nome);
    }

    public ImageView getFoto() {
        return foto.get();
    }

    public SimpleObjectProperty<ImageView> getFotoProperty() {
        return foto;
    }

    public void setFoto(ImageView foto) {
        this.foto = new SimpleObjectProperty<>(foto);
    }

    public String getEmail() {
        return email.get();
    }

    public SimpleStringProperty getEmailProperty() {
        return email;
    }

    public void setEmail(String email) {
        this.email = new SimpleStringProperty(email);
    }

    public String getDtNasc() {
        return dtNasc.get();
    }

    public SimpleStringProperty getDtNascProperty() {
        return dtNasc;
    }

    public void setDtNasc(String dtNasc) {
        this.dtNasc = new SimpleStringProperty(dtNasc);
    }

    public Integer getIdade() {
        return idade.get();
    }

    public SimpleIntegerProperty getIdadeProperty() {
        return idade;
    }

    public void setIdade(Integer idade) {
        this.idade = new SimpleIntegerProperty(idade);
    }

    public String getFuncao() {
        return funcao.get();
    }

    public SimpleStringProperty getFuncaoProperty() {
        return funcao;
    }

    public void setFuncao(String funcao) {
        this.funcao = new SimpleStringProperty(funcao);
    }

    public String getHotel() {
        return hotel.get();
    }

    public SimpleStringProperty getHotelProperty() {
        return hotel;
    }

    public void setHotel(String hotel) {
        this.hotel = new SimpleStringProperty(hotel);
    }

    public String getStatus() {
        return status.get();
    }

    public SimpleStringProperty getStatusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status = new SimpleStringProperty(status);
    }
}
