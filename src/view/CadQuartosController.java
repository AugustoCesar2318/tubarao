/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import dao.FotosquartosJpaController;
import dao.QuartosJpaController;
import dao.TiposDeQuartosJpaController;
import dao.exceptions.NonexistentEntityException;
import domain.Fotosquartos;
import domain.Quartos;
import domain.TiposDeQuartos;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author Augusto
 */
public class CadQuartosController implements Initializable {

    @FXML
    private ImageView logo;

    @FXML
    private TextField bloco;

    @FXML
    private ComboBox<TiposDeQuartos> tipo;

//    @FXML
//    private ImageView foto1;
    @FXML
    private Button choose1;

    @FXML
    private Button salvar;

    @FXML
    private Button voltar;

    @FXML
    private Label preco;

    @FXML
    private ListView<String> list;

    @FXML
    private Button remove1;

    //JPA's
    TiposDeQuartosJpaController jpaTipos = new TiposDeQuartosJpaController();
    QuartosJpaController jpaQuartos = new QuartosJpaController();
    FotosquartosJpaController jpaFotos = new FotosquartosJpaController();

    String imagemPath = "";

    ObservableList<String> fotos = FXCollections.observableArrayList();

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        List<TiposDeQuartos> lista = jpaTipos.findTiposDeQuartosEntities();
        for (TiposDeQuartos t : lista) {
            tipo.getItems().addAll(t);
        }

        choose1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                FileChooser Filechooser = new FileChooser();
                File file = Filechooser.showOpenDialog(CadQuartos.cadquartos);

                URI ulr = file.toURI();
                String url = ulr.toString();
//                Image img = new Image(url);
//                foto1.setImage(img);

                fotos.add(url);

                String replace = url.replace("file:/", "");
                String replace1 = replace.replace("/", "//");

                imagemPath = replace1;

                list.setItems(fotos);
            }
        });

        remove1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                String remover = list.getSelectionModel().getSelectedItem();

                list.getItems().remove(remover);
            }
        });

        tipo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                TiposDeQuartos tip = tipo.getSelectionModel().getSelectedItem();
                preco.setText(String.valueOf(tip.getPreco()));
            }
        });

        Quartos quart = GerencQuartosController.quart;
        List<Fotosquartos> listafotos = GerencQuartosController.ListaFotos;

        if (GerencQuartosController.quart != null) {
            bloco.setText(quart.getBloco());

//            tipo.setPromptText(quart.getTiposdequartosidTiposdequartos().getNome());
            tipo.setValue(quart.getTiposdequartosidTiposdequartos());

            List<TiposDeQuartos> t = jpaTipos.findTiposDeQuartosEntities();
            for (TiposDeQuartos tip : t) {
                if (tip.equals(tipo.getValue())) {
                    preco.setText(String.valueOf(tip.getPreco()));
                }
            }

            for (int i = 0; i < listafotos.size(); i++) {
                fotos.add(listafotos.get(i).getFotos());
            }
            list.getItems().addAll(fotos);

            remove1.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    int resp = JOptionPane.showConfirmDialog(null, "Não será possível desfazer a ação.\nContinuar?");
                    if (resp == 0) {
                        String remover = list.getSelectionModel().getSelectedItem();

                        Fotosquartos f = jpaFotos.findByUrl(quart, remover);
                        try {
                            list.getItems().remove(remover);
                            jpaFotos.destroy(f.getIdFotosQuartos());
                        } catch (NonexistentEntityException ex) {
                            Logger.getLogger(CadQuartosController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            });
        }

        salvar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                if (GerencQuartosController.quart == null) {
                    Quartos quarto = new Quartos();
                    Fotosquartos fotos = new Fotosquartos();

                    quarto.setBloco(bloco.getText());
                    quarto.setTiposdequartosidTiposdequartos(tipo.getSelectionModel().getSelectedItem());
                    quarto.setRedeDeHoteisidRedeDeHoteis(MenuController.rede);

                    jpaQuartos.create(quarto);

                    for (int i = 0; i < list.getItems().size(); i++) {
                        fotos.setFotos(list.getItems().get(i));
                        fotos.setQuartosidQuartos(quarto);

                        jpaFotos.create(fotos);
                    }
                    JOptionPane.showMessageDialog(null, "Quarto cadastrado com sucesso!");
                } else {
                    Quartos quarto = GerencQuartosController.quart;

                    quarto.setBloco(bloco.getText());
                    quarto.setTiposdequartosidTiposdequartos(tipo.getSelectionModel().getSelectedItem());
                    quarto.setRedeDeHoteisidRedeDeHoteis(MenuController.rede);

                    try {
                        jpaQuartos.edit(quarto);
                    } catch (NonexistentEntityException ex) {
                        Logger.getLogger(CadQuartosController.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (Exception ex) {
                        Logger.getLogger(CadQuartosController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    Fotosquartos fotos = new Fotosquartos();
                    for (int i = 0; i < list.getItems().size(); i++) {
                        Fotosquartos fot = jpaFotos.findByUrl(quarto, list.getItems().get(i));
                        if (fot == null) {
                            fotos.setFotos(list.getItems().get(i));
                            fotos.setQuartosidQuartos(quarto);
                            jpaFotos.create(fotos);
                        }
                    }
                    JOptionPane.showMessageDialog(null, "Quarto editado com sucesso!");
                }
                try {
                    new GerencQuartos().start(new Stage());
                    CadQuartos.cadquartos.close();
                } catch (IOException ex) {
                    Logger.getLogger(CadQuartosController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        voltar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                try {
                    new GerencQuartos().start(new Stage());
                    CadQuartos.cadquartos.close();
                } catch (IOException ex) {
                    Logger.getLogger(CadQuartosController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }
}
