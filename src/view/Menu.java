/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Augusto
 */
public class Menu extends Application {
    
    public static Stage MyStage;
    
    @Override
    public void start(Stage primaryStage) throws IOException {      
        Parent root = FXMLLoader.load(Menu.class.getResource("Menu.fxml"));
        
        Scene scene = new Scene(root);
        
        this.MyStage = primaryStage;
        
        primaryStage.setTitle("Tela Principal");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}