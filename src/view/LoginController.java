/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import dao.FuncionariosJpaController;
import dao.PessoaJpaController;
import domain.Funcionarios;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import domain.Pessoa;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javax.imageio.ImageIO;
import static javax.imageio.ImageIO.createImageInputStream;
import static javax.imageio.ImageIO.read;
import javax.imageio.stream.ImageInputStream;

/**
 * FXML Controller class
 *
 * @author Augusto
 */
public class LoginController implements Initializable {

    @FXML
    private TextField usuario;

    @FXML
    private PasswordField senha;

    @FXML
    private Button entrar;

    @FXML
    private Button sair;

    @FXML
    private ImageView logo;

    @FXML
    private ImageView foto;

    FuncionariosJpaController jpaFunc = new FuncionariosJpaController();
    PessoaJpaController jpaPessoa = new PessoaJpaController();
    public static String user;
    public static Image imagem;
    private String senh;

    boolean ativo;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        sair.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.exit(0);
            }
        });

        entrar.setDisable(true);

        Image img = new Image("file:C:\\Users\\Augusto\\Downloads\\datafiles_v2\\datafiles\\Secao1\\datafiles\\Logo\\super_black.jpg");
        logo.setImage(img);

        usuario.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                String email = usuario.getText();
                if (verifica(email)) {//Faz a vericação se o email digitado é válido
                    List<Pessoa> lista = jpaPessoa.findByEmail(email);//Recupera uma List do metódo findByEmail

                    for (int i = 0; i < lista.size(); i++) {
                        Pessoa login = new Pessoa();
                        login = lista.get(i);//Seta o login com o resultado da posição 'i'

                        List<Funcionarios> listaFunc = jpaFunc.findByPessoa(login);

                        for (Funcionarios func : listaFunc) {
                            senh = func.getSenha();

                            user = login.getNomePessoa();//Retorna o Nome da Pessoa que será usado no Menu

                            entrar.setDisable(false);

                            if (func.getAtivo()) {
                                ativo = true;
                            } else {
                                ativo = false;
                            }
                        }

                        final String password = senh;

                        imagem = new Image("file:" + login.getFoto());//Cria uma imagem de acordo com o URL que está no banco
                        foto.setImage(imagem);//Seta o ImageView com a imagem que recuperei

                        if (ativo) {
                            entrar.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    try {
                                        if (password.equals(senha.getText())) {
                                            new Menu().start(new Stage());
                                            MainLogin.login.close();
                                        } else {
                                            JOptionPane.showMessageDialog(null, "Senha incorreta!");
                                        }
                                    } catch (IOException ex) {
                                        Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                }
                            });
                        }else{
                            JOptionPane.showMessageDialog(null, "Funcionário inativo!");
                            entrar.setDisable(true);
                        }
                    }
                    if (lista.isEmpty()) {//Se não retornar nada na lista com o Query que executa é pq o email está errado
                        JOptionPane.showMessageDialog(null, "Email incorreto!");
                    }
                }
            }
        });
    }

    public boolean verifica(String email) {

        /*O que entendi do que li:
        ^ = início do campo;
        [...]cria um conjunto;
        \\D = qualquer número;
        && = mesma coisa que em if;
        \\S = Qualquer tipo de espaço(tab ele não deixaria também);
        {1} = Verifica o primeiro digito;
        * = Qualquer coisa;
        (@) = Tem que ter um @ no text;
        * = Qualquer coisa;
        (.com) = Teria que ter o .com;
        $ = final da string*/
        Pattern padrao = Pattern.compile("[a-z][a-z0-9]+@[a-z0-9]+\\.com$");
//        Pattern padrao = Pattern.compile("^[\\D&&\\S]*@*\\.com$");
        Matcher m = padrao.matcher(email);

        return m.matches();
    }
}
