/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import dao.FotosquartosJpaController;
import dao.QuartosJpaController;
import dao.TiposDeQuartosJpaController;
import dao.exceptions.IllegalOrphanException;
import dao.exceptions.NonexistentEntityException;
import domain.Fotosquartos;
import domain.Quartos;
import domain.TiposDeQuartos;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import view.lista.ListaQuartos;

/**
 * FXML Controller class
 *
 * @author Augusto
 */
public class GerencQuartosController implements Initializable {

    @FXML
    private ImageView logo;

    @FXML
    private TableView<ListaQuartos> tabela;

    @FXML
    private TableColumn<ListaQuartos, Integer> numero;

    @FXML
    private TableColumn<ListaQuartos, String> bloco;

    @FXML
    private TableColumn<ListaQuartos, String> tipo;

    @FXML
    private TableColumn<ListaQuartos, BigDecimal> preco;

    @FXML
    private Button novo;

    @FXML
    private Button voltar;

    @FXML
    private Button excluir;

    //JPA's
    QuartosJpaController jpaQuartos = new QuartosJpaController();
    FotosquartosJpaController jpaFotos = new FotosquartosJpaController();
    ObservableList<ListaQuartos> data = FXCollections.observableArrayList();
    
    static Quartos quart;
    static List<Fotosquartos> ListaFotos;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Image img = new Image("file:C:\\Users\\Augusto\\Downloads\\datafiles_v2\\datafiles\\Secao1\\datafiles\\Logo\\super_black.jpg");
        logo.setImage(img);

        populaTabela();

        novo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                try {
                    GerencQuartos.quartos.close();
                    new CadQuartos().start(new Stage());
                } catch (IOException ex) {
                    Logger.getLogger(GerencQuartosController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        excluir.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                int id = tabela.getSelectionModel().getSelectedItem().getNumero();
                Quartos q = jpaQuartos.findQuartos(id);
                List<Fotosquartos> listafot = jpaFotos.findByQuarto(q);

                int resposta = JOptionPane.showConfirmDialog(null, "Você realmente deseja excluir o quarto " + id + "?");
                if (resposta == 0) {
                    try {
                        for (Fotosquartos fot : listafot) {
                            jpaFotos.destroy(fot.getIdFotosQuartos());
                        }
                        jpaQuartos.destroy(q.getIdQuartos());
                    } catch (IllegalOrphanException ex) {
                        Logger.getLogger(GerencQuartosController.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (NonexistentEntityException ex) {
                        Logger.getLogger(GerencQuartosController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    JOptionPane.showMessageDialog(null, "Quarto excluído com sucesso!");
                    populaTabela();
                }
            }
        });

        voltar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                try {
                    GerencQuartos.quartos.close();
                    new Menu().start(new Stage());
                } catch (IOException ex) {
                    Logger.getLogger(GerencQuartosController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        tabela.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                if (e.getClickCount() > 1) {
                    int id = tabela.getSelectionModel().getSelectedItem().getNumero();
                    quart = jpaQuartos.findQuartos(id);
                    ListaFotos = jpaFotos.findByQuarto(quart);
                    try {
                        GerencQuartos.quartos.close();
                        new CadQuartos().start(new Stage());
                    } catch (IOException ex) {
                        Logger.getLogger(GerencQuartosController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    }

    public void populaTabela() {
        tabela.getItems().clear();
        List<Quartos> lista = jpaQuartos.findQuartosEntities();

        for (Quartos quarto : lista) {
            TiposDeQuartos tipos = quarto.getTiposdequartosidTiposdequartos();

            String bloco2 = quarto.getBloco();
            Integer numero2 = quarto.getIdQuartos();

            String tipo2 = "";
            BigDecimal precoQuartos = null;

            tipo2 = tipos.getNome();
            precoQuartos = tipos.getPreco();

            data.add(new ListaQuartos(numero2, bloco2, tipo2, precoQuartos));
        }
        tabela.setItems(data);

        numero.setCellValueFactory(cell -> cell.getValue().getNumeroProperty().asObject());
        bloco.setCellValueFactory(cell -> cell.getValue().getBlocoProperty());
        tipo.setCellValueFactory(cell -> cell.getValue().getTipoProperty());
        preco.setCellValueFactory(cell -> cell.getValue().getPrecoProperty());
    }
}
