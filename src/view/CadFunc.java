/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Augusto
 */
public class CadFunc extends Application {
    
    public static Stage Cad = new Stage();
    
    @Override
    public void start(Stage primaryStage) throws IOException {
        
        Cad = primaryStage;
        
        Parent root = FXMLLoader.load(CadFunc.class.getResource("CadFunc.fxml"));
        
        Scene scene = new Scene(root);
        
        primaryStage.setTitle("Cadastro de Funcioarios");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
