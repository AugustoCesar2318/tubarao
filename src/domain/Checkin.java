/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Augusto
 */
@Entity
@Table(name = "checkin")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Checkin.findAll", query = "SELECT c FROM Checkin c")
    , @NamedQuery(name = "Checkin.findByIdCheckIn", query = "SELECT c FROM Checkin c WHERE c.idCheckIn = :idCheckIn")
    , @NamedQuery(name = "Checkin.findByValor", query = "SELECT c FROM Checkin c WHERE c.valor = :valor")})
public class Checkin implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idCheckIn")
    private Integer idCheckIn;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Valor")
    private BigDecimal valor;
    @JoinColumn(name = "Reserva_idReseva", referencedColumnName = "idReseva")
    @ManyToOne(optional = false)
    private Reserva reservaidReseva;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "checkInidCheckIn")
    private Collection<Checkout> checkoutCollection;

    public Checkin() {
    }

    public Checkin(Integer idCheckIn) {
        this.idCheckIn = idCheckIn;
    }

    public Integer getIdCheckIn() {
        return idCheckIn;
    }

    public void setIdCheckIn(Integer idCheckIn) {
        this.idCheckIn = idCheckIn;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Reserva getReservaidReseva() {
        return reservaidReseva;
    }

    public void setReservaidReseva(Reserva reservaidReseva) {
        this.reservaidReseva = reservaidReseva;
    }

    @XmlTransient
    public Collection<Checkout> getCheckoutCollection() {
        return checkoutCollection;
    }

    public void setCheckoutCollection(Collection<Checkout> checkoutCollection) {
        this.checkoutCollection = checkoutCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCheckIn != null ? idCheckIn.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Checkin)) {
            return false;
        }
        Checkin other = (Checkin) object;
        if ((this.idCheckIn == null && other.idCheckIn != null) || (this.idCheckIn != null && !this.idCheckIn.equals(other.idCheckIn))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "domain.Checkin[ idCheckIn=" + idCheckIn + " ]";
    }
    
}
