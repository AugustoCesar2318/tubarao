/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Augusto
 */
@Entity
@Table(name = "depesas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Depesas.findAll", query = "SELECT d FROM Depesas d")
    , @NamedQuery(name = "Depesas.findByIdDepesas", query = "SELECT d FROM Depesas d WHERE d.idDepesas = :idDepesas")
    , @NamedQuery(name = "Depesas.findByValor", query = "SELECT d FROM Depesas d WHERE d.valor = :valor")
    , @NamedQuery(name = "Depesas.findByDataReceita", query = "SELECT d FROM Depesas d WHERE d.dataReceita = :dataReceita")
    , @NamedQuery(name = "Depesas.findByTipo", query = "SELECT d FROM Depesas d WHERE d.tipo = :tipo")})
public class Depesas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idDepesas")
    private Integer idDepesas;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Valor")
    private BigDecimal valor;
    @Column(name = "DataReceita")
    @Temporal(TemporalType.DATE)
    private Date dataReceita;
    @Column(name = "Tipo")
    private String tipo;
    @Lob
    @Column(name = "Explicacao")
    private String explicacao;
    @JoinColumn(name = "Funcionarios_idFuncionarios", referencedColumnName = "idFuncionarios")
    @ManyToOne(optional = false)
    private Funcionarios funcionariosidFuncionarios;

    public Depesas() {
    }

    public Depesas(Integer idDepesas) {
        this.idDepesas = idDepesas;
    }

    public Integer getIdDepesas() {
        return idDepesas;
    }

    public void setIdDepesas(Integer idDepesas) {
        this.idDepesas = idDepesas;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Date getDataReceita() {
        return dataReceita;
    }

    public void setDataReceita(Date dataReceita) {
        this.dataReceita = dataReceita;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getExplicacao() {
        return explicacao;
    }

    public void setExplicacao(String explicacao) {
        this.explicacao = explicacao;
    }

    public Funcionarios getFuncionariosidFuncionarios() {
        return funcionariosidFuncionarios;
    }

    public void setFuncionariosidFuncionarios(Funcionarios funcionariosidFuncionarios) {
        this.funcionariosidFuncionarios = funcionariosidFuncionarios;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDepesas != null ? idDepesas.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Depesas)) {
            return false;
        }
        Depesas other = (Depesas) object;
        if ((this.idDepesas == null && other.idDepesas != null) || (this.idDepesas != null && !this.idDepesas.equals(other.idDepesas))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "domain.Depesas[ idDepesas=" + idDepesas + " ]";
    }
    
}
