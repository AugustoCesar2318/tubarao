/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Augusto
 */
@Entity
@Table(name = "fotosquartos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Fotosquartos.findAll", query = "SELECT f FROM Fotosquartos f")
    , @NamedQuery(name = "Fotosquartos.findByIdFotosQuartos", query = "SELECT f FROM Fotosquartos f WHERE f.idFotosQuartos = :idFotosQuartos")
    , @NamedQuery(name = "Fotosquartos.findByFotos", query = "SELECT f FROM Fotosquartos f WHERE f.fotos = :fotos")})
public class Fotosquartos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idFotosQuartos")
    private Integer idFotosQuartos;
    @Column(name = "Fotos")
    private String fotos;
    @JoinColumn(name = "Quartos_idQuartos", referencedColumnName = "idQuartos")
    @ManyToOne(optional = false)
    private Quartos quartosidQuartos;

    public Fotosquartos() {
    }

    public Fotosquartos(Integer idFotosQuartos) {
        this.idFotosQuartos = idFotosQuartos;
    }

    public Integer getIdFotosQuartos() {
        return idFotosQuartos;
    }

    public void setIdFotosQuartos(Integer idFotosQuartos) {
        this.idFotosQuartos = idFotosQuartos;
    }

    public String getFotos() {
        return fotos;
    }

    public void setFotos(String fotos) {
        this.fotos = fotos;
    }

    public Quartos getQuartosidQuartos() {
        return quartosidQuartos;
    }

    public void setQuartosidQuartos(Quartos quartosidQuartos) {
        this.quartosidQuartos = quartosidQuartos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFotosQuartos != null ? idFotosQuartos.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Fotosquartos)) {
            return false;
        }
        Fotosquartos other = (Fotosquartos) object;
        if ((this.idFotosQuartos == null && other.idFotosQuartos != null) || (this.idFotosQuartos != null && !this.idFotosQuartos.equals(other.idFotosQuartos))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "domain.Fotosquartos[ idFotosQuartos=" + idFotosQuartos + " ]";
    }
    
}
