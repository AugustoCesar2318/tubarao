/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Augusto
 */
@Entity
@Table(name = "reserva")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reserva.findAll", query = "SELECT r FROM Reserva r")
    , @NamedQuery(name = "Reserva.findByIdReseva", query = "SELECT r FROM Reserva r WHERE r.idReseva = :idReseva")
    , @NamedQuery(name = "Reserva.findByEntrada", query = "SELECT r FROM Reserva r WHERE r.entrada = :entrada")
    , @NamedQuery(name = "Reserva.findBySaida", query = "SELECT r FROM Reserva r WHERE r.saida = :saida")
    , @NamedQuery(name = "Reserva.findByReservacol", query = "SELECT r FROM Reserva r WHERE r.reservacol = :reservacol")
    , @NamedQuery(name = "Reserva.findByFormaDePagamento", query = "SELECT r FROM Reserva r WHERE r.formaDePagamento = :formaDePagamento")
    , @NamedQuery(name = "Reserva.findByHoraParaCheckIn", query = "SELECT r FROM Reserva r WHERE r.horaParaCheckIn = :horaParaCheckIn")})
public class Reserva implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idReseva")
    private Integer idReseva;
    @Column(name = "Entrada")
    @Temporal(TemporalType.DATE)
    private Date entrada;
    @Column(name = "Saida")
    @Temporal(TemporalType.DATE)
    private Date saida;
    @Column(name = "Reservacol")
    private String reservacol;
    @Column(name = "FormaDePagamento")
    private String formaDePagamento;
    @Column(name = "HoraParaCheckIn")
    @Temporal(TemporalType.TIME)
    private Date horaParaCheckIn;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reservaidReseva")
    private Collection<Checkin> checkinCollection;
    @JoinColumn(name = "Funcionarios_idFuncionarios", referencedColumnName = "idFuncionarios")
    @ManyToOne(optional = false)
    private Funcionarios funcionariosidFuncionarios;
    @JoinColumn(name = "Hospede_idHospede", referencedColumnName = "idHospede")
    @ManyToOne(optional = false)
    private Hospede hospedeidHospede;
    @JoinColumn(name = "Quartos_idQuartos", referencedColumnName = "idQuartos")
    @ManyToOne(optional = false)
    private Quartos quartosidQuartos;

    public Reserva() {
    }

    public Reserva(Integer idReseva) {
        this.idReseva = idReseva;
    }

    public Integer getIdReseva() {
        return idReseva;
    }

    public void setIdReseva(Integer idReseva) {
        this.idReseva = idReseva;
    }

    public Date getEntrada() {
        return entrada;
    }

    public void setEntrada(Date entrada) {
        this.entrada = entrada;
    }

    public Date getSaida() {
        return saida;
    }

    public void setSaida(Date saida) {
        this.saida = saida;
    }

    public String getReservacol() {
        return reservacol;
    }

    public void setReservacol(String reservacol) {
        this.reservacol = reservacol;
    }

    public String getFormaDePagamento() {
        return formaDePagamento;
    }

    public void setFormaDePagamento(String formaDePagamento) {
        this.formaDePagamento = formaDePagamento;
    }

    public Date getHoraParaCheckIn() {
        return horaParaCheckIn;
    }

    public void setHoraParaCheckIn(Date horaParaCheckIn) {
        this.horaParaCheckIn = horaParaCheckIn;
    }

    @XmlTransient
    public Collection<Checkin> getCheckinCollection() {
        return checkinCollection;
    }

    public void setCheckinCollection(Collection<Checkin> checkinCollection) {
        this.checkinCollection = checkinCollection;
    }

    public Funcionarios getFuncionariosidFuncionarios() {
        return funcionariosidFuncionarios;
    }

    public void setFuncionariosidFuncionarios(Funcionarios funcionariosidFuncionarios) {
        this.funcionariosidFuncionarios = funcionariosidFuncionarios;
    }

    public Hospede getHospedeidHospede() {
        return hospedeidHospede;
    }

    public void setHospedeidHospede(Hospede hospedeidHospede) {
        this.hospedeidHospede = hospedeidHospede;
    }

    public Quartos getQuartosidQuartos() {
        return quartosidQuartos;
    }

    public void setQuartosidQuartos(Quartos quartosidQuartos) {
        this.quartosidQuartos = quartosidQuartos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idReseva != null ? idReseva.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reserva)) {
            return false;
        }
        Reserva other = (Reserva) object;
        if ((this.idReseva == null && other.idReseva != null) || (this.idReseva != null && !this.idReseva.equals(other.idReseva))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "domain.Reserva[ idReseva=" + idReseva + " ]";
    }
    
}
