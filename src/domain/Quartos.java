/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Augusto
 */
@Entity
@Table(name = "quartos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Quartos.findAll", query = "SELECT q FROM Quartos q")
    , @NamedQuery(name = "Quartos.findByIdQuartos", query = "SELECT q FROM Quartos q WHERE q.idQuartos = :idQuartos")
    , @NamedQuery(name = "Quartos.findByBloco", query = "SELECT q FROM Quartos q WHERE q.bloco = :bloco")})
public class Quartos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idQuartos")
    private Integer idQuartos;
    @Basic(optional = false)
    @Column(name = "Bloco")
    private String bloco;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "quartosidQuartos")
    private Collection<Fotosquartos> fotosquartosCollection;
    @JoinColumn(name = "RedeDeHoteis_idRedeDeHoteis", referencedColumnName = "idRedeDeHoteis")
    @ManyToOne(optional = false)
    private Rededehoteis redeDeHoteisidRedeDeHoteis;
    @JoinColumn(name = "Tipos_de_quartos_idTipos_de_quartos", referencedColumnName = "idTipos_de_quartos")
    @ManyToOne(optional = false)
    private TiposDeQuartos tiposdequartosidTiposdequartos;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "quartosidQuartos")
    private Collection<Reserva> reservaCollection;

    public Quartos() {
    }

    public Quartos(Integer idQuartos) {
        this.idQuartos = idQuartos;
    }

    public Quartos(Integer idQuartos, String bloco) {
        this.idQuartos = idQuartos;
        this.bloco = bloco;
    }

    public Integer getIdQuartos() {
        return idQuartos;
    }

    public void setIdQuartos(Integer idQuartos) {
        this.idQuartos = idQuartos;
    }

    public String getBloco() {
        return bloco;
    }

    public void setBloco(String bloco) {
        this.bloco = bloco;
    }

    @XmlTransient
    public Collection<Fotosquartos> getFotosquartosCollection() {
        return fotosquartosCollection;
    }

    public void setFotosquartosCollection(Collection<Fotosquartos> fotosquartosCollection) {
        this.fotosquartosCollection = fotosquartosCollection;
    }

    public Rededehoteis getRedeDeHoteisidRedeDeHoteis() {
        return redeDeHoteisidRedeDeHoteis;
    }

    public void setRedeDeHoteisidRedeDeHoteis(Rededehoteis redeDeHoteisidRedeDeHoteis) {
        this.redeDeHoteisidRedeDeHoteis = redeDeHoteisidRedeDeHoteis;
    }

    public TiposDeQuartos getTiposdequartosidTiposdequartos() {
        return tiposdequartosidTiposdequartos;
    }

    public void setTiposdequartosidTiposdequartos(TiposDeQuartos tiposdequartosidTiposdequartos) {
        this.tiposdequartosidTiposdequartos = tiposdequartosidTiposdequartos;
    }

    @XmlTransient
    public Collection<Reserva> getReservaCollection() {
        return reservaCollection;
    }

    public void setReservaCollection(Collection<Reserva> reservaCollection) {
        this.reservaCollection = reservaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idQuartos != null ? idQuartos.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Quartos)) {
            return false;
        }
        Quartos other = (Quartos) object;
        if ((this.idQuartos == null && other.idQuartos != null) || (this.idQuartos != null && !this.idQuartos.equals(other.idQuartos))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "domain.Quartos[ idQuartos=" + idQuartos + " ]";
    }
    
}
