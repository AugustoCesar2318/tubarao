/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Augusto
 */
@Entity
@Table(name = "funcionarios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Funcionarios.findAll", query = "SELECT f FROM Funcionarios f")
    , @NamedQuery(name = "Funcionarios.findByIdFuncionarios", query = "SELECT f FROM Funcionarios f WHERE f.idFuncionarios = :idFuncionarios")
    , @NamedQuery(name = "Funcionarios.findBySenha", query = "SELECT f FROM Funcionarios f WHERE f.senha = :senha")
    , @NamedQuery(name = "Funcionarios.findByAtivo", query = "SELECT f FROM Funcionarios f WHERE f.ativo = :ativo")})
public class Funcionarios implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idFuncionarios")
    private Integer idFuncionarios;
    @Column(name = "Senha")
    private String senha;
    @Column(name = "Ativo")
    private Boolean ativo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "funcionariosidFuncionarios")
    private Collection<Depesas> depesasCollection;
    @JoinColumn(name = "Pessoa_idPessoa", referencedColumnName = "idPessoa")
    @ManyToOne(optional = false)
    private Pessoa pessoaidPessoa;
    @JoinColumn(name = "RedeDeHoteis_idRedeDeHoteis", referencedColumnName = "idRedeDeHoteis")
    @ManyToOne(optional = false)
    private Rededehoteis redeDeHoteisidRedeDeHoteis;
    @JoinColumn(name = "Tipo_Funcionario_idTipo_Funcionario", referencedColumnName = "idTipo_Funcionario")
    @ManyToOne(optional = false)
    private TipoFuncionario tipoFuncionarioidTipoFuncionario;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "funcionariosidFuncionarios")
    private Collection<Reserva> reservaCollection;

    public Funcionarios() {
    }

    public Funcionarios(Integer idFuncionarios) {
        this.idFuncionarios = idFuncionarios;
    }

    public Integer getIdFuncionarios() {
        return idFuncionarios;
    }

    public void setIdFuncionarios(Integer idFuncionarios) {
        this.idFuncionarios = idFuncionarios;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    @XmlTransient
    public Collection<Depesas> getDepesasCollection() {
        return depesasCollection;
    }

    public void setDepesasCollection(Collection<Depesas> depesasCollection) {
        this.depesasCollection = depesasCollection;
    }

    public Pessoa getPessoaidPessoa() {
        return pessoaidPessoa;
    }

    public void setPessoaidPessoa(Pessoa pessoaidPessoa) {
        this.pessoaidPessoa = pessoaidPessoa;
    }

    public Rededehoteis getRedeDeHoteisidRedeDeHoteis() {
        return redeDeHoteisidRedeDeHoteis;
    }

    public void setRedeDeHoteisidRedeDeHoteis(Rededehoteis redeDeHoteisidRedeDeHoteis) {
        this.redeDeHoteisidRedeDeHoteis = redeDeHoteisidRedeDeHoteis;
    }

    public TipoFuncionario getTipoFuncionarioidTipoFuncionario() {
        return tipoFuncionarioidTipoFuncionario;
    }

    public void setTipoFuncionarioidTipoFuncionario(TipoFuncionario tipoFuncionarioidTipoFuncionario) {
        this.tipoFuncionarioidTipoFuncionario = tipoFuncionarioidTipoFuncionario;
    }

    @XmlTransient
    public Collection<Reserva> getReservaCollection() {
        return reservaCollection;
    }

    public void setReservaCollection(Collection<Reserva> reservaCollection) {
        this.reservaCollection = reservaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFuncionarios != null ? idFuncionarios.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Funcionarios)) {
            return false;
        }
        Funcionarios other = (Funcionarios) object;
        if ((this.idFuncionarios == null && other.idFuncionarios != null) || (this.idFuncionarios != null && !this.idFuncionarios.equals(other.idFuncionarios))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "domain.Funcionarios[ idFuncionarios=" + idFuncionarios + " ]";
    }
    
}
