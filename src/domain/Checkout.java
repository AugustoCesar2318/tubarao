/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Augusto
 */
@Entity
@Table(name = "checkout")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Checkout.findAll", query = "SELECT c FROM Checkout c")
    , @NamedQuery(name = "Checkout.findByIdCheckOut", query = "SELECT c FROM Checkout c WHERE c.idCheckOut = :idCheckOut")
    , @NamedQuery(name = "Checkout.findBySaida", query = "SELECT c FROM Checkout c WHERE c.saida = :saida")})
public class Checkout implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idCheckOut")
    private Integer idCheckOut;
    @Column(name = "Saida")
    @Temporal(TemporalType.TIMESTAMP)
    private Date saida;
    @Lob
    @Column(name = "Obseracao")
    private String obseracao;
    @JoinColumn(name = "CheckIn_idCheckIn", referencedColumnName = "idCheckIn")
    @ManyToOne(optional = false)
    private Checkin checkInidCheckIn;

    public Checkout() {
    }

    public Checkout(Integer idCheckOut) {
        this.idCheckOut = idCheckOut;
    }

    public Integer getIdCheckOut() {
        return idCheckOut;
    }

    public void setIdCheckOut(Integer idCheckOut) {
        this.idCheckOut = idCheckOut;
    }

    public Date getSaida() {
        return saida;
    }

    public void setSaida(Date saida) {
        this.saida = saida;
    }

    public String getObseracao() {
        return obseracao;
    }

    public void setObseracao(String obseracao) {
        this.obseracao = obseracao;
    }

    public Checkin getCheckInidCheckIn() {
        return checkInidCheckIn;
    }

    public void setCheckInidCheckIn(Checkin checkInidCheckIn) {
        this.checkInidCheckIn = checkInidCheckIn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCheckOut != null ? idCheckOut.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Checkout)) {
            return false;
        }
        Checkout other = (Checkout) object;
        if ((this.idCheckOut == null && other.idCheckOut != null) || (this.idCheckOut != null && !this.idCheckOut.equals(other.idCheckOut))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "domain.Checkout[ idCheckOut=" + idCheckOut + " ]";
    }
    
}
