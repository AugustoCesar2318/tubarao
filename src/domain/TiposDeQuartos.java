/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Augusto
 */
@Entity
@Table(name = "tipos_de_quartos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TiposDeQuartos.findAll", query = "SELECT t FROM TiposDeQuartos t")
    , @NamedQuery(name = "TiposDeQuartos.findByIdTiposdequartos", query = "SELECT t FROM TiposDeQuartos t WHERE t.idTiposdequartos = :idTiposdequartos")
    , @NamedQuery(name = "TiposDeQuartos.findByNome", query = "SELECT t FROM TiposDeQuartos t WHERE t.nome = :nome")
    , @NamedQuery(name = "TiposDeQuartos.findByPreco", query = "SELECT t FROM TiposDeQuartos t WHERE t.preco = :preco")})
public class TiposDeQuartos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "idTipos_de_quartos")
    private Integer idTiposdequartos;
    @Basic(optional = false)
    @Column(name = "Nome")
    private String nome;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "preco")
    private BigDecimal preco;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tiposdequartosidTiposdequartos")
    private Collection<Quartos> quartosCollection;

    public TiposDeQuartos() {
    }

    public TiposDeQuartos(Integer idTiposdequartos) {
        this.idTiposdequartos = idTiposdequartos;
    }

    public TiposDeQuartos(Integer idTiposdequartos, String nome, BigDecimal preco) {
        this.idTiposdequartos = idTiposdequartos;
        this.nome = nome;
        this.preco = preco;
    }

    public Integer getIdTiposdequartos() {
        return idTiposdequartos;
    }

    public void setIdTiposdequartos(Integer idTiposdequartos) {
        this.idTiposdequartos = idTiposdequartos;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public BigDecimal getPreco() {
        return preco;
    }

    public void setPreco(BigDecimal preco) {
        this.preco = preco;
    }

    @XmlTransient
    public Collection<Quartos> getQuartosCollection() {
        return quartosCollection;
    }

    public void setQuartosCollection(Collection<Quartos> quartosCollection) {
        this.quartosCollection = quartosCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTiposdequartos != null ? idTiposdequartos.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TiposDeQuartos)) {
            return false;
        }
        TiposDeQuartos other = (TiposDeQuartos) object;
        if ((this.idTiposdequartos == null && other.idTiposdequartos != null) || (this.idTiposdequartos != null && !this.idTiposdequartos.equals(other.idTiposdequartos))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nome;
    }
    
}
