/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Augusto
 */
@Entity
@Table(name = "rededehoteis")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Rededehoteis.findAll", query = "SELECT r FROM Rededehoteis r")
    , @NamedQuery(name = "Rededehoteis.findByIdRedeDeHoteis", query = "SELECT r FROM Rededehoteis r WHERE r.idRedeDeHoteis = :idRedeDeHoteis")
    , @NamedQuery(name = "Rededehoteis.findByNomeHotel", query = "SELECT r FROM Rededehoteis r WHERE r.nomeHotel = :nomeHotel")
    , @NamedQuery(name = "Rededehoteis.findByRua", query = "SELECT r FROM Rededehoteis r WHERE r.rua = :rua")
    , @NamedQuery(name = "Rededehoteis.findByNumero", query = "SELECT r FROM Rededehoteis r WHERE r.numero = :numero")
    , @NamedQuery(name = "Rededehoteis.findByComplemento", query = "SELECT r FROM Rededehoteis r WHERE r.complemento = :complemento")
    , @NamedQuery(name = "Rededehoteis.findByBairro", query = "SELECT r FROM Rededehoteis r WHERE r.bairro = :bairro")
    , @NamedQuery(name = "Rededehoteis.findByCep", query = "SELECT r FROM Rededehoteis r WHERE r.cep = :cep")
    , @NamedQuery(name = "Rededehoteis.findByEstrelas", query = "SELECT r FROM Rededehoteis r WHERE r.estrelas = :estrelas")})
public class Rededehoteis implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idRedeDeHoteis")
    private Integer idRedeDeHoteis;
    @Basic(optional = false)
    @Column(name = "NomeHotel")
    private String nomeHotel;
    @Column(name = "Rua")
    private String rua;
    @Column(name = "Numero")
    private Integer numero;
    @Column(name = "Complemento")
    private String complemento;
    @Column(name = "Bairro")
    private String bairro;
    @Column(name = "CEP")
    private String cep;
    @Lob
    @Column(name = "Foto")
    private byte[] foto;
    @Basic(optional = false)
    @Column(name = "Estrelas")
    private String estrelas;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "redeDeHoteisidRedeDeHoteis")
    private Collection<Telefone> telefoneCollection;
    @JoinColumn(name = "Cidade_idCidade", referencedColumnName = "idCidade")
    @ManyToOne(optional = false)
    private Cidade cidadeidCidade;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "redeDeHoteisidRedeDeHoteis")
    private Collection<Quartos> quartosCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "redeDeHoteisidRedeDeHoteis")
    private Collection<Funcionarios> funcionariosCollection;

    public Rededehoteis() {
    }

    public Rededehoteis(Integer idRedeDeHoteis) {
        this.idRedeDeHoteis = idRedeDeHoteis;
    }

    public Rededehoteis(Integer idRedeDeHoteis, String nomeHotel, String estrelas) {
        this.idRedeDeHoteis = idRedeDeHoteis;
        this.nomeHotel = nomeHotel;
        this.estrelas = estrelas;
    }

    public Integer getIdRedeDeHoteis() {
        return idRedeDeHoteis;
    }

    public void setIdRedeDeHoteis(Integer idRedeDeHoteis) {
        this.idRedeDeHoteis = idRedeDeHoteis;
    }

    public String getNomeHotel() {
        return nomeHotel;
    }

    public void setNomeHotel(String nomeHotel) {
        this.nomeHotel = nomeHotel;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    public String getEstrelas() {
        return estrelas;
    }

    public void setEstrelas(String estrelas) {
        this.estrelas = estrelas;
    }

    @XmlTransient
    public Collection<Telefone> getTelefoneCollection() {
        return telefoneCollection;
    }

    public void setTelefoneCollection(Collection<Telefone> telefoneCollection) {
        this.telefoneCollection = telefoneCollection;
    }

    public Cidade getCidadeidCidade() {
        return cidadeidCidade;
    }

    public void setCidadeidCidade(Cidade cidadeidCidade) {
        this.cidadeidCidade = cidadeidCidade;
    }

    @XmlTransient
    public Collection<Quartos> getQuartosCollection() {
        return quartosCollection;
    }

    public void setQuartosCollection(Collection<Quartos> quartosCollection) {
        this.quartosCollection = quartosCollection;
    }

    @XmlTransient
    public Collection<Funcionarios> getFuncionariosCollection() {
        return funcionariosCollection;
    }

    public void setFuncionariosCollection(Collection<Funcionarios> funcionariosCollection) {
        this.funcionariosCollection = funcionariosCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRedeDeHoteis != null ? idRedeDeHoteis.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rededehoteis)) {
            return false;
        }
        Rededehoteis other = (Rededehoteis) object;
        if ((this.idRedeDeHoteis == null && other.idRedeDeHoteis != null) || (this.idRedeDeHoteis != null && !this.idRedeDeHoteis.equals(other.idRedeDeHoteis))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nomeHotel;
    }
    
}
