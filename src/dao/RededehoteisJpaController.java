/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.IllegalOrphanException;
import dao.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import domain.Cidade;
import domain.Telefone;
import java.util.ArrayList;
import java.util.Collection;
import domain.Quartos;
import domain.Funcionarios;
import domain.Rededehoteis;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Augusto
 */
public class RededehoteisJpaController implements Serializable {

    public EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("ProvaTubaraoPU");
        return factory.createEntityManager();
    }

    public void create(Rededehoteis rededehoteis) {
        if (rededehoteis.getTelefoneCollection() == null) {
            rededehoteis.setTelefoneCollection(new ArrayList<Telefone>());
        }
        if (rededehoteis.getQuartosCollection() == null) {
            rededehoteis.setQuartosCollection(new ArrayList<Quartos>());
        }
        if (rededehoteis.getFuncionariosCollection() == null) {
            rededehoteis.setFuncionariosCollection(new ArrayList<Funcionarios>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cidade cidadeidCidade = rededehoteis.getCidadeidCidade();
            if (cidadeidCidade != null) {
                cidadeidCidade = em.getReference(cidadeidCidade.getClass(), cidadeidCidade.getIdCidade());
                rededehoteis.setCidadeidCidade(cidadeidCidade);
            }
            Collection<Telefone> attachedTelefoneCollection = new ArrayList<Telefone>();
            for (Telefone telefoneCollectionTelefoneToAttach : rededehoteis.getTelefoneCollection()) {
                telefoneCollectionTelefoneToAttach = em.getReference(telefoneCollectionTelefoneToAttach.getClass(), telefoneCollectionTelefoneToAttach.getIdTelefone());
                attachedTelefoneCollection.add(telefoneCollectionTelefoneToAttach);
            }
            rededehoteis.setTelefoneCollection(attachedTelefoneCollection);
            Collection<Quartos> attachedQuartosCollection = new ArrayList<Quartos>();
            for (Quartos quartosCollectionQuartosToAttach : rededehoteis.getQuartosCollection()) {
                quartosCollectionQuartosToAttach = em.getReference(quartosCollectionQuartosToAttach.getClass(), quartosCollectionQuartosToAttach.getIdQuartos());
                attachedQuartosCollection.add(quartosCollectionQuartosToAttach);
            }
            rededehoteis.setQuartosCollection(attachedQuartosCollection);
            Collection<Funcionarios> attachedFuncionariosCollection = new ArrayList<Funcionarios>();
            for (Funcionarios funcionariosCollectionFuncionariosToAttach : rededehoteis.getFuncionariosCollection()) {
                funcionariosCollectionFuncionariosToAttach = em.getReference(funcionariosCollectionFuncionariosToAttach.getClass(), funcionariosCollectionFuncionariosToAttach.getIdFuncionarios());
                attachedFuncionariosCollection.add(funcionariosCollectionFuncionariosToAttach);
            }
            rededehoteis.setFuncionariosCollection(attachedFuncionariosCollection);
            em.persist(rededehoteis);
            if (cidadeidCidade != null) {
                cidadeidCidade.getRededehoteisCollection().add(rededehoteis);
                cidadeidCidade = em.merge(cidadeidCidade);
            }
            for (Telefone telefoneCollectionTelefone : rededehoteis.getTelefoneCollection()) {
                Rededehoteis oldRedeDeHoteisidRedeDeHoteisOfTelefoneCollectionTelefone = telefoneCollectionTelefone.getRedeDeHoteisidRedeDeHoteis();
                telefoneCollectionTelefone.setRedeDeHoteisidRedeDeHoteis(rededehoteis);
                telefoneCollectionTelefone = em.merge(telefoneCollectionTelefone);
                if (oldRedeDeHoteisidRedeDeHoteisOfTelefoneCollectionTelefone != null) {
                    oldRedeDeHoteisidRedeDeHoteisOfTelefoneCollectionTelefone.getTelefoneCollection().remove(telefoneCollectionTelefone);
                    oldRedeDeHoteisidRedeDeHoteisOfTelefoneCollectionTelefone = em.merge(oldRedeDeHoteisidRedeDeHoteisOfTelefoneCollectionTelefone);
                }
            }
            for (Quartos quartosCollectionQuartos : rededehoteis.getQuartosCollection()) {
                Rededehoteis oldRedeDeHoteisidRedeDeHoteisOfQuartosCollectionQuartos = quartosCollectionQuartos.getRedeDeHoteisidRedeDeHoteis();
                quartosCollectionQuartos.setRedeDeHoteisidRedeDeHoteis(rededehoteis);
                quartosCollectionQuartos = em.merge(quartosCollectionQuartos);
                if (oldRedeDeHoteisidRedeDeHoteisOfQuartosCollectionQuartos != null) {
                    oldRedeDeHoteisidRedeDeHoteisOfQuartosCollectionQuartos.getQuartosCollection().remove(quartosCollectionQuartos);
                    oldRedeDeHoteisidRedeDeHoteisOfQuartosCollectionQuartos = em.merge(oldRedeDeHoteisidRedeDeHoteisOfQuartosCollectionQuartos);
                }
            }
            for (Funcionarios funcionariosCollectionFuncionarios : rededehoteis.getFuncionariosCollection()) {
                Rededehoteis oldRedeDeHoteisidRedeDeHoteisOfFuncionariosCollectionFuncionarios = funcionariosCollectionFuncionarios.getRedeDeHoteisidRedeDeHoteis();
                funcionariosCollectionFuncionarios.setRedeDeHoteisidRedeDeHoteis(rededehoteis);
                funcionariosCollectionFuncionarios = em.merge(funcionariosCollectionFuncionarios);
                if (oldRedeDeHoteisidRedeDeHoteisOfFuncionariosCollectionFuncionarios != null) {
                    oldRedeDeHoteisidRedeDeHoteisOfFuncionariosCollectionFuncionarios.getFuncionariosCollection().remove(funcionariosCollectionFuncionarios);
                    oldRedeDeHoteisidRedeDeHoteisOfFuncionariosCollectionFuncionarios = em.merge(oldRedeDeHoteisidRedeDeHoteisOfFuncionariosCollectionFuncionarios);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Rededehoteis rededehoteis) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Rededehoteis persistentRededehoteis = em.find(Rededehoteis.class, rededehoteis.getIdRedeDeHoteis());
            Cidade cidadeidCidadeOld = persistentRededehoteis.getCidadeidCidade();
            Cidade cidadeidCidadeNew = rededehoteis.getCidadeidCidade();
            Collection<Telefone> telefoneCollectionOld = persistentRededehoteis.getTelefoneCollection();
            Collection<Telefone> telefoneCollectionNew = rededehoteis.getTelefoneCollection();
            Collection<Quartos> quartosCollectionOld = persistentRededehoteis.getQuartosCollection();
            Collection<Quartos> quartosCollectionNew = rededehoteis.getQuartosCollection();
            Collection<Funcionarios> funcionariosCollectionOld = persistentRededehoteis.getFuncionariosCollection();
            Collection<Funcionarios> funcionariosCollectionNew = rededehoteis.getFuncionariosCollection();
            List<String> illegalOrphanMessages = null;
            for (Telefone telefoneCollectionOldTelefone : telefoneCollectionOld) {
                if (!telefoneCollectionNew.contains(telefoneCollectionOldTelefone)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Telefone " + telefoneCollectionOldTelefone + " since its redeDeHoteisidRedeDeHoteis field is not nullable.");
                }
            }
            for (Quartos quartosCollectionOldQuartos : quartosCollectionOld) {
                if (!quartosCollectionNew.contains(quartosCollectionOldQuartos)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Quartos " + quartosCollectionOldQuartos + " since its redeDeHoteisidRedeDeHoteis field is not nullable.");
                }
            }
            for (Funcionarios funcionariosCollectionOldFuncionarios : funcionariosCollectionOld) {
                if (!funcionariosCollectionNew.contains(funcionariosCollectionOldFuncionarios)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Funcionarios " + funcionariosCollectionOldFuncionarios + " since its redeDeHoteisidRedeDeHoteis field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (cidadeidCidadeNew != null) {
                cidadeidCidadeNew = em.getReference(cidadeidCidadeNew.getClass(), cidadeidCidadeNew.getIdCidade());
                rededehoteis.setCidadeidCidade(cidadeidCidadeNew);
            }
            Collection<Telefone> attachedTelefoneCollectionNew = new ArrayList<Telefone>();
            for (Telefone telefoneCollectionNewTelefoneToAttach : telefoneCollectionNew) {
                telefoneCollectionNewTelefoneToAttach = em.getReference(telefoneCollectionNewTelefoneToAttach.getClass(), telefoneCollectionNewTelefoneToAttach.getIdTelefone());
                attachedTelefoneCollectionNew.add(telefoneCollectionNewTelefoneToAttach);
            }
            telefoneCollectionNew = attachedTelefoneCollectionNew;
            rededehoteis.setTelefoneCollection(telefoneCollectionNew);
            Collection<Quartos> attachedQuartosCollectionNew = new ArrayList<Quartos>();
            for (Quartos quartosCollectionNewQuartosToAttach : quartosCollectionNew) {
                quartosCollectionNewQuartosToAttach = em.getReference(quartosCollectionNewQuartosToAttach.getClass(), quartosCollectionNewQuartosToAttach.getIdQuartos());
                attachedQuartosCollectionNew.add(quartosCollectionNewQuartosToAttach);
            }
            quartosCollectionNew = attachedQuartosCollectionNew;
            rededehoteis.setQuartosCollection(quartosCollectionNew);
            Collection<Funcionarios> attachedFuncionariosCollectionNew = new ArrayList<Funcionarios>();
            for (Funcionarios funcionariosCollectionNewFuncionariosToAttach : funcionariosCollectionNew) {
                funcionariosCollectionNewFuncionariosToAttach = em.getReference(funcionariosCollectionNewFuncionariosToAttach.getClass(), funcionariosCollectionNewFuncionariosToAttach.getIdFuncionarios());
                attachedFuncionariosCollectionNew.add(funcionariosCollectionNewFuncionariosToAttach);
            }
            funcionariosCollectionNew = attachedFuncionariosCollectionNew;
            rededehoteis.setFuncionariosCollection(funcionariosCollectionNew);
            rededehoteis = em.merge(rededehoteis);
            if (cidadeidCidadeOld != null && !cidadeidCidadeOld.equals(cidadeidCidadeNew)) {
                cidadeidCidadeOld.getRededehoteisCollection().remove(rededehoteis);
                cidadeidCidadeOld = em.merge(cidadeidCidadeOld);
            }
            if (cidadeidCidadeNew != null && !cidadeidCidadeNew.equals(cidadeidCidadeOld)) {
                cidadeidCidadeNew.getRededehoteisCollection().add(rededehoteis);
                cidadeidCidadeNew = em.merge(cidadeidCidadeNew);
            }
            for (Telefone telefoneCollectionNewTelefone : telefoneCollectionNew) {
                if (!telefoneCollectionOld.contains(telefoneCollectionNewTelefone)) {
                    Rededehoteis oldRedeDeHoteisidRedeDeHoteisOfTelefoneCollectionNewTelefone = telefoneCollectionNewTelefone.getRedeDeHoteisidRedeDeHoteis();
                    telefoneCollectionNewTelefone.setRedeDeHoteisidRedeDeHoteis(rededehoteis);
                    telefoneCollectionNewTelefone = em.merge(telefoneCollectionNewTelefone);
                    if (oldRedeDeHoteisidRedeDeHoteisOfTelefoneCollectionNewTelefone != null && !oldRedeDeHoteisidRedeDeHoteisOfTelefoneCollectionNewTelefone.equals(rededehoteis)) {
                        oldRedeDeHoteisidRedeDeHoteisOfTelefoneCollectionNewTelefone.getTelefoneCollection().remove(telefoneCollectionNewTelefone);
                        oldRedeDeHoteisidRedeDeHoteisOfTelefoneCollectionNewTelefone = em.merge(oldRedeDeHoteisidRedeDeHoteisOfTelefoneCollectionNewTelefone);
                    }
                }
            }
            for (Quartos quartosCollectionNewQuartos : quartosCollectionNew) {
                if (!quartosCollectionOld.contains(quartosCollectionNewQuartos)) {
                    Rededehoteis oldRedeDeHoteisidRedeDeHoteisOfQuartosCollectionNewQuartos = quartosCollectionNewQuartos.getRedeDeHoteisidRedeDeHoteis();
                    quartosCollectionNewQuartos.setRedeDeHoteisidRedeDeHoteis(rededehoteis);
                    quartosCollectionNewQuartos = em.merge(quartosCollectionNewQuartos);
                    if (oldRedeDeHoteisidRedeDeHoteisOfQuartosCollectionNewQuartos != null && !oldRedeDeHoteisidRedeDeHoteisOfQuartosCollectionNewQuartos.equals(rededehoteis)) {
                        oldRedeDeHoteisidRedeDeHoteisOfQuartosCollectionNewQuartos.getQuartosCollection().remove(quartosCollectionNewQuartos);
                        oldRedeDeHoteisidRedeDeHoteisOfQuartosCollectionNewQuartos = em.merge(oldRedeDeHoteisidRedeDeHoteisOfQuartosCollectionNewQuartos);
                    }
                }
            }
            for (Funcionarios funcionariosCollectionNewFuncionarios : funcionariosCollectionNew) {
                if (!funcionariosCollectionOld.contains(funcionariosCollectionNewFuncionarios)) {
                    Rededehoteis oldRedeDeHoteisidRedeDeHoteisOfFuncionariosCollectionNewFuncionarios = funcionariosCollectionNewFuncionarios.getRedeDeHoteisidRedeDeHoteis();
                    funcionariosCollectionNewFuncionarios.setRedeDeHoteisidRedeDeHoteis(rededehoteis);
                    funcionariosCollectionNewFuncionarios = em.merge(funcionariosCollectionNewFuncionarios);
                    if (oldRedeDeHoteisidRedeDeHoteisOfFuncionariosCollectionNewFuncionarios != null && !oldRedeDeHoteisidRedeDeHoteisOfFuncionariosCollectionNewFuncionarios.equals(rededehoteis)) {
                        oldRedeDeHoteisidRedeDeHoteisOfFuncionariosCollectionNewFuncionarios.getFuncionariosCollection().remove(funcionariosCollectionNewFuncionarios);
                        oldRedeDeHoteisidRedeDeHoteisOfFuncionariosCollectionNewFuncionarios = em.merge(oldRedeDeHoteisidRedeDeHoteisOfFuncionariosCollectionNewFuncionarios);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = rededehoteis.getIdRedeDeHoteis();
                if (findRededehoteis(id) == null) {
                    throw new NonexistentEntityException("The rededehoteis with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Rededehoteis rededehoteis;
            try {
                rededehoteis = em.getReference(Rededehoteis.class, id);
                rededehoteis.getIdRedeDeHoteis();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The rededehoteis with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Telefone> telefoneCollectionOrphanCheck = rededehoteis.getTelefoneCollection();
            for (Telefone telefoneCollectionOrphanCheckTelefone : telefoneCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Rededehoteis (" + rededehoteis + ") cannot be destroyed since the Telefone " + telefoneCollectionOrphanCheckTelefone + " in its telefoneCollection field has a non-nullable redeDeHoteisidRedeDeHoteis field.");
            }
            Collection<Quartos> quartosCollectionOrphanCheck = rededehoteis.getQuartosCollection();
            for (Quartos quartosCollectionOrphanCheckQuartos : quartosCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Rededehoteis (" + rededehoteis + ") cannot be destroyed since the Quartos " + quartosCollectionOrphanCheckQuartos + " in its quartosCollection field has a non-nullable redeDeHoteisidRedeDeHoteis field.");
            }
            Collection<Funcionarios> funcionariosCollectionOrphanCheck = rededehoteis.getFuncionariosCollection();
            for (Funcionarios funcionariosCollectionOrphanCheckFuncionarios : funcionariosCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Rededehoteis (" + rededehoteis + ") cannot be destroyed since the Funcionarios " + funcionariosCollectionOrphanCheckFuncionarios + " in its funcionariosCollection field has a non-nullable redeDeHoteisidRedeDeHoteis field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Cidade cidadeidCidade = rededehoteis.getCidadeidCidade();
            if (cidadeidCidade != null) {
                cidadeidCidade.getRededehoteisCollection().remove(rededehoteis);
                cidadeidCidade = em.merge(cidadeidCidade);
            }
            em.remove(rededehoteis);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Rededehoteis> findRededehoteisEntities() {
        return findRededehoteisEntities(true, -1, -1);
    }

    public List<Rededehoteis> findRededehoteisEntities(int maxResults, int firstResult) {
        return findRededehoteisEntities(false, maxResults, firstResult);
    }

    private List<Rededehoteis> findRededehoteisEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Rededehoteis.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Rededehoteis findRededehoteis(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Rededehoteis.class, id);
        } finally {
            em.close();
        }
    }

    public int getRededehoteisCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Rededehoteis> rt = cq.from(Rededehoteis.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
