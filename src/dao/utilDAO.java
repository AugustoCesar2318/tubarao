/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Augusto
 */
public class utilDAO {

    public EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("ProvaTubaraoPU");
        return factory.createEntityManager();
    }
    
    public Date getDate(){
        EntityManager em = getEntityManager();
        
        Query q = em.createNativeQuery("Select CURRENT_DATE");
        
        return (Date) q.getSingleResult();
    }
}
