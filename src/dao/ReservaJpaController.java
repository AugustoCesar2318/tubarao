/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.IllegalOrphanException;
import dao.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import domain.Funcionarios;
import domain.Hospede;
import domain.Quartos;
import domain.Checkin;
import domain.Reserva;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Augusto
 */
public class ReservaJpaController implements Serializable {

    public EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("ProvaTubaraoPU");
        return factory.createEntityManager();
    }

    public void create(Reserva reserva) {
        if (reserva.getCheckinCollection() == null) {
            reserva.setCheckinCollection(new ArrayList<Checkin>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Funcionarios funcionariosidFuncionarios = reserva.getFuncionariosidFuncionarios();
            if (funcionariosidFuncionarios != null) {
                funcionariosidFuncionarios = em.getReference(funcionariosidFuncionarios.getClass(), funcionariosidFuncionarios.getIdFuncionarios());
                reserva.setFuncionariosidFuncionarios(funcionariosidFuncionarios);
            }
            Hospede hospedeidHospede = reserva.getHospedeidHospede();
            if (hospedeidHospede != null) {
                hospedeidHospede = em.getReference(hospedeidHospede.getClass(), hospedeidHospede.getIdHospede());
                reserva.setHospedeidHospede(hospedeidHospede);
            }
            Quartos quartosidQuartos = reserva.getQuartosidQuartos();
            if (quartosidQuartos != null) {
                quartosidQuartos = em.getReference(quartosidQuartos.getClass(), quartosidQuartos.getIdQuartos());
                reserva.setQuartosidQuartos(quartosidQuartos);
            }
            Collection<Checkin> attachedCheckinCollection = new ArrayList<Checkin>();
            for (Checkin checkinCollectionCheckinToAttach : reserva.getCheckinCollection()) {
                checkinCollectionCheckinToAttach = em.getReference(checkinCollectionCheckinToAttach.getClass(), checkinCollectionCheckinToAttach.getIdCheckIn());
                attachedCheckinCollection.add(checkinCollectionCheckinToAttach);
            }
            reserva.setCheckinCollection(attachedCheckinCollection);
            em.persist(reserva);
            if (funcionariosidFuncionarios != null) {
                funcionariosidFuncionarios.getReservaCollection().add(reserva);
                funcionariosidFuncionarios = em.merge(funcionariosidFuncionarios);
            }
            if (hospedeidHospede != null) {
                hospedeidHospede.getReservaCollection().add(reserva);
                hospedeidHospede = em.merge(hospedeidHospede);
            }
            if (quartosidQuartos != null) {
                quartosidQuartos.getReservaCollection().add(reserva);
                quartosidQuartos = em.merge(quartosidQuartos);
            }
            for (Checkin checkinCollectionCheckin : reserva.getCheckinCollection()) {
                Reserva oldReservaidResevaOfCheckinCollectionCheckin = checkinCollectionCheckin.getReservaidReseva();
                checkinCollectionCheckin.setReservaidReseva(reserva);
                checkinCollectionCheckin = em.merge(checkinCollectionCheckin);
                if (oldReservaidResevaOfCheckinCollectionCheckin != null) {
                    oldReservaidResevaOfCheckinCollectionCheckin.getCheckinCollection().remove(checkinCollectionCheckin);
                    oldReservaidResevaOfCheckinCollectionCheckin = em.merge(oldReservaidResevaOfCheckinCollectionCheckin);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Reserva reserva) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Reserva persistentReserva = em.find(Reserva.class, reserva.getIdReseva());
            Funcionarios funcionariosidFuncionariosOld = persistentReserva.getFuncionariosidFuncionarios();
            Funcionarios funcionariosidFuncionariosNew = reserva.getFuncionariosidFuncionarios();
            Hospede hospedeidHospedeOld = persistentReserva.getHospedeidHospede();
            Hospede hospedeidHospedeNew = reserva.getHospedeidHospede();
            Quartos quartosidQuartosOld = persistentReserva.getQuartosidQuartos();
            Quartos quartosidQuartosNew = reserva.getQuartosidQuartos();
            Collection<Checkin> checkinCollectionOld = persistentReserva.getCheckinCollection();
            Collection<Checkin> checkinCollectionNew = reserva.getCheckinCollection();
            List<String> illegalOrphanMessages = null;
            for (Checkin checkinCollectionOldCheckin : checkinCollectionOld) {
                if (!checkinCollectionNew.contains(checkinCollectionOldCheckin)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Checkin " + checkinCollectionOldCheckin + " since its reservaidReseva field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (funcionariosidFuncionariosNew != null) {
                funcionariosidFuncionariosNew = em.getReference(funcionariosidFuncionariosNew.getClass(), funcionariosidFuncionariosNew.getIdFuncionarios());
                reserva.setFuncionariosidFuncionarios(funcionariosidFuncionariosNew);
            }
            if (hospedeidHospedeNew != null) {
                hospedeidHospedeNew = em.getReference(hospedeidHospedeNew.getClass(), hospedeidHospedeNew.getIdHospede());
                reserva.setHospedeidHospede(hospedeidHospedeNew);
            }
            if (quartosidQuartosNew != null) {
                quartosidQuartosNew = em.getReference(quartosidQuartosNew.getClass(), quartosidQuartosNew.getIdQuartos());
                reserva.setQuartosidQuartos(quartosidQuartosNew);
            }
            Collection<Checkin> attachedCheckinCollectionNew = new ArrayList<Checkin>();
            for (Checkin checkinCollectionNewCheckinToAttach : checkinCollectionNew) {
                checkinCollectionNewCheckinToAttach = em.getReference(checkinCollectionNewCheckinToAttach.getClass(), checkinCollectionNewCheckinToAttach.getIdCheckIn());
                attachedCheckinCollectionNew.add(checkinCollectionNewCheckinToAttach);
            }
            checkinCollectionNew = attachedCheckinCollectionNew;
            reserva.setCheckinCollection(checkinCollectionNew);
            reserva = em.merge(reserva);
            if (funcionariosidFuncionariosOld != null && !funcionariosidFuncionariosOld.equals(funcionariosidFuncionariosNew)) {
                funcionariosidFuncionariosOld.getReservaCollection().remove(reserva);
                funcionariosidFuncionariosOld = em.merge(funcionariosidFuncionariosOld);
            }
            if (funcionariosidFuncionariosNew != null && !funcionariosidFuncionariosNew.equals(funcionariosidFuncionariosOld)) {
                funcionariosidFuncionariosNew.getReservaCollection().add(reserva);
                funcionariosidFuncionariosNew = em.merge(funcionariosidFuncionariosNew);
            }
            if (hospedeidHospedeOld != null && !hospedeidHospedeOld.equals(hospedeidHospedeNew)) {
                hospedeidHospedeOld.getReservaCollection().remove(reserva);
                hospedeidHospedeOld = em.merge(hospedeidHospedeOld);
            }
            if (hospedeidHospedeNew != null && !hospedeidHospedeNew.equals(hospedeidHospedeOld)) {
                hospedeidHospedeNew.getReservaCollection().add(reserva);
                hospedeidHospedeNew = em.merge(hospedeidHospedeNew);
            }
            if (quartosidQuartosOld != null && !quartosidQuartosOld.equals(quartosidQuartosNew)) {
                quartosidQuartosOld.getReservaCollection().remove(reserva);
                quartosidQuartosOld = em.merge(quartosidQuartosOld);
            }
            if (quartosidQuartosNew != null && !quartosidQuartosNew.equals(quartosidQuartosOld)) {
                quartosidQuartosNew.getReservaCollection().add(reserva);
                quartosidQuartosNew = em.merge(quartosidQuartosNew);
            }
            for (Checkin checkinCollectionNewCheckin : checkinCollectionNew) {
                if (!checkinCollectionOld.contains(checkinCollectionNewCheckin)) {
                    Reserva oldReservaidResevaOfCheckinCollectionNewCheckin = checkinCollectionNewCheckin.getReservaidReseva();
                    checkinCollectionNewCheckin.setReservaidReseva(reserva);
                    checkinCollectionNewCheckin = em.merge(checkinCollectionNewCheckin);
                    if (oldReservaidResevaOfCheckinCollectionNewCheckin != null && !oldReservaidResevaOfCheckinCollectionNewCheckin.equals(reserva)) {
                        oldReservaidResevaOfCheckinCollectionNewCheckin.getCheckinCollection().remove(checkinCollectionNewCheckin);
                        oldReservaidResevaOfCheckinCollectionNewCheckin = em.merge(oldReservaidResevaOfCheckinCollectionNewCheckin);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = reserva.getIdReseva();
                if (findReserva(id) == null) {
                    throw new NonexistentEntityException("The reserva with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Reserva reserva;
            try {
                reserva = em.getReference(Reserva.class, id);
                reserva.getIdReseva();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The reserva with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Checkin> checkinCollectionOrphanCheck = reserva.getCheckinCollection();
            for (Checkin checkinCollectionOrphanCheckCheckin : checkinCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Reserva (" + reserva + ") cannot be destroyed since the Checkin " + checkinCollectionOrphanCheckCheckin + " in its checkinCollection field has a non-nullable reservaidReseva field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Funcionarios funcionariosidFuncionarios = reserva.getFuncionariosidFuncionarios();
            if (funcionariosidFuncionarios != null) {
                funcionariosidFuncionarios.getReservaCollection().remove(reserva);
                funcionariosidFuncionarios = em.merge(funcionariosidFuncionarios);
            }
            Hospede hospedeidHospede = reserva.getHospedeidHospede();
            if (hospedeidHospede != null) {
                hospedeidHospede.getReservaCollection().remove(reserva);
                hospedeidHospede = em.merge(hospedeidHospede);
            }
            Quartos quartosidQuartos = reserva.getQuartosidQuartos();
            if (quartosidQuartos != null) {
                quartosidQuartos.getReservaCollection().remove(reserva);
                quartosidQuartos = em.merge(quartosidQuartos);
            }
            em.remove(reserva);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Reserva> findReservaEntities() {
        return findReservaEntities(true, -1, -1);
    }

    public List<Reserva> findReservaEntities(int maxResults, int firstResult) {
        return findReservaEntities(false, maxResults, firstResult);
    }

    private List<Reserva> findReservaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Reserva.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Reserva findReserva(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Reserva.class, id);
        } finally {
            em.close();
        }
    }

    public int getReservaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Reserva> rt = cq.from(Reserva.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
