/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import domain.Pessoa;
import domain.Rededehoteis;
import domain.Telefone;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Augusto
 */
public class TelefoneJpaController implements Serializable {

    public EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("ProvaTubaraoPU");
        return factory.createEntityManager();
    }

    public void create(Telefone telefone) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pessoa pessoaidPessoa = telefone.getPessoaidPessoa();
            if (pessoaidPessoa != null) {
                pessoaidPessoa = em.getReference(pessoaidPessoa.getClass(), pessoaidPessoa.getIdPessoa());
                telefone.setPessoaidPessoa(pessoaidPessoa);
            }
            Rededehoteis redeDeHoteisidRedeDeHoteis = telefone.getRedeDeHoteisidRedeDeHoteis();
            if (redeDeHoteisidRedeDeHoteis != null) {
                redeDeHoteisidRedeDeHoteis = em.getReference(redeDeHoteisidRedeDeHoteis.getClass(), redeDeHoteisidRedeDeHoteis.getIdRedeDeHoteis());
                telefone.setRedeDeHoteisidRedeDeHoteis(redeDeHoteisidRedeDeHoteis);
            }
            em.persist(telefone);
            if (pessoaidPessoa != null) {
                pessoaidPessoa.getTelefoneCollection().add(telefone);
                pessoaidPessoa = em.merge(pessoaidPessoa);
            }
            if (redeDeHoteisidRedeDeHoteis != null) {
                redeDeHoteisidRedeDeHoteis.getTelefoneCollection().add(telefone);
                redeDeHoteisidRedeDeHoteis = em.merge(redeDeHoteisidRedeDeHoteis);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Telefone telefone) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Telefone persistentTelefone = em.find(Telefone.class, telefone.getIdTelefone());
            Pessoa pessoaidPessoaOld = persistentTelefone.getPessoaidPessoa();
            Pessoa pessoaidPessoaNew = telefone.getPessoaidPessoa();
            Rededehoteis redeDeHoteisidRedeDeHoteisOld = persistentTelefone.getRedeDeHoteisidRedeDeHoteis();
            Rededehoteis redeDeHoteisidRedeDeHoteisNew = telefone.getRedeDeHoteisidRedeDeHoteis();
            if (pessoaidPessoaNew != null) {
                pessoaidPessoaNew = em.getReference(pessoaidPessoaNew.getClass(), pessoaidPessoaNew.getIdPessoa());
                telefone.setPessoaidPessoa(pessoaidPessoaNew);
            }
            if (redeDeHoteisidRedeDeHoteisNew != null) {
                redeDeHoteisidRedeDeHoteisNew = em.getReference(redeDeHoteisidRedeDeHoteisNew.getClass(), redeDeHoteisidRedeDeHoteisNew.getIdRedeDeHoteis());
                telefone.setRedeDeHoteisidRedeDeHoteis(redeDeHoteisidRedeDeHoteisNew);
            }
            telefone = em.merge(telefone);
            if (pessoaidPessoaOld != null && !pessoaidPessoaOld.equals(pessoaidPessoaNew)) {
                pessoaidPessoaOld.getTelefoneCollection().remove(telefone);
                pessoaidPessoaOld = em.merge(pessoaidPessoaOld);
            }
            if (pessoaidPessoaNew != null && !pessoaidPessoaNew.equals(pessoaidPessoaOld)) {
                pessoaidPessoaNew.getTelefoneCollection().add(telefone);
                pessoaidPessoaNew = em.merge(pessoaidPessoaNew);
            }
            if (redeDeHoteisidRedeDeHoteisOld != null && !redeDeHoteisidRedeDeHoteisOld.equals(redeDeHoteisidRedeDeHoteisNew)) {
                redeDeHoteisidRedeDeHoteisOld.getTelefoneCollection().remove(telefone);
                redeDeHoteisidRedeDeHoteisOld = em.merge(redeDeHoteisidRedeDeHoteisOld);
            }
            if (redeDeHoteisidRedeDeHoteisNew != null && !redeDeHoteisidRedeDeHoteisNew.equals(redeDeHoteisidRedeDeHoteisOld)) {
                redeDeHoteisidRedeDeHoteisNew.getTelefoneCollection().add(telefone);
                redeDeHoteisidRedeDeHoteisNew = em.merge(redeDeHoteisidRedeDeHoteisNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = telefone.getIdTelefone();
                if (findTelefone(id) == null) {
                    throw new NonexistentEntityException("The telefone with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Telefone telefone;
            try {
                telefone = em.getReference(Telefone.class, id);
                telefone.getIdTelefone();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The telefone with id " + id + " no longer exists.", enfe);
            }
            Pessoa pessoaidPessoa = telefone.getPessoaidPessoa();
            if (pessoaidPessoa != null) {
                pessoaidPessoa.getTelefoneCollection().remove(telefone);
                pessoaidPessoa = em.merge(pessoaidPessoa);
            }
            Rededehoteis redeDeHoteisidRedeDeHoteis = telefone.getRedeDeHoteisidRedeDeHoteis();
            if (redeDeHoteisidRedeDeHoteis != null) {
                redeDeHoteisidRedeDeHoteis.getTelefoneCollection().remove(telefone);
                redeDeHoteisidRedeDeHoteis = em.merge(redeDeHoteisidRedeDeHoteis);
            }
            em.remove(telefone);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Telefone> findTelefoneEntities() {
        return findTelefoneEntities(true, -1, -1);
    }

    public List<Telefone> findTelefoneEntities(int maxResults, int firstResult) {
        return findTelefoneEntities(false, maxResults, firstResult);
    }

    private List<Telefone> findTelefoneEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Telefone.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Telefone findTelefone(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Telefone.class, id);
        } finally {
            em.close();
        }
    }

    public int getTelefoneCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Telefone> rt = cq.from(Telefone.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List<Telefone> findTelByPessoa(Pessoa p) {
        EntityManager em = getEntityManager();

        Query q = em.createQuery("Select t from Telefone t WHERE t.pessoaidPessoa = :id");
        q.setParameter("id", p);

        return q.getResultList();
    }
    
}
