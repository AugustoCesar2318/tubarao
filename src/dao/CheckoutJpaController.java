/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import domain.Checkin;
import domain.Checkout;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Augusto
 */
public class CheckoutJpaController implements Serializable {

    public EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("ProvaTubaraoPU");
        return factory.createEntityManager();
    }

    public void create(Checkout checkout) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Checkin checkInidCheckIn = checkout.getCheckInidCheckIn();
            if (checkInidCheckIn != null) {
                checkInidCheckIn = em.getReference(checkInidCheckIn.getClass(), checkInidCheckIn.getIdCheckIn());
                checkout.setCheckInidCheckIn(checkInidCheckIn);
            }
            em.persist(checkout);
            if (checkInidCheckIn != null) {
                checkInidCheckIn.getCheckoutCollection().add(checkout);
                checkInidCheckIn = em.merge(checkInidCheckIn);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Checkout checkout) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Checkout persistentCheckout = em.find(Checkout.class, checkout.getIdCheckOut());
            Checkin checkInidCheckInOld = persistentCheckout.getCheckInidCheckIn();
            Checkin checkInidCheckInNew = checkout.getCheckInidCheckIn();
            if (checkInidCheckInNew != null) {
                checkInidCheckInNew = em.getReference(checkInidCheckInNew.getClass(), checkInidCheckInNew.getIdCheckIn());
                checkout.setCheckInidCheckIn(checkInidCheckInNew);
            }
            checkout = em.merge(checkout);
            if (checkInidCheckInOld != null && !checkInidCheckInOld.equals(checkInidCheckInNew)) {
                checkInidCheckInOld.getCheckoutCollection().remove(checkout);
                checkInidCheckInOld = em.merge(checkInidCheckInOld);
            }
            if (checkInidCheckInNew != null && !checkInidCheckInNew.equals(checkInidCheckInOld)) {
                checkInidCheckInNew.getCheckoutCollection().add(checkout);
                checkInidCheckInNew = em.merge(checkInidCheckInNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = checkout.getIdCheckOut();
                if (findCheckout(id) == null) {
                    throw new NonexistentEntityException("The checkout with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Checkout checkout;
            try {
                checkout = em.getReference(Checkout.class, id);
                checkout.getIdCheckOut();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The checkout with id " + id + " no longer exists.", enfe);
            }
            Checkin checkInidCheckIn = checkout.getCheckInidCheckIn();
            if (checkInidCheckIn != null) {
                checkInidCheckIn.getCheckoutCollection().remove(checkout);
                checkInidCheckIn = em.merge(checkInidCheckIn);
            }
            em.remove(checkout);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Checkout> findCheckoutEntities() {
        return findCheckoutEntities(true, -1, -1);
    }

    public List<Checkout> findCheckoutEntities(int maxResults, int firstResult) {
        return findCheckoutEntities(false, maxResults, firstResult);
    }

    private List<Checkout> findCheckoutEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Checkout.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Checkout findCheckout(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Checkout.class, id);
        } finally {
            em.close();
        }
    }

    public int getCheckoutCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Checkout> rt = cq.from(Checkout.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
