/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.IllegalOrphanException;
import dao.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import domain.Cidade;
import domain.Telefone;
import java.util.ArrayList;
import java.util.Collection;
import domain.Hospede;
import domain.Funcionarios;
import domain.Pessoa;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Augusto
 */
public class PessoaJpaController implements Serializable {

    public EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("ProvaTubaraoPU");
        return factory.createEntityManager();
    }

    public void create(Pessoa pessoa) {
        if (pessoa.getTelefoneCollection() == null) {
            pessoa.setTelefoneCollection(new ArrayList<Telefone>());
        }
        if (pessoa.getHospedeCollection() == null) {
            pessoa.setHospedeCollection(new ArrayList<Hospede>());
        }
        if (pessoa.getFuncionariosCollection() == null) {
            pessoa.setFuncionariosCollection(new ArrayList<Funcionarios>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cidade cidadeidCidade = pessoa.getCidadeidCidade();
            if (cidadeidCidade != null) {
                cidadeidCidade = em.getReference(cidadeidCidade.getClass(), cidadeidCidade.getIdCidade());
                pessoa.setCidadeidCidade(cidadeidCidade);
            }
            Collection<Telefone> attachedTelefoneCollection = new ArrayList<Telefone>();
            for (Telefone telefoneCollectionTelefoneToAttach : pessoa.getTelefoneCollection()) {
                telefoneCollectionTelefoneToAttach = em.getReference(telefoneCollectionTelefoneToAttach.getClass(), telefoneCollectionTelefoneToAttach.getIdTelefone());
                attachedTelefoneCollection.add(telefoneCollectionTelefoneToAttach);
            }
            pessoa.setTelefoneCollection(attachedTelefoneCollection);
            Collection<Hospede> attachedHospedeCollection = new ArrayList<Hospede>();
            for (Hospede hospedeCollectionHospedeToAttach : pessoa.getHospedeCollection()) {
                hospedeCollectionHospedeToAttach = em.getReference(hospedeCollectionHospedeToAttach.getClass(), hospedeCollectionHospedeToAttach.getIdHospede());
                attachedHospedeCollection.add(hospedeCollectionHospedeToAttach);
            }
            pessoa.setHospedeCollection(attachedHospedeCollection);
            Collection<Funcionarios> attachedFuncionariosCollection = new ArrayList<Funcionarios>();
            for (Funcionarios funcionariosCollectionFuncionariosToAttach : pessoa.getFuncionariosCollection()) {
                funcionariosCollectionFuncionariosToAttach = em.getReference(funcionariosCollectionFuncionariosToAttach.getClass(), funcionariosCollectionFuncionariosToAttach.getIdFuncionarios());
                attachedFuncionariosCollection.add(funcionariosCollectionFuncionariosToAttach);
            }
            pessoa.setFuncionariosCollection(attachedFuncionariosCollection);
            em.persist(pessoa);
            if (cidadeidCidade != null) {
                cidadeidCidade.getPessoaCollection().add(pessoa);
                cidadeidCidade = em.merge(cidadeidCidade);
            }
            for (Telefone telefoneCollectionTelefone : pessoa.getTelefoneCollection()) {
                Pessoa oldPessoaidPessoaOfTelefoneCollectionTelefone = telefoneCollectionTelefone.getPessoaidPessoa();
                telefoneCollectionTelefone.setPessoaidPessoa(pessoa);
                telefoneCollectionTelefone = em.merge(telefoneCollectionTelefone);
                if (oldPessoaidPessoaOfTelefoneCollectionTelefone != null) {
                    oldPessoaidPessoaOfTelefoneCollectionTelefone.getTelefoneCollection().remove(telefoneCollectionTelefone);
                    oldPessoaidPessoaOfTelefoneCollectionTelefone = em.merge(oldPessoaidPessoaOfTelefoneCollectionTelefone);
                }
            }
            for (Hospede hospedeCollectionHospede : pessoa.getHospedeCollection()) {
                Pessoa oldPessoaidPessoaOfHospedeCollectionHospede = hospedeCollectionHospede.getPessoaidPessoa();
                hospedeCollectionHospede.setPessoaidPessoa(pessoa);
                hospedeCollectionHospede = em.merge(hospedeCollectionHospede);
                if (oldPessoaidPessoaOfHospedeCollectionHospede != null) {
                    oldPessoaidPessoaOfHospedeCollectionHospede.getHospedeCollection().remove(hospedeCollectionHospede);
                    oldPessoaidPessoaOfHospedeCollectionHospede = em.merge(oldPessoaidPessoaOfHospedeCollectionHospede);
                }
            }
            for (Funcionarios funcionariosCollectionFuncionarios : pessoa.getFuncionariosCollection()) {
                Pessoa oldPessoaidPessoaOfFuncionariosCollectionFuncionarios = funcionariosCollectionFuncionarios.getPessoaidPessoa();
                funcionariosCollectionFuncionarios.setPessoaidPessoa(pessoa);
                funcionariosCollectionFuncionarios = em.merge(funcionariosCollectionFuncionarios);
                if (oldPessoaidPessoaOfFuncionariosCollectionFuncionarios != null) {
                    oldPessoaidPessoaOfFuncionariosCollectionFuncionarios.getFuncionariosCollection().remove(funcionariosCollectionFuncionarios);
                    oldPessoaidPessoaOfFuncionariosCollectionFuncionarios = em.merge(oldPessoaidPessoaOfFuncionariosCollectionFuncionarios);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Pessoa pessoa) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pessoa persistentPessoa = em.find(Pessoa.class, pessoa.getIdPessoa());
            Cidade cidadeidCidadeOld = persistentPessoa.getCidadeidCidade();
            Cidade cidadeidCidadeNew = pessoa.getCidadeidCidade();
            Collection<Telefone> telefoneCollectionOld = persistentPessoa.getTelefoneCollection();
            Collection<Telefone> telefoneCollectionNew = pessoa.getTelefoneCollection();
            Collection<Hospede> hospedeCollectionOld = persistentPessoa.getHospedeCollection();
            Collection<Hospede> hospedeCollectionNew = pessoa.getHospedeCollection();
            Collection<Funcionarios> funcionariosCollectionOld = persistentPessoa.getFuncionariosCollection();
            Collection<Funcionarios> funcionariosCollectionNew = pessoa.getFuncionariosCollection();
            List<String> illegalOrphanMessages = null;
            for (Telefone telefoneCollectionOldTelefone : telefoneCollectionOld) {
                if (!telefoneCollectionNew.contains(telefoneCollectionOldTelefone)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Telefone " + telefoneCollectionOldTelefone + " since its pessoaidPessoa field is not nullable.");
                }
            }
            for (Hospede hospedeCollectionOldHospede : hospedeCollectionOld) {
                if (!hospedeCollectionNew.contains(hospedeCollectionOldHospede)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Hospede " + hospedeCollectionOldHospede + " since its pessoaidPessoa field is not nullable.");
                }
            }
            for (Funcionarios funcionariosCollectionOldFuncionarios : funcionariosCollectionOld) {
                if (!funcionariosCollectionNew.contains(funcionariosCollectionOldFuncionarios)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Funcionarios " + funcionariosCollectionOldFuncionarios + " since its pessoaidPessoa field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (cidadeidCidadeNew != null) {
                cidadeidCidadeNew = em.getReference(cidadeidCidadeNew.getClass(), cidadeidCidadeNew.getIdCidade());
                pessoa.setCidadeidCidade(cidadeidCidadeNew);
            }
            Collection<Telefone> attachedTelefoneCollectionNew = new ArrayList<Telefone>();
            for (Telefone telefoneCollectionNewTelefoneToAttach : telefoneCollectionNew) {
                telefoneCollectionNewTelefoneToAttach = em.getReference(telefoneCollectionNewTelefoneToAttach.getClass(), telefoneCollectionNewTelefoneToAttach.getIdTelefone());
                attachedTelefoneCollectionNew.add(telefoneCollectionNewTelefoneToAttach);
            }
            telefoneCollectionNew = attachedTelefoneCollectionNew;
            pessoa.setTelefoneCollection(telefoneCollectionNew);
            Collection<Hospede> attachedHospedeCollectionNew = new ArrayList<Hospede>();
            for (Hospede hospedeCollectionNewHospedeToAttach : hospedeCollectionNew) {
                hospedeCollectionNewHospedeToAttach = em.getReference(hospedeCollectionNewHospedeToAttach.getClass(), hospedeCollectionNewHospedeToAttach.getIdHospede());
                attachedHospedeCollectionNew.add(hospedeCollectionNewHospedeToAttach);
            }
            hospedeCollectionNew = attachedHospedeCollectionNew;
            pessoa.setHospedeCollection(hospedeCollectionNew);
            Collection<Funcionarios> attachedFuncionariosCollectionNew = new ArrayList<Funcionarios>();
            for (Funcionarios funcionariosCollectionNewFuncionariosToAttach : funcionariosCollectionNew) {
                funcionariosCollectionNewFuncionariosToAttach = em.getReference(funcionariosCollectionNewFuncionariosToAttach.getClass(), funcionariosCollectionNewFuncionariosToAttach.getIdFuncionarios());
                attachedFuncionariosCollectionNew.add(funcionariosCollectionNewFuncionariosToAttach);
            }
            funcionariosCollectionNew = attachedFuncionariosCollectionNew;
            pessoa.setFuncionariosCollection(funcionariosCollectionNew);
            pessoa = em.merge(pessoa);
            if (cidadeidCidadeOld != null && !cidadeidCidadeOld.equals(cidadeidCidadeNew)) {
                cidadeidCidadeOld.getPessoaCollection().remove(pessoa);
                cidadeidCidadeOld = em.merge(cidadeidCidadeOld);
            }
            if (cidadeidCidadeNew != null && !cidadeidCidadeNew.equals(cidadeidCidadeOld)) {
                cidadeidCidadeNew.getPessoaCollection().add(pessoa);
                cidadeidCidadeNew = em.merge(cidadeidCidadeNew);
            }
            for (Telefone telefoneCollectionNewTelefone : telefoneCollectionNew) {
                if (!telefoneCollectionOld.contains(telefoneCollectionNewTelefone)) {
                    Pessoa oldPessoaidPessoaOfTelefoneCollectionNewTelefone = telefoneCollectionNewTelefone.getPessoaidPessoa();
                    telefoneCollectionNewTelefone.setPessoaidPessoa(pessoa);
                    telefoneCollectionNewTelefone = em.merge(telefoneCollectionNewTelefone);
                    if (oldPessoaidPessoaOfTelefoneCollectionNewTelefone != null && !oldPessoaidPessoaOfTelefoneCollectionNewTelefone.equals(pessoa)) {
                        oldPessoaidPessoaOfTelefoneCollectionNewTelefone.getTelefoneCollection().remove(telefoneCollectionNewTelefone);
                        oldPessoaidPessoaOfTelefoneCollectionNewTelefone = em.merge(oldPessoaidPessoaOfTelefoneCollectionNewTelefone);
                    }
                }
            }
            for (Hospede hospedeCollectionNewHospede : hospedeCollectionNew) {
                if (!hospedeCollectionOld.contains(hospedeCollectionNewHospede)) {
                    Pessoa oldPessoaidPessoaOfHospedeCollectionNewHospede = hospedeCollectionNewHospede.getPessoaidPessoa();
                    hospedeCollectionNewHospede.setPessoaidPessoa(pessoa);
                    hospedeCollectionNewHospede = em.merge(hospedeCollectionNewHospede);
                    if (oldPessoaidPessoaOfHospedeCollectionNewHospede != null && !oldPessoaidPessoaOfHospedeCollectionNewHospede.equals(pessoa)) {
                        oldPessoaidPessoaOfHospedeCollectionNewHospede.getHospedeCollection().remove(hospedeCollectionNewHospede);
                        oldPessoaidPessoaOfHospedeCollectionNewHospede = em.merge(oldPessoaidPessoaOfHospedeCollectionNewHospede);
                    }
                }
            }
            for (Funcionarios funcionariosCollectionNewFuncionarios : funcionariosCollectionNew) {
                if (!funcionariosCollectionOld.contains(funcionariosCollectionNewFuncionarios)) {
                    Pessoa oldPessoaidPessoaOfFuncionariosCollectionNewFuncionarios = funcionariosCollectionNewFuncionarios.getPessoaidPessoa();
                    funcionariosCollectionNewFuncionarios.setPessoaidPessoa(pessoa);
                    funcionariosCollectionNewFuncionarios = em.merge(funcionariosCollectionNewFuncionarios);
                    if (oldPessoaidPessoaOfFuncionariosCollectionNewFuncionarios != null && !oldPessoaidPessoaOfFuncionariosCollectionNewFuncionarios.equals(pessoa)) {
                        oldPessoaidPessoaOfFuncionariosCollectionNewFuncionarios.getFuncionariosCollection().remove(funcionariosCollectionNewFuncionarios);
                        oldPessoaidPessoaOfFuncionariosCollectionNewFuncionarios = em.merge(oldPessoaidPessoaOfFuncionariosCollectionNewFuncionarios);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = pessoa.getIdPessoa();
                if (findPessoa(id) == null) {
                    throw new NonexistentEntityException("The pessoa with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pessoa pessoa;
            try {
                pessoa = em.getReference(Pessoa.class, id);
                pessoa.getIdPessoa();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The pessoa with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Telefone> telefoneCollectionOrphanCheck = pessoa.getTelefoneCollection();
            for (Telefone telefoneCollectionOrphanCheckTelefone : telefoneCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Pessoa (" + pessoa + ") cannot be destroyed since the Telefone " + telefoneCollectionOrphanCheckTelefone + " in its telefoneCollection field has a non-nullable pessoaidPessoa field.");
            }
            Collection<Hospede> hospedeCollectionOrphanCheck = pessoa.getHospedeCollection();
            for (Hospede hospedeCollectionOrphanCheckHospede : hospedeCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Pessoa (" + pessoa + ") cannot be destroyed since the Hospede " + hospedeCollectionOrphanCheckHospede + " in its hospedeCollection field has a non-nullable pessoaidPessoa field.");
            }
            Collection<Funcionarios> funcionariosCollectionOrphanCheck = pessoa.getFuncionariosCollection();
            for (Funcionarios funcionariosCollectionOrphanCheckFuncionarios : funcionariosCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Pessoa (" + pessoa + ") cannot be destroyed since the Funcionarios " + funcionariosCollectionOrphanCheckFuncionarios + " in its funcionariosCollection field has a non-nullable pessoaidPessoa field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Cidade cidadeidCidade = pessoa.getCidadeidCidade();
            if (cidadeidCidade != null) {
                cidadeidCidade.getPessoaCollection().remove(pessoa);
                cidadeidCidade = em.merge(cidadeidCidade);
            }
            em.remove(pessoa);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Pessoa> findPessoaEntities() {
        return findPessoaEntities(true, -1, -1);
    }

    public List<Pessoa> findPessoaEntities(int maxResults, int firstResult) {
        return findPessoaEntities(false, maxResults, firstResult);
    }

    private List<Pessoa> findPessoaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Pessoa.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Pessoa findPessoa(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Pessoa.class, id);
        } finally {
            em.close();
        }
    }

    public int getPessoaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Pessoa> rt = cq.from(Pessoa.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List<Pessoa> findByEmail(String email) {
        EntityManager em = getEntityManager();

        Query query = em.createQuery("Select p from Pessoa p WHERE p.email = :email");
        query.setParameter("email", email);

        return query.getResultList();
    }
    
}
