/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.IllegalOrphanException;
import dao.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import domain.Pessoa;
import domain.Rededehoteis;
import domain.TipoFuncionario;
import domain.Depesas;
import domain.Funcionarios;
import java.util.ArrayList;
import java.util.Collection;
import domain.Reserva;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Augusto
 */
public class FuncionariosJpaController implements Serializable {

    public EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("ProvaTubaraoPU");
        return factory.createEntityManager();
    }

    public void create(Funcionarios funcionarios) {
        if (funcionarios.getDepesasCollection() == null) {
            funcionarios.setDepesasCollection(new ArrayList<Depesas>());
        }
        if (funcionarios.getReservaCollection() == null) {
            funcionarios.setReservaCollection(new ArrayList<Reserva>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pessoa pessoaidPessoa = funcionarios.getPessoaidPessoa();
            if (pessoaidPessoa != null) {
                pessoaidPessoa = em.getReference(pessoaidPessoa.getClass(), pessoaidPessoa.getIdPessoa());
                funcionarios.setPessoaidPessoa(pessoaidPessoa);
            }
            Rededehoteis redeDeHoteisidRedeDeHoteis = funcionarios.getRedeDeHoteisidRedeDeHoteis();
            if (redeDeHoteisidRedeDeHoteis != null) {
                redeDeHoteisidRedeDeHoteis = em.getReference(redeDeHoteisidRedeDeHoteis.getClass(), redeDeHoteisidRedeDeHoteis.getIdRedeDeHoteis());
                funcionarios.setRedeDeHoteisidRedeDeHoteis(redeDeHoteisidRedeDeHoteis);
            }
            TipoFuncionario tipoFuncionarioidTipoFuncionario = funcionarios.getTipoFuncionarioidTipoFuncionario();
            if (tipoFuncionarioidTipoFuncionario != null) {
                tipoFuncionarioidTipoFuncionario = em.getReference(tipoFuncionarioidTipoFuncionario.getClass(), tipoFuncionarioidTipoFuncionario.getIdTipoFuncionario());
                funcionarios.setTipoFuncionarioidTipoFuncionario(tipoFuncionarioidTipoFuncionario);
            }
            Collection<Depesas> attachedDepesasCollection = new ArrayList<Depesas>();
            for (Depesas depesasCollectionDepesasToAttach : funcionarios.getDepesasCollection()) {
                depesasCollectionDepesasToAttach = em.getReference(depesasCollectionDepesasToAttach.getClass(), depesasCollectionDepesasToAttach.getIdDepesas());
                attachedDepesasCollection.add(depesasCollectionDepesasToAttach);
            }
            funcionarios.setDepesasCollection(attachedDepesasCollection);
            Collection<Reserva> attachedReservaCollection = new ArrayList<Reserva>();
            for (Reserva reservaCollectionReservaToAttach : funcionarios.getReservaCollection()) {
                reservaCollectionReservaToAttach = em.getReference(reservaCollectionReservaToAttach.getClass(), reservaCollectionReservaToAttach.getIdReseva());
                attachedReservaCollection.add(reservaCollectionReservaToAttach);
            }
            funcionarios.setReservaCollection(attachedReservaCollection);
            em.persist(funcionarios);
            if (pessoaidPessoa != null) {
                pessoaidPessoa.getFuncionariosCollection().add(funcionarios);
                pessoaidPessoa = em.merge(pessoaidPessoa);
            }
            if (redeDeHoteisidRedeDeHoteis != null) {
                redeDeHoteisidRedeDeHoteis.getFuncionariosCollection().add(funcionarios);
                redeDeHoteisidRedeDeHoteis = em.merge(redeDeHoteisidRedeDeHoteis);
            }
            if (tipoFuncionarioidTipoFuncionario != null) {
                tipoFuncionarioidTipoFuncionario.getFuncionariosCollection().add(funcionarios);
                tipoFuncionarioidTipoFuncionario = em.merge(tipoFuncionarioidTipoFuncionario);
            }
            for (Depesas depesasCollectionDepesas : funcionarios.getDepesasCollection()) {
                Funcionarios oldFuncionariosidFuncionariosOfDepesasCollectionDepesas = depesasCollectionDepesas.getFuncionariosidFuncionarios();
                depesasCollectionDepesas.setFuncionariosidFuncionarios(funcionarios);
                depesasCollectionDepesas = em.merge(depesasCollectionDepesas);
                if (oldFuncionariosidFuncionariosOfDepesasCollectionDepesas != null) {
                    oldFuncionariosidFuncionariosOfDepesasCollectionDepesas.getDepesasCollection().remove(depesasCollectionDepesas);
                    oldFuncionariosidFuncionariosOfDepesasCollectionDepesas = em.merge(oldFuncionariosidFuncionariosOfDepesasCollectionDepesas);
                }
            }
            for (Reserva reservaCollectionReserva : funcionarios.getReservaCollection()) {
                Funcionarios oldFuncionariosidFuncionariosOfReservaCollectionReserva = reservaCollectionReserva.getFuncionariosidFuncionarios();
                reservaCollectionReserva.setFuncionariosidFuncionarios(funcionarios);
                reservaCollectionReserva = em.merge(reservaCollectionReserva);
                if (oldFuncionariosidFuncionariosOfReservaCollectionReserva != null) {
                    oldFuncionariosidFuncionariosOfReservaCollectionReserva.getReservaCollection().remove(reservaCollectionReserva);
                    oldFuncionariosidFuncionariosOfReservaCollectionReserva = em.merge(oldFuncionariosidFuncionariosOfReservaCollectionReserva);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Funcionarios funcionarios) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Funcionarios persistentFuncionarios = em.find(Funcionarios.class, funcionarios.getIdFuncionarios());
            Pessoa pessoaidPessoaOld = persistentFuncionarios.getPessoaidPessoa();
            Pessoa pessoaidPessoaNew = funcionarios.getPessoaidPessoa();
            Rededehoteis redeDeHoteisidRedeDeHoteisOld = persistentFuncionarios.getRedeDeHoteisidRedeDeHoteis();
            Rededehoteis redeDeHoteisidRedeDeHoteisNew = funcionarios.getRedeDeHoteisidRedeDeHoteis();
            TipoFuncionario tipoFuncionarioidTipoFuncionarioOld = persistentFuncionarios.getTipoFuncionarioidTipoFuncionario();
            TipoFuncionario tipoFuncionarioidTipoFuncionarioNew = funcionarios.getTipoFuncionarioidTipoFuncionario();
            Collection<Depesas> depesasCollectionOld = persistentFuncionarios.getDepesasCollection();
            Collection<Depesas> depesasCollectionNew = funcionarios.getDepesasCollection();
            Collection<Reserva> reservaCollectionOld = persistentFuncionarios.getReservaCollection();
            Collection<Reserva> reservaCollectionNew = funcionarios.getReservaCollection();
            List<String> illegalOrphanMessages = null;
            for (Depesas depesasCollectionOldDepesas : depesasCollectionOld) {
                if (!depesasCollectionNew.contains(depesasCollectionOldDepesas)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Depesas " + depesasCollectionOldDepesas + " since its funcionariosidFuncionarios field is not nullable.");
                }
            }
            for (Reserva reservaCollectionOldReserva : reservaCollectionOld) {
                if (!reservaCollectionNew.contains(reservaCollectionOldReserva)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Reserva " + reservaCollectionOldReserva + " since its funcionariosidFuncionarios field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (pessoaidPessoaNew != null) {
                pessoaidPessoaNew = em.getReference(pessoaidPessoaNew.getClass(), pessoaidPessoaNew.getIdPessoa());
                funcionarios.setPessoaidPessoa(pessoaidPessoaNew);
            }
            if (redeDeHoteisidRedeDeHoteisNew != null) {
                redeDeHoteisidRedeDeHoteisNew = em.getReference(redeDeHoteisidRedeDeHoteisNew.getClass(), redeDeHoteisidRedeDeHoteisNew.getIdRedeDeHoteis());
                funcionarios.setRedeDeHoteisidRedeDeHoteis(redeDeHoteisidRedeDeHoteisNew);
            }
            if (tipoFuncionarioidTipoFuncionarioNew != null) {
                tipoFuncionarioidTipoFuncionarioNew = em.getReference(tipoFuncionarioidTipoFuncionarioNew.getClass(), tipoFuncionarioidTipoFuncionarioNew.getIdTipoFuncionario());
                funcionarios.setTipoFuncionarioidTipoFuncionario(tipoFuncionarioidTipoFuncionarioNew);
            }
            Collection<Depesas> attachedDepesasCollectionNew = new ArrayList<Depesas>();
            for (Depesas depesasCollectionNewDepesasToAttach : depesasCollectionNew) {
                depesasCollectionNewDepesasToAttach = em.getReference(depesasCollectionNewDepesasToAttach.getClass(), depesasCollectionNewDepesasToAttach.getIdDepesas());
                attachedDepesasCollectionNew.add(depesasCollectionNewDepesasToAttach);
            }
            depesasCollectionNew = attachedDepesasCollectionNew;
            funcionarios.setDepesasCollection(depesasCollectionNew);
            Collection<Reserva> attachedReservaCollectionNew = new ArrayList<Reserva>();
            for (Reserva reservaCollectionNewReservaToAttach : reservaCollectionNew) {
                reservaCollectionNewReservaToAttach = em.getReference(reservaCollectionNewReservaToAttach.getClass(), reservaCollectionNewReservaToAttach.getIdReseva());
                attachedReservaCollectionNew.add(reservaCollectionNewReservaToAttach);
            }
            reservaCollectionNew = attachedReservaCollectionNew;
            funcionarios.setReservaCollection(reservaCollectionNew);
            funcionarios = em.merge(funcionarios);
            if (pessoaidPessoaOld != null && !pessoaidPessoaOld.equals(pessoaidPessoaNew)) {
                pessoaidPessoaOld.getFuncionariosCollection().remove(funcionarios);
                pessoaidPessoaOld = em.merge(pessoaidPessoaOld);
            }
            if (pessoaidPessoaNew != null && !pessoaidPessoaNew.equals(pessoaidPessoaOld)) {
                pessoaidPessoaNew.getFuncionariosCollection().add(funcionarios);
                pessoaidPessoaNew = em.merge(pessoaidPessoaNew);
            }
            if (redeDeHoteisidRedeDeHoteisOld != null && !redeDeHoteisidRedeDeHoteisOld.equals(redeDeHoteisidRedeDeHoteisNew)) {
                redeDeHoteisidRedeDeHoteisOld.getFuncionariosCollection().remove(funcionarios);
                redeDeHoteisidRedeDeHoteisOld = em.merge(redeDeHoteisidRedeDeHoteisOld);
            }
            if (redeDeHoteisidRedeDeHoteisNew != null && !redeDeHoteisidRedeDeHoteisNew.equals(redeDeHoteisidRedeDeHoteisOld)) {
                redeDeHoteisidRedeDeHoteisNew.getFuncionariosCollection().add(funcionarios);
                redeDeHoteisidRedeDeHoteisNew = em.merge(redeDeHoteisidRedeDeHoteisNew);
            }
            if (tipoFuncionarioidTipoFuncionarioOld != null && !tipoFuncionarioidTipoFuncionarioOld.equals(tipoFuncionarioidTipoFuncionarioNew)) {
                tipoFuncionarioidTipoFuncionarioOld.getFuncionariosCollection().remove(funcionarios);
                tipoFuncionarioidTipoFuncionarioOld = em.merge(tipoFuncionarioidTipoFuncionarioOld);
            }
            if (tipoFuncionarioidTipoFuncionarioNew != null && !tipoFuncionarioidTipoFuncionarioNew.equals(tipoFuncionarioidTipoFuncionarioOld)) {
                tipoFuncionarioidTipoFuncionarioNew.getFuncionariosCollection().add(funcionarios);
                tipoFuncionarioidTipoFuncionarioNew = em.merge(tipoFuncionarioidTipoFuncionarioNew);
            }
            for (Depesas depesasCollectionNewDepesas : depesasCollectionNew) {
                if (!depesasCollectionOld.contains(depesasCollectionNewDepesas)) {
                    Funcionarios oldFuncionariosidFuncionariosOfDepesasCollectionNewDepesas = depesasCollectionNewDepesas.getFuncionariosidFuncionarios();
                    depesasCollectionNewDepesas.setFuncionariosidFuncionarios(funcionarios);
                    depesasCollectionNewDepesas = em.merge(depesasCollectionNewDepesas);
                    if (oldFuncionariosidFuncionariosOfDepesasCollectionNewDepesas != null && !oldFuncionariosidFuncionariosOfDepesasCollectionNewDepesas.equals(funcionarios)) {
                        oldFuncionariosidFuncionariosOfDepesasCollectionNewDepesas.getDepesasCollection().remove(depesasCollectionNewDepesas);
                        oldFuncionariosidFuncionariosOfDepesasCollectionNewDepesas = em.merge(oldFuncionariosidFuncionariosOfDepesasCollectionNewDepesas);
                    }
                }
            }
            for (Reserva reservaCollectionNewReserva : reservaCollectionNew) {
                if (!reservaCollectionOld.contains(reservaCollectionNewReserva)) {
                    Funcionarios oldFuncionariosidFuncionariosOfReservaCollectionNewReserva = reservaCollectionNewReserva.getFuncionariosidFuncionarios();
                    reservaCollectionNewReserva.setFuncionariosidFuncionarios(funcionarios);
                    reservaCollectionNewReserva = em.merge(reservaCollectionNewReserva);
                    if (oldFuncionariosidFuncionariosOfReservaCollectionNewReserva != null && !oldFuncionariosidFuncionariosOfReservaCollectionNewReserva.equals(funcionarios)) {
                        oldFuncionariosidFuncionariosOfReservaCollectionNewReserva.getReservaCollection().remove(reservaCollectionNewReserva);
                        oldFuncionariosidFuncionariosOfReservaCollectionNewReserva = em.merge(oldFuncionariosidFuncionariosOfReservaCollectionNewReserva);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = funcionarios.getIdFuncionarios();
                if (findFuncionarios(id) == null) {
                    throw new NonexistentEntityException("The funcionarios with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Funcionarios funcionarios;
            try {
                funcionarios = em.getReference(Funcionarios.class, id);
                funcionarios.getIdFuncionarios();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The funcionarios with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Depesas> depesasCollectionOrphanCheck = funcionarios.getDepesasCollection();
            for (Depesas depesasCollectionOrphanCheckDepesas : depesasCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Funcionarios (" + funcionarios + ") cannot be destroyed since the Depesas " + depesasCollectionOrphanCheckDepesas + " in its depesasCollection field has a non-nullable funcionariosidFuncionarios field.");
            }
            Collection<Reserva> reservaCollectionOrphanCheck = funcionarios.getReservaCollection();
            for (Reserva reservaCollectionOrphanCheckReserva : reservaCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Funcionarios (" + funcionarios + ") cannot be destroyed since the Reserva " + reservaCollectionOrphanCheckReserva + " in its reservaCollection field has a non-nullable funcionariosidFuncionarios field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Pessoa pessoaidPessoa = funcionarios.getPessoaidPessoa();
            if (pessoaidPessoa != null) {
                pessoaidPessoa.getFuncionariosCollection().remove(funcionarios);
                pessoaidPessoa = em.merge(pessoaidPessoa);
            }
            Rededehoteis redeDeHoteisidRedeDeHoteis = funcionarios.getRedeDeHoteisidRedeDeHoteis();
            if (redeDeHoteisidRedeDeHoteis != null) {
                redeDeHoteisidRedeDeHoteis.getFuncionariosCollection().remove(funcionarios);
                redeDeHoteisidRedeDeHoteis = em.merge(redeDeHoteisidRedeDeHoteis);
            }
            TipoFuncionario tipoFuncionarioidTipoFuncionario = funcionarios.getTipoFuncionarioidTipoFuncionario();
            if (tipoFuncionarioidTipoFuncionario != null) {
                tipoFuncionarioidTipoFuncionario.getFuncionariosCollection().remove(funcionarios);
                tipoFuncionarioidTipoFuncionario = em.merge(tipoFuncionarioidTipoFuncionario);
            }
            em.remove(funcionarios);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Funcionarios> findFuncionariosEntities() {
        return findFuncionariosEntities(true, -1, -1);
    }

    public List<Funcionarios> findFuncionariosEntities(int maxResults, int firstResult) {
        return findFuncionariosEntities(false, maxResults, firstResult);
    }

    private List<Funcionarios> findFuncionariosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Funcionarios.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Funcionarios findFuncionarios(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Funcionarios.class, id);
        } finally {
            em.close();
        }
    }

    public int getFuncionariosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Funcionarios> rt = cq.from(Funcionarios.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List<Funcionarios> findByPessoa(Pessoa p) {
        EntityManager em = getEntityManager();

        Query query = em.createQuery("Select f from Funcionarios f WHERE f.pessoaidPessoa = :id");
        query.setParameter("id", p);

        return query.getResultList();
    }

    public List<Funcionarios> findByEmail(String email) {
        EntityManager em = getEntityManager();

        Query query = em.createQuery("Select p from Pessoa p where p.email = :email");
        query.setParameter("email", email);

        Pessoa p = (Pessoa) query.getSingleResult();

        Query q = em.createQuery("Select f from Funcionarios f WHERE f.pessoaidPessoa = :id");
        q.setParameter("id", p);

        return q.getResultList();
    }

    public void update(Funcionarios f) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        Query q = em.createQuery("UPDATE Funcionarios f SET f.ativo = 1 WHERE f.idFuncionarios = :id");
        q.setParameter("id", f.getIdFuncionarios());
        q.executeUpdate();
        em.getTransaction().commit();
    }

    public void update2(Funcionarios f) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        Query q = em.createQuery("UPDATE Funcionarios f SET f.ativo = 0 WHERE f.idFuncionarios = :id");
        q.setParameter("id", f.getIdFuncionarios());
        q.executeUpdate();
        em.getTransaction().commit();
    }

    public List<Funcionarios> findByNome(String nome) {
        EntityManager em = getEntityManager();

        Query q = em.createQuery("Select p from Pessoa p WHERE p.nomePessoa LIKE :nome");
        q.setParameter("nome", '%'+nome+'%');

        List<Pessoa> listap = q.getResultList();
        
        Query query = null;
        
        for(Pessoa p : listap){

            query = em.createQuery("Select f from Funcionarios f WHERE f.pessoaidPessoa = :id");
            query.setParameter("id", p);
        }
        
        return query.getResultList();
    }
}
