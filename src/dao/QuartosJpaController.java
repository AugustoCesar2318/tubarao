/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.IllegalOrphanException;
import dao.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import domain.Rededehoteis;
import domain.TiposDeQuartos;
import domain.Fotosquartos;
import domain.Quartos;
import java.util.ArrayList;
import java.util.Collection;
import domain.Reserva;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Augusto
 */
public class QuartosJpaController implements Serializable {

    public EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("ProvaTubaraoPU");
        return factory.createEntityManager();
    }

    public void create(Quartos quartos) {
        if (quartos.getFotosquartosCollection() == null) {
            quartos.setFotosquartosCollection(new ArrayList<Fotosquartos>());
        }
        if (quartos.getReservaCollection() == null) {
            quartos.setReservaCollection(new ArrayList<Reserva>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Rededehoteis redeDeHoteisidRedeDeHoteis = quartos.getRedeDeHoteisidRedeDeHoteis();
            if (redeDeHoteisidRedeDeHoteis != null) {
                redeDeHoteisidRedeDeHoteis = em.getReference(redeDeHoteisidRedeDeHoteis.getClass(), redeDeHoteisidRedeDeHoteis.getIdRedeDeHoteis());
                quartos.setRedeDeHoteisidRedeDeHoteis(redeDeHoteisidRedeDeHoteis);
            }
            TiposDeQuartos tiposdequartosidTiposdequartos = quartos.getTiposdequartosidTiposdequartos();
            if (tiposdequartosidTiposdequartos != null) {
                tiposdequartosidTiposdequartos = em.getReference(tiposdequartosidTiposdequartos.getClass(), tiposdequartosidTiposdequartos.getIdTiposdequartos());
                quartos.setTiposdequartosidTiposdequartos(tiposdequartosidTiposdequartos);
            }
            Collection<Fotosquartos> attachedFotosquartosCollection = new ArrayList<Fotosquartos>();
            for (Fotosquartos fotosquartosCollectionFotosquartosToAttach : quartos.getFotosquartosCollection()) {
                fotosquartosCollectionFotosquartosToAttach = em.getReference(fotosquartosCollectionFotosquartosToAttach.getClass(), fotosquartosCollectionFotosquartosToAttach.getIdFotosQuartos());
                attachedFotosquartosCollection.add(fotosquartosCollectionFotosquartosToAttach);
            }
            quartos.setFotosquartosCollection(attachedFotosquartosCollection);
            Collection<Reserva> attachedReservaCollection = new ArrayList<Reserva>();
            for (Reserva reservaCollectionReservaToAttach : quartos.getReservaCollection()) {
                reservaCollectionReservaToAttach = em.getReference(reservaCollectionReservaToAttach.getClass(), reservaCollectionReservaToAttach.getIdReseva());
                attachedReservaCollection.add(reservaCollectionReservaToAttach);
            }
            quartos.setReservaCollection(attachedReservaCollection);
            em.persist(quartos);
            if (redeDeHoteisidRedeDeHoteis != null) {
                redeDeHoteisidRedeDeHoteis.getQuartosCollection().add(quartos);
                redeDeHoteisidRedeDeHoteis = em.merge(redeDeHoteisidRedeDeHoteis);
            }
            if (tiposdequartosidTiposdequartos != null) {
                tiposdequartosidTiposdequartos.getQuartosCollection().add(quartos);
                tiposdequartosidTiposdequartos = em.merge(tiposdequartosidTiposdequartos);
            }
            for (Fotosquartos fotosquartosCollectionFotosquartos : quartos.getFotosquartosCollection()) {
                Quartos oldQuartosidQuartosOfFotosquartosCollectionFotosquartos = fotosquartosCollectionFotosquartos.getQuartosidQuartos();
                fotosquartosCollectionFotosquartos.setQuartosidQuartos(quartos);
                fotosquartosCollectionFotosquartos = em.merge(fotosquartosCollectionFotosquartos);
                if (oldQuartosidQuartosOfFotosquartosCollectionFotosquartos != null) {
                    oldQuartosidQuartosOfFotosquartosCollectionFotosquartos.getFotosquartosCollection().remove(fotosquartosCollectionFotosquartos);
                    oldQuartosidQuartosOfFotosquartosCollectionFotosquartos = em.merge(oldQuartosidQuartosOfFotosquartosCollectionFotosquartos);
                }
            }
            for (Reserva reservaCollectionReserva : quartos.getReservaCollection()) {
                Quartos oldQuartosidQuartosOfReservaCollectionReserva = reservaCollectionReserva.getQuartosidQuartos();
                reservaCollectionReserva.setQuartosidQuartos(quartos);
                reservaCollectionReserva = em.merge(reservaCollectionReserva);
                if (oldQuartosidQuartosOfReservaCollectionReserva != null) {
                    oldQuartosidQuartosOfReservaCollectionReserva.getReservaCollection().remove(reservaCollectionReserva);
                    oldQuartosidQuartosOfReservaCollectionReserva = em.merge(oldQuartosidQuartosOfReservaCollectionReserva);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Quartos quartos) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Quartos persistentQuartos = em.find(Quartos.class, quartos.getIdQuartos());
            Rededehoteis redeDeHoteisidRedeDeHoteisOld = persistentQuartos.getRedeDeHoteisidRedeDeHoteis();
            Rededehoteis redeDeHoteisidRedeDeHoteisNew = quartos.getRedeDeHoteisidRedeDeHoteis();
            TiposDeQuartos tiposdequartosidTiposdequartosOld = persistentQuartos.getTiposdequartosidTiposdequartos();
            TiposDeQuartos tiposdequartosidTiposdequartosNew = quartos.getTiposdequartosidTiposdequartos();
            Collection<Fotosquartos> fotosquartosCollectionOld = persistentQuartos.getFotosquartosCollection();
            Collection<Fotosquartos> fotosquartosCollectionNew = quartos.getFotosquartosCollection();
            Collection<Reserva> reservaCollectionOld = persistentQuartos.getReservaCollection();
            Collection<Reserva> reservaCollectionNew = quartos.getReservaCollection();
            List<String> illegalOrphanMessages = null;
            for (Fotosquartos fotosquartosCollectionOldFotosquartos : fotosquartosCollectionOld) {
                if (!fotosquartosCollectionNew.contains(fotosquartosCollectionOldFotosquartos)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Fotosquartos " + fotosquartosCollectionOldFotosquartos + " since its quartosidQuartos field is not nullable.");
                }
            }
            for (Reserva reservaCollectionOldReserva : reservaCollectionOld) {
                if (!reservaCollectionNew.contains(reservaCollectionOldReserva)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Reserva " + reservaCollectionOldReserva + " since its quartosidQuartos field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (redeDeHoteisidRedeDeHoteisNew != null) {
                redeDeHoteisidRedeDeHoteisNew = em.getReference(redeDeHoteisidRedeDeHoteisNew.getClass(), redeDeHoteisidRedeDeHoteisNew.getIdRedeDeHoteis());
                quartos.setRedeDeHoteisidRedeDeHoteis(redeDeHoteisidRedeDeHoteisNew);
            }
            if (tiposdequartosidTiposdequartosNew != null) {
                tiposdequartosidTiposdequartosNew = em.getReference(tiposdequartosidTiposdequartosNew.getClass(), tiposdequartosidTiposdequartosNew.getIdTiposdequartos());
                quartos.setTiposdequartosidTiposdequartos(tiposdequartosidTiposdequartosNew);
            }
            Collection<Fotosquartos> attachedFotosquartosCollectionNew = new ArrayList<Fotosquartos>();
            for (Fotosquartos fotosquartosCollectionNewFotosquartosToAttach : fotosquartosCollectionNew) {
                fotosquartosCollectionNewFotosquartosToAttach = em.getReference(fotosquartosCollectionNewFotosquartosToAttach.getClass(), fotosquartosCollectionNewFotosquartosToAttach.getIdFotosQuartos());
                attachedFotosquartosCollectionNew.add(fotosquartosCollectionNewFotosquartosToAttach);
            }
            fotosquartosCollectionNew = attachedFotosquartosCollectionNew;
            quartos.setFotosquartosCollection(fotosquartosCollectionNew);
            Collection<Reserva> attachedReservaCollectionNew = new ArrayList<Reserva>();
            for (Reserva reservaCollectionNewReservaToAttach : reservaCollectionNew) {
                reservaCollectionNewReservaToAttach = em.getReference(reservaCollectionNewReservaToAttach.getClass(), reservaCollectionNewReservaToAttach.getIdReseva());
                attachedReservaCollectionNew.add(reservaCollectionNewReservaToAttach);
            }
            reservaCollectionNew = attachedReservaCollectionNew;
            quartos.setReservaCollection(reservaCollectionNew);
            quartos = em.merge(quartos);
            if (redeDeHoteisidRedeDeHoteisOld != null && !redeDeHoteisidRedeDeHoteisOld.equals(redeDeHoteisidRedeDeHoteisNew)) {
                redeDeHoteisidRedeDeHoteisOld.getQuartosCollection().remove(quartos);
                redeDeHoteisidRedeDeHoteisOld = em.merge(redeDeHoteisidRedeDeHoteisOld);
            }
            if (redeDeHoteisidRedeDeHoteisNew != null && !redeDeHoteisidRedeDeHoteisNew.equals(redeDeHoteisidRedeDeHoteisOld)) {
                redeDeHoteisidRedeDeHoteisNew.getQuartosCollection().add(quartos);
                redeDeHoteisidRedeDeHoteisNew = em.merge(redeDeHoteisidRedeDeHoteisNew);
            }
            if (tiposdequartosidTiposdequartosOld != null && !tiposdequartosidTiposdequartosOld.equals(tiposdequartosidTiposdequartosNew)) {
                tiposdequartosidTiposdequartosOld.getQuartosCollection().remove(quartos);
                tiposdequartosidTiposdequartosOld = em.merge(tiposdequartosidTiposdequartosOld);
            }
            if (tiposdequartosidTiposdequartosNew != null && !tiposdequartosidTiposdequartosNew.equals(tiposdequartosidTiposdequartosOld)) {
                tiposdequartosidTiposdequartosNew.getQuartosCollection().add(quartos);
                tiposdequartosidTiposdequartosNew = em.merge(tiposdequartosidTiposdequartosNew);
            }
            for (Fotosquartos fotosquartosCollectionNewFotosquartos : fotosquartosCollectionNew) {
                if (!fotosquartosCollectionOld.contains(fotosquartosCollectionNewFotosquartos)) {
                    Quartos oldQuartosidQuartosOfFotosquartosCollectionNewFotosquartos = fotosquartosCollectionNewFotosquartos.getQuartosidQuartos();
                    fotosquartosCollectionNewFotosquartos.setQuartosidQuartos(quartos);
                    fotosquartosCollectionNewFotosquartos = em.merge(fotosquartosCollectionNewFotosquartos);
                    if (oldQuartosidQuartosOfFotosquartosCollectionNewFotosquartos != null && !oldQuartosidQuartosOfFotosquartosCollectionNewFotosquartos.equals(quartos)) {
                        oldQuartosidQuartosOfFotosquartosCollectionNewFotosquartos.getFotosquartosCollection().remove(fotosquartosCollectionNewFotosquartos);
                        oldQuartosidQuartosOfFotosquartosCollectionNewFotosquartos = em.merge(oldQuartosidQuartosOfFotosquartosCollectionNewFotosquartos);
                    }
                }
            }
            for (Reserva reservaCollectionNewReserva : reservaCollectionNew) {
                if (!reservaCollectionOld.contains(reservaCollectionNewReserva)) {
                    Quartos oldQuartosidQuartosOfReservaCollectionNewReserva = reservaCollectionNewReserva.getQuartosidQuartos();
                    reservaCollectionNewReserva.setQuartosidQuartos(quartos);
                    reservaCollectionNewReserva = em.merge(reservaCollectionNewReserva);
                    if (oldQuartosidQuartosOfReservaCollectionNewReserva != null && !oldQuartosidQuartosOfReservaCollectionNewReserva.equals(quartos)) {
                        oldQuartosidQuartosOfReservaCollectionNewReserva.getReservaCollection().remove(reservaCollectionNewReserva);
                        oldQuartosidQuartosOfReservaCollectionNewReserva = em.merge(oldQuartosidQuartosOfReservaCollectionNewReserva);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = quartos.getIdQuartos();
                if (findQuartos(id) == null) {
                    throw new NonexistentEntityException("The quartos with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Quartos quartos;
            try {
                quartos = em.getReference(Quartos.class, id);
                quartos.getIdQuartos();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The quartos with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Fotosquartos> fotosquartosCollectionOrphanCheck = quartos.getFotosquartosCollection();
            for (Fotosquartos fotosquartosCollectionOrphanCheckFotosquartos : fotosquartosCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Quartos (" + quartos + ") cannot be destroyed since the Fotosquartos " + fotosquartosCollectionOrphanCheckFotosquartos + " in its fotosquartosCollection field has a non-nullable quartosidQuartos field.");
            }
            Collection<Reserva> reservaCollectionOrphanCheck = quartos.getReservaCollection();
            for (Reserva reservaCollectionOrphanCheckReserva : reservaCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Quartos (" + quartos + ") cannot be destroyed since the Reserva " + reservaCollectionOrphanCheckReserva + " in its reservaCollection field has a non-nullable quartosidQuartos field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Rededehoteis redeDeHoteisidRedeDeHoteis = quartos.getRedeDeHoteisidRedeDeHoteis();
            if (redeDeHoteisidRedeDeHoteis != null) {
                redeDeHoteisidRedeDeHoteis.getQuartosCollection().remove(quartos);
                redeDeHoteisidRedeDeHoteis = em.merge(redeDeHoteisidRedeDeHoteis);
            }
            TiposDeQuartos tiposdequartosidTiposdequartos = quartos.getTiposdequartosidTiposdequartos();
            if (tiposdequartosidTiposdequartos != null) {
                tiposdequartosidTiposdequartos.getQuartosCollection().remove(quartos);
                tiposdequartosidTiposdequartos = em.merge(tiposdequartosidTiposdequartos);
            }
            em.remove(quartos);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Quartos> findQuartosEntities() {
        return findQuartosEntities(true, -1, -1);
    }

    public List<Quartos> findQuartosEntities(int maxResults, int firstResult) {
        return findQuartosEntities(false, maxResults, firstResult);
    }

    private List<Quartos> findQuartosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Quartos.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Quartos findQuartos(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Quartos.class, id);
        } finally {
            em.close();
        }
    }

    public int getQuartosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Quartos> rt = cq.from(Quartos.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
