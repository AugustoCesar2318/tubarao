/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.IllegalOrphanException;
import dao.exceptions.NonexistentEntityException;
import domain.Cidade;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import domain.Estado;
import domain.Pessoa;
import java.util.ArrayList;
import java.util.Collection;
import domain.Rededehoteis;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Augusto
 */
public class CidadeJpaController implements Serializable {

    public EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("ProvaTubaraoPU");
        return factory.createEntityManager();
    }

    public void create(Cidade cidade) {
        if (cidade.getPessoaCollection() == null) {
            cidade.setPessoaCollection(new ArrayList<Pessoa>());
        }
        if (cidade.getRededehoteisCollection() == null) {
            cidade.setRededehoteisCollection(new ArrayList<Rededehoteis>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Estado estadoidEstado = cidade.getEstadoidEstado();
            if (estadoidEstado != null) {
                estadoidEstado = em.getReference(estadoidEstado.getClass(), estadoidEstado.getIdEstado());
                cidade.setEstadoidEstado(estadoidEstado);
            }
            Collection<Pessoa> attachedPessoaCollection = new ArrayList<Pessoa>();
            for (Pessoa pessoaCollectionPessoaToAttach : cidade.getPessoaCollection()) {
                pessoaCollectionPessoaToAttach = em.getReference(pessoaCollectionPessoaToAttach.getClass(), pessoaCollectionPessoaToAttach.getIdPessoa());
                attachedPessoaCollection.add(pessoaCollectionPessoaToAttach);
            }
            cidade.setPessoaCollection(attachedPessoaCollection);
            Collection<Rededehoteis> attachedRededehoteisCollection = new ArrayList<Rededehoteis>();
            for (Rededehoteis rededehoteisCollectionRededehoteisToAttach : cidade.getRededehoteisCollection()) {
                rededehoteisCollectionRededehoteisToAttach = em.getReference(rededehoteisCollectionRededehoteisToAttach.getClass(), rededehoteisCollectionRededehoteisToAttach.getIdRedeDeHoteis());
                attachedRededehoteisCollection.add(rededehoteisCollectionRededehoteisToAttach);
            }
            cidade.setRededehoteisCollection(attachedRededehoteisCollection);
            em.persist(cidade);
            if (estadoidEstado != null) {
                estadoidEstado.getCidadeCollection().add(cidade);
                estadoidEstado = em.merge(estadoidEstado);
            }
            for (Pessoa pessoaCollectionPessoa : cidade.getPessoaCollection()) {
                Cidade oldCidadeidCidadeOfPessoaCollectionPessoa = pessoaCollectionPessoa.getCidadeidCidade();
                pessoaCollectionPessoa.setCidadeidCidade(cidade);
                pessoaCollectionPessoa = em.merge(pessoaCollectionPessoa);
                if (oldCidadeidCidadeOfPessoaCollectionPessoa != null) {
                    oldCidadeidCidadeOfPessoaCollectionPessoa.getPessoaCollection().remove(pessoaCollectionPessoa);
                    oldCidadeidCidadeOfPessoaCollectionPessoa = em.merge(oldCidadeidCidadeOfPessoaCollectionPessoa);
                }
            }
            for (Rededehoteis rededehoteisCollectionRededehoteis : cidade.getRededehoteisCollection()) {
                Cidade oldCidadeidCidadeOfRededehoteisCollectionRededehoteis = rededehoteisCollectionRededehoteis.getCidadeidCidade();
                rededehoteisCollectionRededehoteis.setCidadeidCidade(cidade);
                rededehoteisCollectionRededehoteis = em.merge(rededehoteisCollectionRededehoteis);
                if (oldCidadeidCidadeOfRededehoteisCollectionRededehoteis != null) {
                    oldCidadeidCidadeOfRededehoteisCollectionRededehoteis.getRededehoteisCollection().remove(rededehoteisCollectionRededehoteis);
                    oldCidadeidCidadeOfRededehoteisCollectionRededehoteis = em.merge(oldCidadeidCidadeOfRededehoteisCollectionRededehoteis);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Cidade cidade) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cidade persistentCidade = em.find(Cidade.class, cidade.getIdCidade());
            Estado estadoidEstadoOld = persistentCidade.getEstadoidEstado();
            Estado estadoidEstadoNew = cidade.getEstadoidEstado();
            Collection<Pessoa> pessoaCollectionOld = persistentCidade.getPessoaCollection();
            Collection<Pessoa> pessoaCollectionNew = cidade.getPessoaCollection();
            Collection<Rededehoteis> rededehoteisCollectionOld = persistentCidade.getRededehoteisCollection();
            Collection<Rededehoteis> rededehoteisCollectionNew = cidade.getRededehoteisCollection();
            List<String> illegalOrphanMessages = null;
            for (Pessoa pessoaCollectionOldPessoa : pessoaCollectionOld) {
                if (!pessoaCollectionNew.contains(pessoaCollectionOldPessoa)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Pessoa " + pessoaCollectionOldPessoa + " since its cidadeidCidade field is not nullable.");
                }
            }
            for (Rededehoteis rededehoteisCollectionOldRededehoteis : rededehoteisCollectionOld) {
                if (!rededehoteisCollectionNew.contains(rededehoteisCollectionOldRededehoteis)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Rededehoteis " + rededehoteisCollectionOldRededehoteis + " since its cidadeidCidade field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (estadoidEstadoNew != null) {
                estadoidEstadoNew = em.getReference(estadoidEstadoNew.getClass(), estadoidEstadoNew.getIdEstado());
                cidade.setEstadoidEstado(estadoidEstadoNew);
            }
            Collection<Pessoa> attachedPessoaCollectionNew = new ArrayList<Pessoa>();
            for (Pessoa pessoaCollectionNewPessoaToAttach : pessoaCollectionNew) {
                pessoaCollectionNewPessoaToAttach = em.getReference(pessoaCollectionNewPessoaToAttach.getClass(), pessoaCollectionNewPessoaToAttach.getIdPessoa());
                attachedPessoaCollectionNew.add(pessoaCollectionNewPessoaToAttach);
            }
            pessoaCollectionNew = attachedPessoaCollectionNew;
            cidade.setPessoaCollection(pessoaCollectionNew);
            Collection<Rededehoteis> attachedRededehoteisCollectionNew = new ArrayList<Rededehoteis>();
            for (Rededehoteis rededehoteisCollectionNewRededehoteisToAttach : rededehoteisCollectionNew) {
                rededehoteisCollectionNewRededehoteisToAttach = em.getReference(rededehoteisCollectionNewRededehoteisToAttach.getClass(), rededehoteisCollectionNewRededehoteisToAttach.getIdRedeDeHoteis());
                attachedRededehoteisCollectionNew.add(rededehoteisCollectionNewRededehoteisToAttach);
            }
            rededehoteisCollectionNew = attachedRededehoteisCollectionNew;
            cidade.setRededehoteisCollection(rededehoteisCollectionNew);
            cidade = em.merge(cidade);
            if (estadoidEstadoOld != null && !estadoidEstadoOld.equals(estadoidEstadoNew)) {
                estadoidEstadoOld.getCidadeCollection().remove(cidade);
                estadoidEstadoOld = em.merge(estadoidEstadoOld);
            }
            if (estadoidEstadoNew != null && !estadoidEstadoNew.equals(estadoidEstadoOld)) {
                estadoidEstadoNew.getCidadeCollection().add(cidade);
                estadoidEstadoNew = em.merge(estadoidEstadoNew);
            }
            for (Pessoa pessoaCollectionNewPessoa : pessoaCollectionNew) {
                if (!pessoaCollectionOld.contains(pessoaCollectionNewPessoa)) {
                    Cidade oldCidadeidCidadeOfPessoaCollectionNewPessoa = pessoaCollectionNewPessoa.getCidadeidCidade();
                    pessoaCollectionNewPessoa.setCidadeidCidade(cidade);
                    pessoaCollectionNewPessoa = em.merge(pessoaCollectionNewPessoa);
                    if (oldCidadeidCidadeOfPessoaCollectionNewPessoa != null && !oldCidadeidCidadeOfPessoaCollectionNewPessoa.equals(cidade)) {
                        oldCidadeidCidadeOfPessoaCollectionNewPessoa.getPessoaCollection().remove(pessoaCollectionNewPessoa);
                        oldCidadeidCidadeOfPessoaCollectionNewPessoa = em.merge(oldCidadeidCidadeOfPessoaCollectionNewPessoa);
                    }
                }
            }
            for (Rededehoteis rededehoteisCollectionNewRededehoteis : rededehoteisCollectionNew) {
                if (!rededehoteisCollectionOld.contains(rededehoteisCollectionNewRededehoteis)) {
                    Cidade oldCidadeidCidadeOfRededehoteisCollectionNewRededehoteis = rededehoteisCollectionNewRededehoteis.getCidadeidCidade();
                    rededehoteisCollectionNewRededehoteis.setCidadeidCidade(cidade);
                    rededehoteisCollectionNewRededehoteis = em.merge(rededehoteisCollectionNewRededehoteis);
                    if (oldCidadeidCidadeOfRededehoteisCollectionNewRededehoteis != null && !oldCidadeidCidadeOfRededehoteisCollectionNewRededehoteis.equals(cidade)) {
                        oldCidadeidCidadeOfRededehoteisCollectionNewRededehoteis.getRededehoteisCollection().remove(rededehoteisCollectionNewRededehoteis);
                        oldCidadeidCidadeOfRededehoteisCollectionNewRededehoteis = em.merge(oldCidadeidCidadeOfRededehoteisCollectionNewRededehoteis);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = cidade.getIdCidade();
                if (findCidade(id) == null) {
                    throw new NonexistentEntityException("The cidade with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cidade cidade;
            try {
                cidade = em.getReference(Cidade.class, id);
                cidade.getIdCidade();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The cidade with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Pessoa> pessoaCollectionOrphanCheck = cidade.getPessoaCollection();
            for (Pessoa pessoaCollectionOrphanCheckPessoa : pessoaCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Cidade (" + cidade + ") cannot be destroyed since the Pessoa " + pessoaCollectionOrphanCheckPessoa + " in its pessoaCollection field has a non-nullable cidadeidCidade field.");
            }
            Collection<Rededehoteis> rededehoteisCollectionOrphanCheck = cidade.getRededehoteisCollection();
            for (Rededehoteis rededehoteisCollectionOrphanCheckRededehoteis : rededehoteisCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Cidade (" + cidade + ") cannot be destroyed since the Rededehoteis " + rededehoteisCollectionOrphanCheckRededehoteis + " in its rededehoteisCollection field has a non-nullable cidadeidCidade field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Estado estadoidEstado = cidade.getEstadoidEstado();
            if (estadoidEstado != null) {
                estadoidEstado.getCidadeCollection().remove(cidade);
                estadoidEstado = em.merge(estadoidEstado);
            }
            em.remove(cidade);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Cidade> findCidadeEntities() {
        return findCidadeEntities(true, -1, -1);
    }

    public List<Cidade> findCidadeEntities(int maxResults, int firstResult) {
        return findCidadeEntities(false, maxResults, firstResult);
    }

    private List<Cidade> findCidadeEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Cidade.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Cidade findCidade(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Cidade.class, id);
        } finally {
            em.close();
        }
    }

    public List<Cidade> findByEstado(Estado e){
        EntityManager em = getEntityManager();
        
        Query q = em.createQuery("Select c FROM Cidade c WHERE c.estadoidEstado = :estado");
        q.setParameter("estado", e);
        
        return q.getResultList();
    }
    
    public int getCidadeCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Cidade> rt = cq.from(Cidade.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
