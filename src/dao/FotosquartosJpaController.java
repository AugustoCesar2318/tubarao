/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.NonexistentEntityException;
import domain.Fotosquartos;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import domain.Quartos;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Augusto
 */
public class FotosquartosJpaController implements Serializable {

    public EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("ProvaTubaraoPU");
        return factory.createEntityManager();
    }

    public void create(Fotosquartos fotosquartos) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Quartos quartosidQuartos = fotosquartos.getQuartosidQuartos();
            if (quartosidQuartos != null) {
                quartosidQuartos = em.getReference(quartosidQuartos.getClass(), quartosidQuartos.getIdQuartos());
                fotosquartos.setQuartosidQuartos(quartosidQuartos);
            }
            em.persist(fotosquartos);
            if (quartosidQuartos != null) {
                quartosidQuartos.getFotosquartosCollection().add(fotosquartos);
                quartosidQuartos = em.merge(quartosidQuartos);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Fotosquartos fotosquartos) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Fotosquartos persistentFotosquartos = em.find(Fotosquartos.class, fotosquartos.getIdFotosQuartos());
            Quartos quartosidQuartosOld = persistentFotosquartos.getQuartosidQuartos();
            Quartos quartosidQuartosNew = fotosquartos.getQuartosidQuartos();
            if (quartosidQuartosNew != null) {
                quartosidQuartosNew = em.getReference(quartosidQuartosNew.getClass(), quartosidQuartosNew.getIdQuartos());
                fotosquartos.setQuartosidQuartos(quartosidQuartosNew);
            }
            fotosquartos = em.merge(fotosquartos);
            if (quartosidQuartosOld != null && !quartosidQuartosOld.equals(quartosidQuartosNew)) {
                quartosidQuartosOld.getFotosquartosCollection().remove(fotosquartos);
                quartosidQuartosOld = em.merge(quartosidQuartosOld);
            }
            if (quartosidQuartosNew != null && !quartosidQuartosNew.equals(quartosidQuartosOld)) {
                quartosidQuartosNew.getFotosquartosCollection().add(fotosquartos);
                quartosidQuartosNew = em.merge(quartosidQuartosNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = fotosquartos.getIdFotosQuartos();
                if (findFotosquartos(id) == null) {
                    throw new NonexistentEntityException("The fotosquartos with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Fotosquartos fotosquartos;
            try {
                fotosquartos = em.getReference(Fotosquartos.class, id);
                fotosquartos.getIdFotosQuartos();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The fotosquartos with id " + id + " no longer exists.", enfe);
            }
            Quartos quartosidQuartos = fotosquartos.getQuartosidQuartos();
            if (quartosidQuartos != null) {
                quartosidQuartos.getFotosquartosCollection().remove(fotosquartos);
                quartosidQuartos = em.merge(quartosidQuartos);
            }
            em.remove(fotosquartos);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Fotosquartos> findFotosquartosEntities() {
        return findFotosquartosEntities(true, -1, -1);
    }

    public List<Fotosquartos> findFotosquartosEntities(int maxResults, int firstResult) {
        return findFotosquartosEntities(false, maxResults, firstResult);
    }

    private List<Fotosquartos> findFotosquartosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Fotosquartos.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Fotosquartos findFotosquartos(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Fotosquartos.class, id);
        } finally {
            em.close();
        }
    }

    public int getFotosquartosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Fotosquartos> rt = cq.from(Fotosquartos.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List<Fotosquartos> findByQuarto(Quartos quarto){
        EntityManager em = getEntityManager();
        
        Query q = em.createQuery("Select q from Fotosquartos q WHERE q.quartosidQuartos = :quarto");
        q.setParameter("quarto", quarto);
        
        return q.getResultList();
    }
    
    public Fotosquartos findByUrl(Quartos quart, String url){
        EntityManager em = getEntityManager();
        
        Query q = em.createQuery("Select q from Fotosquartos q where q.quartosidQuartos = :quarto AND q.fotos = :url");
        q.setParameter("quarto", quart);
        q.setParameter("url", url);
        try{
            return (Fotosquartos) q.getSingleResult();
        }catch(Exception e){
            return null;
        }
    }
}