/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.IllegalOrphanException;
import dao.exceptions.NonexistentEntityException;
import dao.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import domain.Quartos;
import domain.TiposDeQuartos;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Augusto
 */
public class TiposDeQuartosJpaController implements Serializable {

    public EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("ProvaTubaraoPU");
        return factory.createEntityManager();
    }

    public void create(TiposDeQuartos tiposDeQuartos) throws PreexistingEntityException, Exception {
        if (tiposDeQuartos.getQuartosCollection() == null) {
            tiposDeQuartos.setQuartosCollection(new ArrayList<Quartos>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Quartos> attachedQuartosCollection = new ArrayList<Quartos>();
            for (Quartos quartosCollectionQuartosToAttach : tiposDeQuartos.getQuartosCollection()) {
                quartosCollectionQuartosToAttach = em.getReference(quartosCollectionQuartosToAttach.getClass(), quartosCollectionQuartosToAttach.getIdQuartos());
                attachedQuartosCollection.add(quartosCollectionQuartosToAttach);
            }
            tiposDeQuartos.setQuartosCollection(attachedQuartosCollection);
            em.persist(tiposDeQuartos);
            for (Quartos quartosCollectionQuartos : tiposDeQuartos.getQuartosCollection()) {
                TiposDeQuartos oldTiposdequartosidTiposdequartosOfQuartosCollectionQuartos = quartosCollectionQuartos.getTiposdequartosidTiposdequartos();
                quartosCollectionQuartos.setTiposdequartosidTiposdequartos(tiposDeQuartos);
                quartosCollectionQuartos = em.merge(quartosCollectionQuartos);
                if (oldTiposdequartosidTiposdequartosOfQuartosCollectionQuartos != null) {
                    oldTiposdequartosidTiposdequartosOfQuartosCollectionQuartos.getQuartosCollection().remove(quartosCollectionQuartos);
                    oldTiposdequartosidTiposdequartosOfQuartosCollectionQuartos = em.merge(oldTiposdequartosidTiposdequartosOfQuartosCollectionQuartos);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findTiposDeQuartos(tiposDeQuartos.getIdTiposdequartos()) != null) {
                throw new PreexistingEntityException("TiposDeQuartos " + tiposDeQuartos + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TiposDeQuartos tiposDeQuartos) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TiposDeQuartos persistentTiposDeQuartos = em.find(TiposDeQuartos.class, tiposDeQuartos.getIdTiposdequartos());
            Collection<Quartos> quartosCollectionOld = persistentTiposDeQuartos.getQuartosCollection();
            Collection<Quartos> quartosCollectionNew = tiposDeQuartos.getQuartosCollection();
            List<String> illegalOrphanMessages = null;
            for (Quartos quartosCollectionOldQuartos : quartosCollectionOld) {
                if (!quartosCollectionNew.contains(quartosCollectionOldQuartos)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Quartos " + quartosCollectionOldQuartos + " since its tiposdequartosidTiposdequartos field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Quartos> attachedQuartosCollectionNew = new ArrayList<Quartos>();
            for (Quartos quartosCollectionNewQuartosToAttach : quartosCollectionNew) {
                quartosCollectionNewQuartosToAttach = em.getReference(quartosCollectionNewQuartosToAttach.getClass(), quartosCollectionNewQuartosToAttach.getIdQuartos());
                attachedQuartosCollectionNew.add(quartosCollectionNewQuartosToAttach);
            }
            quartosCollectionNew = attachedQuartosCollectionNew;
            tiposDeQuartos.setQuartosCollection(quartosCollectionNew);
            tiposDeQuartos = em.merge(tiposDeQuartos);
            for (Quartos quartosCollectionNewQuartos : quartosCollectionNew) {
                if (!quartosCollectionOld.contains(quartosCollectionNewQuartos)) {
                    TiposDeQuartos oldTiposdequartosidTiposdequartosOfQuartosCollectionNewQuartos = quartosCollectionNewQuartos.getTiposdequartosidTiposdequartos();
                    quartosCollectionNewQuartos.setTiposdequartosidTiposdequartos(tiposDeQuartos);
                    quartosCollectionNewQuartos = em.merge(quartosCollectionNewQuartos);
                    if (oldTiposdequartosidTiposdequartosOfQuartosCollectionNewQuartos != null && !oldTiposdequartosidTiposdequartosOfQuartosCollectionNewQuartos.equals(tiposDeQuartos)) {
                        oldTiposdequartosidTiposdequartosOfQuartosCollectionNewQuartos.getQuartosCollection().remove(quartosCollectionNewQuartos);
                        oldTiposdequartosidTiposdequartosOfQuartosCollectionNewQuartos = em.merge(oldTiposdequartosidTiposdequartosOfQuartosCollectionNewQuartos);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tiposDeQuartos.getIdTiposdequartos();
                if (findTiposDeQuartos(id) == null) {
                    throw new NonexistentEntityException("The tiposDeQuartos with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TiposDeQuartos tiposDeQuartos;
            try {
                tiposDeQuartos = em.getReference(TiposDeQuartos.class, id);
                tiposDeQuartos.getIdTiposdequartos();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tiposDeQuartos with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Quartos> quartosCollectionOrphanCheck = tiposDeQuartos.getQuartosCollection();
            for (Quartos quartosCollectionOrphanCheckQuartos : quartosCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TiposDeQuartos (" + tiposDeQuartos + ") cannot be destroyed since the Quartos " + quartosCollectionOrphanCheckQuartos + " in its quartosCollection field has a non-nullable tiposdequartosidTiposdequartos field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(tiposDeQuartos);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TiposDeQuartos> findTiposDeQuartosEntities() {
        return findTiposDeQuartosEntities(true, -1, -1);
    }

    public List<TiposDeQuartos> findTiposDeQuartosEntities(int maxResults, int firstResult) {
        return findTiposDeQuartosEntities(false, maxResults, firstResult);
    }

    private List<TiposDeQuartos> findTiposDeQuartosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TiposDeQuartos.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TiposDeQuartos findTiposDeQuartos(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TiposDeQuartos.class, id);
        } finally {
            em.close();
        }
    }

    public int getTiposDeQuartosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TiposDeQuartos> rt = cq.from(TiposDeQuartos.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
