/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.IllegalOrphanException;
import dao.exceptions.NonexistentEntityException;
import domain.Checkin;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import domain.Reserva;
import domain.Checkout;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Augusto
 */
public class CheckinJpaController implements Serializable {

    public EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("ProvaTubaraoPU");
        return factory.createEntityManager();
    }

    public void create(Checkin checkin) {
        if (checkin.getCheckoutCollection() == null) {
            checkin.setCheckoutCollection(new ArrayList<Checkout>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Reserva reservaidReseva = checkin.getReservaidReseva();
            if (reservaidReseva != null) {
                reservaidReseva = em.getReference(reservaidReseva.getClass(), reservaidReseva.getIdReseva());
                checkin.setReservaidReseva(reservaidReseva);
            }
            Collection<Checkout> attachedCheckoutCollection = new ArrayList<Checkout>();
            for (Checkout checkoutCollectionCheckoutToAttach : checkin.getCheckoutCollection()) {
                checkoutCollectionCheckoutToAttach = em.getReference(checkoutCollectionCheckoutToAttach.getClass(), checkoutCollectionCheckoutToAttach.getIdCheckOut());
                attachedCheckoutCollection.add(checkoutCollectionCheckoutToAttach);
            }
            checkin.setCheckoutCollection(attachedCheckoutCollection);
            em.persist(checkin);
            if (reservaidReseva != null) {
                reservaidReseva.getCheckinCollection().add(checkin);
                reservaidReseva = em.merge(reservaidReseva);
            }
            for (Checkout checkoutCollectionCheckout : checkin.getCheckoutCollection()) {
                Checkin oldCheckInidCheckInOfCheckoutCollectionCheckout = checkoutCollectionCheckout.getCheckInidCheckIn();
                checkoutCollectionCheckout.setCheckInidCheckIn(checkin);
                checkoutCollectionCheckout = em.merge(checkoutCollectionCheckout);
                if (oldCheckInidCheckInOfCheckoutCollectionCheckout != null) {
                    oldCheckInidCheckInOfCheckoutCollectionCheckout.getCheckoutCollection().remove(checkoutCollectionCheckout);
                    oldCheckInidCheckInOfCheckoutCollectionCheckout = em.merge(oldCheckInidCheckInOfCheckoutCollectionCheckout);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Checkin checkin) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Checkin persistentCheckin = em.find(Checkin.class, checkin.getIdCheckIn());
            Reserva reservaidResevaOld = persistentCheckin.getReservaidReseva();
            Reserva reservaidResevaNew = checkin.getReservaidReseva();
            Collection<Checkout> checkoutCollectionOld = persistentCheckin.getCheckoutCollection();
            Collection<Checkout> checkoutCollectionNew = checkin.getCheckoutCollection();
            List<String> illegalOrphanMessages = null;
            for (Checkout checkoutCollectionOldCheckout : checkoutCollectionOld) {
                if (!checkoutCollectionNew.contains(checkoutCollectionOldCheckout)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Checkout " + checkoutCollectionOldCheckout + " since its checkInidCheckIn field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (reservaidResevaNew != null) {
                reservaidResevaNew = em.getReference(reservaidResevaNew.getClass(), reservaidResevaNew.getIdReseva());
                checkin.setReservaidReseva(reservaidResevaNew);
            }
            Collection<Checkout> attachedCheckoutCollectionNew = new ArrayList<Checkout>();
            for (Checkout checkoutCollectionNewCheckoutToAttach : checkoutCollectionNew) {
                checkoutCollectionNewCheckoutToAttach = em.getReference(checkoutCollectionNewCheckoutToAttach.getClass(), checkoutCollectionNewCheckoutToAttach.getIdCheckOut());
                attachedCheckoutCollectionNew.add(checkoutCollectionNewCheckoutToAttach);
            }
            checkoutCollectionNew = attachedCheckoutCollectionNew;
            checkin.setCheckoutCollection(checkoutCollectionNew);
            checkin = em.merge(checkin);
            if (reservaidResevaOld != null && !reservaidResevaOld.equals(reservaidResevaNew)) {
                reservaidResevaOld.getCheckinCollection().remove(checkin);
                reservaidResevaOld = em.merge(reservaidResevaOld);
            }
            if (reservaidResevaNew != null && !reservaidResevaNew.equals(reservaidResevaOld)) {
                reservaidResevaNew.getCheckinCollection().add(checkin);
                reservaidResevaNew = em.merge(reservaidResevaNew);
            }
            for (Checkout checkoutCollectionNewCheckout : checkoutCollectionNew) {
                if (!checkoutCollectionOld.contains(checkoutCollectionNewCheckout)) {
                    Checkin oldCheckInidCheckInOfCheckoutCollectionNewCheckout = checkoutCollectionNewCheckout.getCheckInidCheckIn();
                    checkoutCollectionNewCheckout.setCheckInidCheckIn(checkin);
                    checkoutCollectionNewCheckout = em.merge(checkoutCollectionNewCheckout);
                    if (oldCheckInidCheckInOfCheckoutCollectionNewCheckout != null && !oldCheckInidCheckInOfCheckoutCollectionNewCheckout.equals(checkin)) {
                        oldCheckInidCheckInOfCheckoutCollectionNewCheckout.getCheckoutCollection().remove(checkoutCollectionNewCheckout);
                        oldCheckInidCheckInOfCheckoutCollectionNewCheckout = em.merge(oldCheckInidCheckInOfCheckoutCollectionNewCheckout);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = checkin.getIdCheckIn();
                if (findCheckin(id) == null) {
                    throw new NonexistentEntityException("The checkin with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Checkin checkin;
            try {
                checkin = em.getReference(Checkin.class, id);
                checkin.getIdCheckIn();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The checkin with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Checkout> checkoutCollectionOrphanCheck = checkin.getCheckoutCollection();
            for (Checkout checkoutCollectionOrphanCheckCheckout : checkoutCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Checkin (" + checkin + ") cannot be destroyed since the Checkout " + checkoutCollectionOrphanCheckCheckout + " in its checkoutCollection field has a non-nullable checkInidCheckIn field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Reserva reservaidReseva = checkin.getReservaidReseva();
            if (reservaidReseva != null) {
                reservaidReseva.getCheckinCollection().remove(checkin);
                reservaidReseva = em.merge(reservaidReseva);
            }
            em.remove(checkin);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Checkin> findCheckinEntities() {
        return findCheckinEntities(true, -1, -1);
    }

    public List<Checkin> findCheckinEntities(int maxResults, int firstResult) {
        return findCheckinEntities(false, maxResults, firstResult);
    }

    private List<Checkin> findCheckinEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Checkin.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Checkin findCheckin(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Checkin.class, id);
        } finally {
            em.close();
        }
    }

    public int getCheckinCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Checkin> rt = cq.from(Checkin.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
