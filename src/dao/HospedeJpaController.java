/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.IllegalOrphanException;
import dao.exceptions.NonexistentEntityException;
import domain.Hospede;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import domain.Pessoa;
import domain.Reserva;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Augusto
 */
public class HospedeJpaController implements Serializable {

    public EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("ProvaTubaraoPU");
        return factory.createEntityManager();
    }

    public void create(Hospede hospede) {
        if (hospede.getReservaCollection() == null) {
            hospede.setReservaCollection(new ArrayList<Reserva>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pessoa pessoaidPessoa = hospede.getPessoaidPessoa();
            if (pessoaidPessoa != null) {
                pessoaidPessoa = em.getReference(pessoaidPessoa.getClass(), pessoaidPessoa.getIdPessoa());
                hospede.setPessoaidPessoa(pessoaidPessoa);
            }
            Collection<Reserva> attachedReservaCollection = new ArrayList<Reserva>();
            for (Reserva reservaCollectionReservaToAttach : hospede.getReservaCollection()) {
                reservaCollectionReservaToAttach = em.getReference(reservaCollectionReservaToAttach.getClass(), reservaCollectionReservaToAttach.getIdReseva());
                attachedReservaCollection.add(reservaCollectionReservaToAttach);
            }
            hospede.setReservaCollection(attachedReservaCollection);
            em.persist(hospede);
            if (pessoaidPessoa != null) {
                pessoaidPessoa.getHospedeCollection().add(hospede);
                pessoaidPessoa = em.merge(pessoaidPessoa);
            }
            for (Reserva reservaCollectionReserva : hospede.getReservaCollection()) {
                Hospede oldHospedeidHospedeOfReservaCollectionReserva = reservaCollectionReserva.getHospedeidHospede();
                reservaCollectionReserva.setHospedeidHospede(hospede);
                reservaCollectionReserva = em.merge(reservaCollectionReserva);
                if (oldHospedeidHospedeOfReservaCollectionReserva != null) {
                    oldHospedeidHospedeOfReservaCollectionReserva.getReservaCollection().remove(reservaCollectionReserva);
                    oldHospedeidHospedeOfReservaCollectionReserva = em.merge(oldHospedeidHospedeOfReservaCollectionReserva);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Hospede hospede) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Hospede persistentHospede = em.find(Hospede.class, hospede.getIdHospede());
            Pessoa pessoaidPessoaOld = persistentHospede.getPessoaidPessoa();
            Pessoa pessoaidPessoaNew = hospede.getPessoaidPessoa();
            Collection<Reserva> reservaCollectionOld = persistentHospede.getReservaCollection();
            Collection<Reserva> reservaCollectionNew = hospede.getReservaCollection();
            List<String> illegalOrphanMessages = null;
            for (Reserva reservaCollectionOldReserva : reservaCollectionOld) {
                if (!reservaCollectionNew.contains(reservaCollectionOldReserva)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Reserva " + reservaCollectionOldReserva + " since its hospedeidHospede field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (pessoaidPessoaNew != null) {
                pessoaidPessoaNew = em.getReference(pessoaidPessoaNew.getClass(), pessoaidPessoaNew.getIdPessoa());
                hospede.setPessoaidPessoa(pessoaidPessoaNew);
            }
            Collection<Reserva> attachedReservaCollectionNew = new ArrayList<Reserva>();
            for (Reserva reservaCollectionNewReservaToAttach : reservaCollectionNew) {
                reservaCollectionNewReservaToAttach = em.getReference(reservaCollectionNewReservaToAttach.getClass(), reservaCollectionNewReservaToAttach.getIdReseva());
                attachedReservaCollectionNew.add(reservaCollectionNewReservaToAttach);
            }
            reservaCollectionNew = attachedReservaCollectionNew;
            hospede.setReservaCollection(reservaCollectionNew);
            hospede = em.merge(hospede);
            if (pessoaidPessoaOld != null && !pessoaidPessoaOld.equals(pessoaidPessoaNew)) {
                pessoaidPessoaOld.getHospedeCollection().remove(hospede);
                pessoaidPessoaOld = em.merge(pessoaidPessoaOld);
            }
            if (pessoaidPessoaNew != null && !pessoaidPessoaNew.equals(pessoaidPessoaOld)) {
                pessoaidPessoaNew.getHospedeCollection().add(hospede);
                pessoaidPessoaNew = em.merge(pessoaidPessoaNew);
            }
            for (Reserva reservaCollectionNewReserva : reservaCollectionNew) {
                if (!reservaCollectionOld.contains(reservaCollectionNewReserva)) {
                    Hospede oldHospedeidHospedeOfReservaCollectionNewReserva = reservaCollectionNewReserva.getHospedeidHospede();
                    reservaCollectionNewReserva.setHospedeidHospede(hospede);
                    reservaCollectionNewReserva = em.merge(reservaCollectionNewReserva);
                    if (oldHospedeidHospedeOfReservaCollectionNewReserva != null && !oldHospedeidHospedeOfReservaCollectionNewReserva.equals(hospede)) {
                        oldHospedeidHospedeOfReservaCollectionNewReserva.getReservaCollection().remove(reservaCollectionNewReserva);
                        oldHospedeidHospedeOfReservaCollectionNewReserva = em.merge(oldHospedeidHospedeOfReservaCollectionNewReserva);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = hospede.getIdHospede();
                if (findHospede(id) == null) {
                    throw new NonexistentEntityException("The hospede with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Hospede hospede;
            try {
                hospede = em.getReference(Hospede.class, id);
                hospede.getIdHospede();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The hospede with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Reserva> reservaCollectionOrphanCheck = hospede.getReservaCollection();
            for (Reserva reservaCollectionOrphanCheckReserva : reservaCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Hospede (" + hospede + ") cannot be destroyed since the Reserva " + reservaCollectionOrphanCheckReserva + " in its reservaCollection field has a non-nullable hospedeidHospede field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Pessoa pessoaidPessoa = hospede.getPessoaidPessoa();
            if (pessoaidPessoa != null) {
                pessoaidPessoa.getHospedeCollection().remove(hospede);
                pessoaidPessoa = em.merge(pessoaidPessoa);
            }
            em.remove(hospede);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Hospede> findHospedeEntities() {
        return findHospedeEntities(true, -1, -1);
    }

    public List<Hospede> findHospedeEntities(int maxResults, int firstResult) {
        return findHospedeEntities(false, maxResults, firstResult);
    }

    private List<Hospede> findHospedeEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Hospede.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Hospede findHospede(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Hospede.class, id);
        } finally {
            em.close();
        }
    }

    public int getHospedeCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Hospede> rt = cq.from(Hospede.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List<Hospede> findByEmail(String email) {
        EntityManager em = getEntityManager();

        Query q = em.createQuery("Select p from Pessoa p WHERE p.email = :email");
        q.setParameter("email", email);

        List<Pessoa> p = q.getResultList();

        Query query = em.createQuery("Select h from Hospede h WHERE h.pessoaidPessoa = :id");

        for (Pessoa pessoa : p) {
            query.setParameter("id", pessoa);
        }

        return query.getResultList();
    }

    public List<Hospede> findByNome(String nome) {
        EntityManager em = getEntityManager();

        Query q = em.createQuery("Select p from Pessoa p WHERE p.nomePessoa LIKE :nome");
        q.setParameter("nome", "%" + nome + "%");

        List<Pessoa> p = q.getResultList();

        Query query = em.createQuery("Select h from Hospede h WHERE h.pessoaidPessoa = :id");

        for (Pessoa pessoa : p) {
            query.setParameter("id", pessoa);
        }

        return query.getResultList();
    }
}
