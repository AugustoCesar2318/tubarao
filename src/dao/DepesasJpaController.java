/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.NonexistentEntityException;
import domain.Depesas;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import domain.Funcionarios;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Augusto
 */
public class DepesasJpaController implements Serializable {

    public EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("ProvaTubaraoPU");
        return factory.createEntityManager();
    }

    public void create(Depesas depesas) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Funcionarios funcionariosidFuncionarios = depesas.getFuncionariosidFuncionarios();
            if (funcionariosidFuncionarios != null) {
                funcionariosidFuncionarios = em.getReference(funcionariosidFuncionarios.getClass(), funcionariosidFuncionarios.getIdFuncionarios());
                depesas.setFuncionariosidFuncionarios(funcionariosidFuncionarios);
            }
            em.persist(depesas);
            if (funcionariosidFuncionarios != null) {
                funcionariosidFuncionarios.getDepesasCollection().add(depesas);
                funcionariosidFuncionarios = em.merge(funcionariosidFuncionarios);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Depesas depesas) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Depesas persistentDepesas = em.find(Depesas.class, depesas.getIdDepesas());
            Funcionarios funcionariosidFuncionariosOld = persistentDepesas.getFuncionariosidFuncionarios();
            Funcionarios funcionariosidFuncionariosNew = depesas.getFuncionariosidFuncionarios();
            if (funcionariosidFuncionariosNew != null) {
                funcionariosidFuncionariosNew = em.getReference(funcionariosidFuncionariosNew.getClass(), funcionariosidFuncionariosNew.getIdFuncionarios());
                depesas.setFuncionariosidFuncionarios(funcionariosidFuncionariosNew);
            }
            depesas = em.merge(depesas);
            if (funcionariosidFuncionariosOld != null && !funcionariosidFuncionariosOld.equals(funcionariosidFuncionariosNew)) {
                funcionariosidFuncionariosOld.getDepesasCollection().remove(depesas);
                funcionariosidFuncionariosOld = em.merge(funcionariosidFuncionariosOld);
            }
            if (funcionariosidFuncionariosNew != null && !funcionariosidFuncionariosNew.equals(funcionariosidFuncionariosOld)) {
                funcionariosidFuncionariosNew.getDepesasCollection().add(depesas);
                funcionariosidFuncionariosNew = em.merge(funcionariosidFuncionariosNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = depesas.getIdDepesas();
                if (findDepesas(id) == null) {
                    throw new NonexistentEntityException("The depesas with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Depesas depesas;
            try {
                depesas = em.getReference(Depesas.class, id);
                depesas.getIdDepesas();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The depesas with id " + id + " no longer exists.", enfe);
            }
            Funcionarios funcionariosidFuncionarios = depesas.getFuncionariosidFuncionarios();
            if (funcionariosidFuncionarios != null) {
                funcionariosidFuncionarios.getDepesasCollection().remove(depesas);
                funcionariosidFuncionarios = em.merge(funcionariosidFuncionarios);
            }
            em.remove(depesas);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Depesas> findDepesasEntities() {
        return findDepesasEntities(true, -1, -1);
    }

    public List<Depesas> findDepesasEntities(int maxResults, int firstResult) {
        return findDepesasEntities(false, maxResults, firstResult);
    }

    private List<Depesas> findDepesasEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Depesas.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Depesas findDepesas(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Depesas.class, id);
        } finally {
            em.close();
        }
    }

    public int getDepesasCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Depesas> rt = cq.from(Depesas.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
