/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.IllegalOrphanException;
import dao.exceptions.NonexistentEntityException;
import dao.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import domain.Funcionarios;
import domain.TipoFuncionario;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Augusto
 */
public class TipoFuncionarioJpaController implements Serializable {

    public EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("ProvaTubaraoPU");
        return factory.createEntityManager();
    }

    public void create(TipoFuncionario tipoFuncionario) throws PreexistingEntityException, Exception {
        if (tipoFuncionario.getFuncionariosCollection() == null) {
            tipoFuncionario.setFuncionariosCollection(new ArrayList<Funcionarios>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Funcionarios> attachedFuncionariosCollection = new ArrayList<Funcionarios>();
            for (Funcionarios funcionariosCollectionFuncionariosToAttach : tipoFuncionario.getFuncionariosCollection()) {
                funcionariosCollectionFuncionariosToAttach = em.getReference(funcionariosCollectionFuncionariosToAttach.getClass(), funcionariosCollectionFuncionariosToAttach.getIdFuncionarios());
                attachedFuncionariosCollection.add(funcionariosCollectionFuncionariosToAttach);
            }
            tipoFuncionario.setFuncionariosCollection(attachedFuncionariosCollection);
            em.persist(tipoFuncionario);
            for (Funcionarios funcionariosCollectionFuncionarios : tipoFuncionario.getFuncionariosCollection()) {
                TipoFuncionario oldTipoFuncionarioidTipoFuncionarioOfFuncionariosCollectionFuncionarios = funcionariosCollectionFuncionarios.getTipoFuncionarioidTipoFuncionario();
                funcionariosCollectionFuncionarios.setTipoFuncionarioidTipoFuncionario(tipoFuncionario);
                funcionariosCollectionFuncionarios = em.merge(funcionariosCollectionFuncionarios);
                if (oldTipoFuncionarioidTipoFuncionarioOfFuncionariosCollectionFuncionarios != null) {
                    oldTipoFuncionarioidTipoFuncionarioOfFuncionariosCollectionFuncionarios.getFuncionariosCollection().remove(funcionariosCollectionFuncionarios);
                    oldTipoFuncionarioidTipoFuncionarioOfFuncionariosCollectionFuncionarios = em.merge(oldTipoFuncionarioidTipoFuncionarioOfFuncionariosCollectionFuncionarios);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findTipoFuncionario(tipoFuncionario.getIdTipoFuncionario()) != null) {
                throw new PreexistingEntityException("TipoFuncionario " + tipoFuncionario + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TipoFuncionario tipoFuncionario) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoFuncionario persistentTipoFuncionario = em.find(TipoFuncionario.class, tipoFuncionario.getIdTipoFuncionario());
            Collection<Funcionarios> funcionariosCollectionOld = persistentTipoFuncionario.getFuncionariosCollection();
            Collection<Funcionarios> funcionariosCollectionNew = tipoFuncionario.getFuncionariosCollection();
            List<String> illegalOrphanMessages = null;
            for (Funcionarios funcionariosCollectionOldFuncionarios : funcionariosCollectionOld) {
                if (!funcionariosCollectionNew.contains(funcionariosCollectionOldFuncionarios)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Funcionarios " + funcionariosCollectionOldFuncionarios + " since its tipoFuncionarioidTipoFuncionario field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Funcionarios> attachedFuncionariosCollectionNew = new ArrayList<Funcionarios>();
            for (Funcionarios funcionariosCollectionNewFuncionariosToAttach : funcionariosCollectionNew) {
                funcionariosCollectionNewFuncionariosToAttach = em.getReference(funcionariosCollectionNewFuncionariosToAttach.getClass(), funcionariosCollectionNewFuncionariosToAttach.getIdFuncionarios());
                attachedFuncionariosCollectionNew.add(funcionariosCollectionNewFuncionariosToAttach);
            }
            funcionariosCollectionNew = attachedFuncionariosCollectionNew;
            tipoFuncionario.setFuncionariosCollection(funcionariosCollectionNew);
            tipoFuncionario = em.merge(tipoFuncionario);
            for (Funcionarios funcionariosCollectionNewFuncionarios : funcionariosCollectionNew) {
                if (!funcionariosCollectionOld.contains(funcionariosCollectionNewFuncionarios)) {
                    TipoFuncionario oldTipoFuncionarioidTipoFuncionarioOfFuncionariosCollectionNewFuncionarios = funcionariosCollectionNewFuncionarios.getTipoFuncionarioidTipoFuncionario();
                    funcionariosCollectionNewFuncionarios.setTipoFuncionarioidTipoFuncionario(tipoFuncionario);
                    funcionariosCollectionNewFuncionarios = em.merge(funcionariosCollectionNewFuncionarios);
                    if (oldTipoFuncionarioidTipoFuncionarioOfFuncionariosCollectionNewFuncionarios != null && !oldTipoFuncionarioidTipoFuncionarioOfFuncionariosCollectionNewFuncionarios.equals(tipoFuncionario)) {
                        oldTipoFuncionarioidTipoFuncionarioOfFuncionariosCollectionNewFuncionarios.getFuncionariosCollection().remove(funcionariosCollectionNewFuncionarios);
                        oldTipoFuncionarioidTipoFuncionarioOfFuncionariosCollectionNewFuncionarios = em.merge(oldTipoFuncionarioidTipoFuncionarioOfFuncionariosCollectionNewFuncionarios);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tipoFuncionario.getIdTipoFuncionario();
                if (findTipoFuncionario(id) == null) {
                    throw new NonexistentEntityException("The tipoFuncionario with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoFuncionario tipoFuncionario;
            try {
                tipoFuncionario = em.getReference(TipoFuncionario.class, id);
                tipoFuncionario.getIdTipoFuncionario();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tipoFuncionario with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Funcionarios> funcionariosCollectionOrphanCheck = tipoFuncionario.getFuncionariosCollection();
            for (Funcionarios funcionariosCollectionOrphanCheckFuncionarios : funcionariosCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TipoFuncionario (" + tipoFuncionario + ") cannot be destroyed since the Funcionarios " + funcionariosCollectionOrphanCheckFuncionarios + " in its funcionariosCollection field has a non-nullable tipoFuncionarioidTipoFuncionario field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(tipoFuncionario);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TipoFuncionario> findTipoFuncionarioEntities() {
        return findTipoFuncionarioEntities(true, -1, -1);
    }

    public List<TipoFuncionario> findTipoFuncionarioEntities(int maxResults, int firstResult) {
        return findTipoFuncionarioEntities(false, maxResults, firstResult);
    }

    private List<TipoFuncionario> findTipoFuncionarioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TipoFuncionario.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TipoFuncionario findTipoFuncionario(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TipoFuncionario.class, id);
        } finally {
            em.close();
        }
    }

    public int getTipoFuncionarioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TipoFuncionario> rt = cq.from(TipoFuncionario.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
